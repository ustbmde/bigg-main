package edu.ustb.sei.mde.bigg.presentation.actions;

import org.eclipse.jface.action.Action;

import edu.ustb.sei.mde.bigg.BiggProgram;

public class DeriveBXAction extends Action {

	public DeriveBXAction() {
		super("Derive BiGG");
	}
	
	protected BiggProgram program = null;


	public BiggProgram getProgram() {
		return program;
	}

	public void setProgram(BiggProgram program) {
		this.program = program;
	}
	
	@Override
	public void run() {
		
//		RuleSet originalSet = program.getRuleSet();
//		URI originalURI = program.eResource().getURI();
//		
//		URI uriWithoutExtension =  originalURI.trimFileExtension();
//		URI uriForPath = uriWithoutExtension.trimSegments(1);
//		
//		
//		BXRuleGenerator generator = new BXRuleGenerator(program.eResource().getResourceSet());
//		
//		RuleConstraintGenerator constraintGenerator = new RuleConstraintGenerator();
//		
////		Collection<GroupLayer> backwardLayers = doGenerateBackwardLayers(generator, uriForPath.appendSegment(uriWithoutExtension.lastSegment()+"Bwd.bigg"), originalSet);
////		Collection<GroupLayer> forwardLayers = doGenerateForwardLayers(generator, uriForPath.appendSegment(uriWithoutExtension.lastSegment()+"Fwd.bigg"), originalSet);
//		
//		Pair<List<GroupLayer>, List<GroupLayer>> pair = doGenerateBXLayers(generator, uriForPath.appendSegment(uriWithoutExtension.lastSegment()+"Fwd.bigg"), uriForPath.appendSegment(uriWithoutExtension.lastSegment()+"Bwd.bigg"), program);
//		
//		doGenerateModelConstraints(uriWithoutExtension.lastSegment(), constraintGenerator, uriWithoutExtension.appendFileExtension("als"), program, pair.getValue(), pair.getKey());
	}
	
//	private Pair<List<GroupLayer>, List<GroupLayer>> doGenerateBXLayers(BXRuleGenerator generator, URI fwdURI, URI bwdURI, BiggProgram program) {
//		OperationalRuleInformation opRuleInformation = generator.generateOperationalRules(program);
//		
//		
//		Resource fwdResource = generator.createResource(fwdURI);
//		Resource bwdResource = generator.createResource(bwdURI);
//		
//		fwdResource.getContents().addAll(opRuleInformation.forwardLayers);
//		bwdResource.getContents().addAll(opRuleInformation.backwardLayers);
//		
////		List<Pair<GroupLayer, GroupLayer>> layerPairs = generator.generateRuleSetPair(opRuleInformation.forwardLayers, opRuleInformation.backwardLayers, fwdResource, bwdResource);
//		
////		List<GroupLayer> forwardLayers = layerPairs.stream().map(Pair::getKey).collect(Collectors.toList());
////		fwdResource.getContents().addAll(generator.makeOrderAndSort(forwardLayers));
////		
////		List<GroupLayer> backwardLayers = layerPairs.stream().map(Pair::getValue).collect(Collectors.toList());
////		bwdResource.getContents().addAll(generator.makeOrderAndSort(backwardLayers));
//		
//		
//		Map<String, Object> option = new HashMap<>();
//		option.put(XMIResource.OPTION_SCHEMA_LOCATION, true);
//		
//		try {
//			fwdResource.save(option);
//			bwdResource.save(option);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		return Pair.of(opRuleInformation.forwardLayers, opRuleInformation.backwardLayers);
//	}
	
//	private void doGenerateModelConstraints(String name, RuleConstraintGenerator generator, URI uri, BiggProgram program,List<GroupLayer> backwardLayers,List<GroupLayer> forwardLayers) {
//		WorkspaceUtil util = new WorkspaceUtil();
//		IFile file = util.getFile(util.getPath(uri));
//		util.write(file, ()->generator.generateConstraints(name, program,backwardLayers, forwardLayers));
//	}

	@Override
	public boolean isEnabled() {
		return program!=null;
	}
}
