/**
 */
package edu.ustb.sei.mde.bigg;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Argument</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getArgument()
 * @model abstract="true"
 * @generated
 */
public interface Argument extends EObject {
	String description();
} // Argument
