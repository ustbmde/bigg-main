/**
 */
package edu.ustb.sei.mde.bigg;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Derived Rule Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getDerivedRuleKind()
 * @model
 * @generated
 */
public enum DerivedRuleKind implements Enumerator {
	/**
	 * The '<em><b>Relation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RELATION_VALUE
	 * @generated
	 * @ordered
	 */
	RELATION(0, "relation", "relation"),

	/**
	 * The '<em><b>Checker</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CHECKER_VALUE
	 * @generated
	 * @ordered
	 */
	CHECKER(1, "checker", "checker"),

	/**
	 * The '<em><b>Adjuster</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ADJUSTER_VALUE
	 * @generated
	 * @ordered
	 */
	ADJUSTER(2, "adjuster", "adjuster"),

	/**
	 * The '<em><b>Bi Relation Creator</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BI_RELATION_CREATOR_VALUE
	 * @generated
	 * @ordered
	 */
	BI_RELATION_CREATOR(3, "biRelationCreator", "biRelationCreator"),

	/**
	 * The '<em><b>Backward Relation Creator</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BACKWARD_RELATION_CREATOR_VALUE
	 * @generated
	 * @ordered
	 */
	BACKWARD_RELATION_CREATOR(4, "backwardRelationCreator", "backwardRelationCreator"),

	/**
	 * The '<em><b>Forward Relation Creator</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FORWARD_RELATION_CREATOR_VALUE
	 * @generated
	 * @ordered
	 */
	FORWARD_RELATION_CREATOR(5, "forwardRelationCreator", "forwardRelationCreator"),

	/**
	 * The '<em><b>Bi Relation Breaker</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BI_RELATION_BREAKER_VALUE
	 * @generated
	 * @ordered
	 */
	BI_RELATION_BREAKER(6, "biRelationBreaker", "biRelationBreaker"),

	/**
	 * The '<em><b>Backward Relation Breaker</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BACKWARD_RELATION_BREAKER_VALUE
	 * @generated
	 * @ordered
	 */
	BACKWARD_RELATION_BREAKER(7, "backwardRelationBreaker", "backwardRelationBreaker"),

	/**
	 * The '<em><b>Forward Relation Breaker</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FORWARD_RELATION_BREAKER_VALUE
	 * @generated
	 * @ordered
	 */
	FORWARD_RELATION_BREAKER(8, "forwardRelationBreaker", "forwardRelationBreaker"),

	/**
	 * The '<em><b>Source Destroyer</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SOURCE_DESTROYER_VALUE
	 * @generated
	 * @ordered
	 */
	SOURCE_DESTROYER(9, "sourceDestroyer", "sourceDestroyer"),

	/**
	 * The '<em><b>View Destroyer</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VIEW_DESTROYER_VALUE
	 * @generated
	 * @ordered
	 */
	VIEW_DESTROYER(10, "viewDestroyer", "viewDestroyer"), /**
	 * The '<em><b>Backward Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BACKWARD_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	BACKWARD_VALUE(11, "backwardValue", "backwardValue"), /**
	 * The '<em><b>Forward Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FORWARD_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	FORWARD_VALUE(12, "forwardValue", "forwardValue");

	/**
	 * The '<em><b>Relation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RELATION
	 * @model name="relation"
	 * @generated
	 * @ordered
	 */
	public static final int RELATION_VALUE = 0;

	/**
	 * The '<em><b>Checker</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CHECKER
	 * @model name="checker"
	 * @generated
	 * @ordered
	 */
	public static final int CHECKER_VALUE = 1;

	/**
	 * The '<em><b>Adjuster</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ADJUSTER
	 * @model name="adjuster"
	 * @generated
	 * @ordered
	 */
	public static final int ADJUSTER_VALUE = 2;

	/**
	 * The '<em><b>Bi Relation Creator</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BI_RELATION_CREATOR
	 * @model name="biRelationCreator"
	 * @generated
	 * @ordered
	 */
	public static final int BI_RELATION_CREATOR_VALUE = 3;

	/**
	 * The '<em><b>Backward Relation Creator</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BACKWARD_RELATION_CREATOR
	 * @model name="backwardRelationCreator"
	 * @generated
	 * @ordered
	 */
	public static final int BACKWARD_RELATION_CREATOR_VALUE = 4;

	/**
	 * The '<em><b>Forward Relation Creator</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FORWARD_RELATION_CREATOR
	 * @model name="forwardRelationCreator"
	 * @generated
	 * @ordered
	 */
	public static final int FORWARD_RELATION_CREATOR_VALUE = 5;

	/**
	 * The '<em><b>Bi Relation Breaker</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BI_RELATION_BREAKER
	 * @model name="biRelationBreaker"
	 * @generated
	 * @ordered
	 */
	public static final int BI_RELATION_BREAKER_VALUE = 6;

	/**
	 * The '<em><b>Backward Relation Breaker</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BACKWARD_RELATION_BREAKER
	 * @model name="backwardRelationBreaker"
	 * @generated
	 * @ordered
	 */
	public static final int BACKWARD_RELATION_BREAKER_VALUE = 7;

	/**
	 * The '<em><b>Forward Relation Breaker</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FORWARD_RELATION_BREAKER
	 * @model name="forwardRelationBreaker"
	 * @generated
	 * @ordered
	 */
	public static final int FORWARD_RELATION_BREAKER_VALUE = 8;

	/**
	 * The '<em><b>Source Destroyer</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SOURCE_DESTROYER
	 * @model name="sourceDestroyer"
	 * @generated
	 * @ordered
	 */
	public static final int SOURCE_DESTROYER_VALUE = 9;

	/**
	 * The '<em><b>View Destroyer</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VIEW_DESTROYER
	 * @model name="viewDestroyer"
	 * @generated
	 * @ordered
	 */
	public static final int VIEW_DESTROYER_VALUE = 10;

	/**
	 * The '<em><b>Backward Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BACKWARD_VALUE
	 * @model name="backwardValue"
	 * @generated
	 * @ordered
	 */
	public static final int BACKWARD_VALUE_VALUE = 11;

	/**
	 * The '<em><b>Forward Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FORWARD_VALUE
	 * @model name="forwardValue"
	 * @generated
	 * @ordered
	 */
	public static final int FORWARD_VALUE_VALUE = 12;

	/**
	 * An array of all the '<em><b>Derived Rule Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DerivedRuleKind[] VALUES_ARRAY =
		new DerivedRuleKind[] {
			RELATION,
			CHECKER,
			ADJUSTER,
			BI_RELATION_CREATOR,
			BACKWARD_RELATION_CREATOR,
			FORWARD_RELATION_CREATOR,
			BI_RELATION_BREAKER,
			BACKWARD_RELATION_BREAKER,
			FORWARD_RELATION_BREAKER,
			SOURCE_DESTROYER,
			VIEW_DESTROYER,
			BACKWARD_VALUE,
			FORWARD_VALUE,
		};

	/**
	 * A public read-only list of all the '<em><b>Derived Rule Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DerivedRuleKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Derived Rule Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DerivedRuleKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DerivedRuleKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Derived Rule Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DerivedRuleKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DerivedRuleKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Derived Rule Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DerivedRuleKind get(int value) {
		switch (value) {
			case RELATION_VALUE: return RELATION;
			case CHECKER_VALUE: return CHECKER;
			case ADJUSTER_VALUE: return ADJUSTER;
			case BI_RELATION_CREATOR_VALUE: return BI_RELATION_CREATOR;
			case BACKWARD_RELATION_CREATOR_VALUE: return BACKWARD_RELATION_CREATOR;
			case FORWARD_RELATION_CREATOR_VALUE: return FORWARD_RELATION_CREATOR;
			case BI_RELATION_BREAKER_VALUE: return BI_RELATION_BREAKER;
			case BACKWARD_RELATION_BREAKER_VALUE: return BACKWARD_RELATION_BREAKER;
			case FORWARD_RELATION_BREAKER_VALUE: return FORWARD_RELATION_BREAKER;
			case SOURCE_DESTROYER_VALUE: return SOURCE_DESTROYER;
			case VIEW_DESTROYER_VALUE: return VIEW_DESTROYER;
			case BACKWARD_VALUE_VALUE: return BACKWARD_VALUE;
			case FORWARD_VALUE_VALUE: return FORWARD_VALUE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DerivedRuleKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DerivedRuleKind
