/**
 */
package edu.ustb.sei.mde.bigg;

import org.eclipse.emf.ecore.EEnumLiteral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Constant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bigg.EnumConstant#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getEnumConstant()
 * @model
 * @generated
 */
public interface EnumConstant extends Constant {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference.
	 * @see #setValue(EEnumLiteral)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getEnumConstant_Value()
	 * @model
	 * @generated
	 */
	EEnumLiteral getValue();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.EnumConstant#getValue <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(EEnumLiteral value);

} // EnumConstant
