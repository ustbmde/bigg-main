package edu.ustb.sei.mde.bigg.generator

import edu.mit.csail.sdg.alloy4.A4Reporter
import edu.mit.csail.sdg.alloy4.ErrorWarning
import edu.mit.csail.sdg.ast.Command
import edu.mit.csail.sdg.ast.Module
import edu.mit.csail.sdg.parser.CompUtil
import edu.mit.csail.sdg.translator.A4Options
import edu.mit.csail.sdg.translator.A4Solution
import edu.mit.csail.sdg.translator.TranslateAlloyToKodkod
import org.eclipse.xtext.xbase.lib.Procedures.Procedure2
import java.io.PrintStream
import java.util.List
import java.io.File
import edu.mit.csail.sdg.alloy4viz.VizGUI

class AlloyUtil {
	
	static val INSTANCE_FOLDER = 'instances'
	
	var totalCmd = 0
	var verifiedCmd = 0
	var failedCmd = 0
	
	def void clearInstanceFolder() {
		val instanceFolder = new File(INSTANCE_FOLDER)
		if(instanceFolder.exists) {
			instanceFolder.listFiles.forEach[
				if(it.file) it.delete
			]
		} else {
			instanceFolder.mkdir
		}
	}
	
	def static void main(String[] args) {
		val tool = new AlloyUtil
		tool.verify('test.als', 'test')[]
	}
	
	def verifyBX(String modelName, String resultFileName) {
		clearInstanceFolder
		val fileOutput = new PrintStream(new File(resultFileName))
		modelName.verifyBX(fileOutput)
		fileOutput.flush
		fileOutput.close
	}
	
	def visualizeInstance(String filename) {
		// You can then visualize the XML file by calling this:
		val actualFile = if(filename.startsWith('/')) filename else INSTANCE_FOLDER + '/' + filename
		new VizGUI(false, actualFile, null)
	}
	
	def verifyBX(String modelName, PrintStream resultPrinter) {
		val conflictFileNames = #[
			'VerificationsForCheckerConflict',
			'VerificationsForBWDCreatorConflict',
			'VerificationsForCheckerBWDCreatorConflict',
			'VerificationsForFWDCreatorConflict',
			'VerificationsForCheckerFWDCreatorConflict',
			'VerificationsForBWDBreakerConflict',
			'VerificationsForFWDBreakerConflict'
		]
		
		modelName.verifyFiles('findConflict', conflictFileNames, resultPrinter)
		
		val keepDeleteConflictFileNames = #[
			'VerificationsForBWDKeepDeleteConflict',
			'VerificationsForFWDKeepDeleteConflict'
		]
		
		modelName.verifyFiles('findKeepDeleteConflict', keepDeleteConflictFileNames, resultPrinter)
		
		val dependencyFileNames = #[
			'VerificationsForBWDCreatorBWDBreakerDependency',
			'VerificationsForBWDCreatorFWDBreakerDependency',
			'VerificationsForFWDCreatorBWDBreakerDependency',
			'VerificationsForFWDCreatorFWDBreakerDependency'
		]
		
		modelName.verifyFiles('findDependency', dependencyFileNames, resultPrinter)
	}
	
	protected def verifyFiles(String modelName, String labelPrefix, List<String> files, PrintStream resultPrinter) {
		verifyFiles(modelName, labelPrefix, files) [cmd, found|
//			if(found) {
//				resultPrinter.append('FAILED')
//			} else {
//				resultPrinter.append('PASSED')
//			}
//			resultPrinter.append('\t')
//			resultPrinter.append(cmd.label)
//			resultPrinter.append('\t')
//			if(found) resultPrinter.append(cmd.label + '_output.xml')			
//			resultPrinter.println

			if(found) {
				val betweenEndPos = cmd.label.indexOf('Between') + 'Between'.length
				val andStartPos = cmd.label.indexOf('And', betweenEndPos)
				val andEndPos = andStartPos + 'And'.length
				
				val ruleA = cmd.label.substring(betweenEndPos, andStartPos)
				val ruleB = cmd.label.substring(andEndPos)
				
				resultPrinter.append('FAILED')
				resultPrinter.append('\t')
				resultPrinter.append(cmd.label)
				resultPrinter.append('\t')
				resultPrinter.append(ruleA)
				resultPrinter.append('\t')
				resultPrinter.append(ruleB)
				resultPrinter.append('\t')
				resultPrinter.append(cmd.label + '_output.xml')		
				resultPrinter.println
			}
			
		]
	}
	
	protected def verifyFiles(String modelName, String labelPrefix, List<String> files, Procedure2<Command, Boolean> resultMsg) {
		val actualFileNames = files.map[modelName + it + '.als']
		
		for(actualFile : actualFileNames) {
			actualFile.verify(labelPrefix, resultMsg)
		}
	}
	
	def verifyConflict(String filename, PrintStream resultPrinter) {
		verify(filename, 'findConflict')[cmd, found|
			if(found) {
				resultPrinter.append('FAILED')
			} else {
				resultPrinter.append('PASSED')
			}
			resultPrinter.append('\t')
			resultPrinter.append(cmd.label)
			resultPrinter.append('\t')
			if(found) resultPrinter.append(cmd.label + '_output.xml')			
			resultPrinter.println
		]
	}
	
	def verifyDependency(String filename, PrintStream resultPrinter) {
		verify(filename, 'findDependency') [cmd, found|
			if(found) {
				resultPrinter.append('FAILED')
			} else {
				resultPrinter.append('PASSED')
			}
			resultPrinter.append('\t')
			resultPrinter.append(cmd.label)
			resultPrinter.append('\t')
			if(found) resultPrinter.append(cmd.label + '_output.xml')			
			resultPrinter.println
		]
	}
	
	def verifyKeepDeleteConflict(String filename, PrintStream resultPrinter) {
		verify(filename, 'findKeepDeleteConflict') [cmd, found|
			if(found) {
				resultPrinter.append('FAILED')
			} else {
				resultPrinter.append('PASSED')
			}
			resultPrinter.append('\t')
			resultPrinter.append(cmd.label)
			resultPrinter.append('\t')
			if(found) resultPrinter.append(cmd.label + '_output.xml')			
			resultPrinter.println
		]
	}
	
	var A4Reporter rep = new A4Reporter() {
		// For example, here we choose to display each "warning" by printing
		// it to System.out
		override void warning(ErrorWarning msg) {
			System.out.print(
				'''
				Relevance Warning:
				«(msg.toString().trim())»
				
				''')
			System.out.flush()
		}
		
		override solve(int primaryVars, int totalVars, int clauses) {
			if(totalVars===0) {
				System.err.println('This command cannot be solved due to empty var set!')
			}
		}
		
	}
	
	def verify(String filename, String labelPrefix, Procedure2<Command, Boolean> resultMsg) {
		System.out.println('''=========== Parsing+Typechecking «filename» =============''')
		var total = 0
		var verified = 0
		var failed = 0
		
		var Module world = CompUtil.parseEverything_fromFile(rep, null, filename)
		var A4Options options = new A4Options()
		options.solver = A4Options.SatSolver.SAT4J
		for (Command command : world.getAllCommands()) {
			total ++
			if (command.label.startsWith(labelPrefix)) {
				verified ++
				// Execute the command
				System.out.println('''============ Command «command»: ============''')
				var A4Solution ans = TranslateAlloyToKodkod.execute_command(rep, world.getAllReachableSigs(), command,
					options)
				// Print the outcome
				System.out.println(ans)
				// If satisfiable...
				val findInstance = 
				if (ans.satisfiable()) {
					failed ++
					val output = INSTANCE_FOLDER+'/'+command.label + '_output.xml'
					ans.writeXML(output)
					true
				} else {
					false
				}
				
				totalCmd += total
				verifiedCmd += verified
				failedCmd += failed
				
				resultMsg.apply(command, findInstance)
			}
		}
		
		System.out.println('''=========== TOTAL: «total», VERIFIED: «verified», FAILED: «failed» =============''')
		println
	}
	
}
