package edu.ustb.sei.mde.bigg.generator

class FunctionInfomation {
	public val String name;
	public val int parameterCount;

	new(String n, int p) {
		name = n
		parameterCount = p
	}
	
	override hashCode() {
		(name===null?0:name.hashCode) * 32 + parameterCount
	}
	
	override equals(Object obj) {
		if(obj instanceof FunctionInfomation) {
			name==obj.name && parameterCount===obj.parameterCount
		} else false
	}
	
}