package edu.ustb.sei.mde.bigg.generator

import edu.ustb.sei.mde.bigg.Argument
import edu.ustb.sei.mde.bigg.BiggFactory
import edu.ustb.sei.mde.bigg.BiggPackage
import edu.ustb.sei.mde.bigg.Condition
import edu.ustb.sei.mde.bigg.Constant
import edu.ustb.sei.mde.bigg.Edge
import edu.ustb.sei.mde.bigg.FunctionCall
import edu.ustb.sei.mde.bigg.Node
import edu.ustb.sei.mde.bigg.Pattern
import edu.ustb.sei.mde.bigg.Property
import edu.ustb.sei.mde.bigg.Rule
import edu.ustb.sei.mde.bigg.TriplePattern
import edu.ustb.sei.mde.bigg.Tuple
import edu.ustb.sei.mde.bigg.Value
import edu.ustb.sei.mde.bigg.Variable
import edu.ustb.sei.mde.bigg.VariableRef
import java.util.ArrayList
import java.util.Collection
import java.util.Collections
import java.util.HashMap
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Set
import java.util.function.Function
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EcoreFactory
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.util.EcoreUtil.Copier
import edu.ustb.sei.mde.bigg.DerivedRuleKind
import edu.ustb.sei.mde.bigg.DerivedRule

class BiGGRuleUtil {
	def getReferredNodes(Variable variable) {
		val rule = variable.eContainer as Rule
		(rule.lhs.allNodes + rule.rhs.allNodes).filter[n|n.properties.exists[p|p.value instanceof VariableRef && (p.value as VariableRef).variable===variable]]
	}
	
	def boolean isStructureIdentical(Rule rule) {
		val lhsNodes = rule.lhs.allNodes
		val lhsEdges = rule.lhs.allEdges
		
		val rhsNodes = rule.rhs.allNodes
		val rhsEdges = rule.rhs.allEdges
		
		if(lhsNodes.size===rhsNodes.size && lhsEdges.size===rhsEdges.size) {
			val lhsNodeMap = lhsNodes.toMap[name]
			val nodeMatched = rhsNodes.forall[
				val ln = lhsNodeMap.get(it.name)
				ln.type===it.type
			]
			if(nodeMatched) {
				rhsEdges.forall[
					lhsEdges.exists[le|
						val mls = lhsNodeMap.get(it.source.name)
						val mlt = lhsNodeMap.get(it.target.name)
						le.type===it.type && mls===le.source && mlt===le.target
					]
				]
			} else false
		} else false
	}
	
	def TriplePattern findInvertibleNAC(DerivedRule rule) {
		if(rule.ruleKind === DerivedRuleKind.SOURCE_DESTROYER) {
			rule.nacs.filter[it instanceof TriplePattern].map[it as TriplePattern].findFirst[it.isForwardable]
		} else if(rule.ruleKind === DerivedRuleKind.VIEW_DESTROYER) {
			rule.nacs.filter[it instanceof TriplePattern].map[it as TriplePattern].findFirst[it.target.isEmptyPattern && it.correspondence.isBridgePattern]
		} else null
	}
	
	def boolean isForwardable(Condition condition) {
		!condition.isInvariant && (switch condition {
			TriplePattern : {
				condition.source.isEmptyPattern && condition.correspondence.isBridgePattern
			}
			FunctionCall : {
				(condition.callee=='=') &&
					condition.arguments.size===2 &&
						{
							val rule = condition.eContainer as Rule
							if(condition.arguments.get(0).isVariableOrTupleOfVariables && condition.arguments.get(1) instanceof FunctionCall) {
								rule.isForwardableCondition(condition.arguments.get(0), condition.arguments.get(1) as FunctionCall)
							} else if (condition.arguments.get(1).isVariableOrTupleOfVariables && condition.arguments.get(0) instanceof FunctionCall) {
								rule.isForwardableCondition(condition.arguments.get(1), condition.arguments.get(0) as FunctionCall)
							} else false
						}
			}
			default: false
		})
	}
	
	def boolean isForwardableCondition(Rule rule, Argument postValue, FunctionCall bigul) {
		if(bigul.arguments.size!==2) return false
		
		val varUse = rule.collectVariableBindings
		val funName = bigul.callee
		val srcValue = bigul.arguments.get(0)
		val tarValue = bigul.arguments.get(1)
		
		if(tarValue.isVariableOrTupleOfVariables) {
			if(!(switch tarValue {
				VariableRef :  {
					val bindings = varUse.get(tarValue.variable)
					bindings.size > 0 && bindings.forall[
						val pattern = it.eContainer.eContainer as Pattern
						rule.lhs.target===pattern || rule.rhs.target===pattern
					]
				}
				Tuple : tarValue.arguments.forall[
					val bindings = varUse.get((it as VariableRef).variable)
					bindings.size > 0 && bindings.forall[
						val pattern = it.eContainer.eContainer as Pattern
						rule.lhs.target===pattern || rule.rhs.target===pattern
					]
				]
			})) {
				return false
			}
		} else return false
		
		if(postValue instanceof VariableRef) {
			if(srcValue instanceof VariableRef) {
				val srcBindings = varUse.get(srcValue.variable)
				val postBindings = varUse.get(postValue.variable)
				return srcBindings.forall[rule.lhs.source===it.eContainer.eContainer] && 
					postBindings.forall[rule.rhs.source===it.eContainer.eContainer]
					&& {
						val srcBindingsInfo = srcBindings.map[(it.eContainer as Node).name -> it.type].toSet
						val postBindingsInfo = postBindings.map[(it.eContainer as Node).name -> it.type].toSet
						
						srcBindingsInfo.equals(postBindingsInfo)
					}
			} else return false
		} else {
			val postTuple = postValue as Tuple
			if(srcValue instanceof Tuple && srcValue.isVariableOrTupleOfVariables) {
				val srcTuple = srcValue as Tuple
				
				if(postTuple.arguments.size!=srcTuple.arguments.size) {
					return false
				} else {
					return (0..<srcTuple.arguments.size).forall[i|
						val s = srcTuple.arguments.get(i) as VariableRef
						val p = postTuple.arguments.get(i) as VariableRef
						
						val srcBindings = varUse.get(s.variable)
						val postBindings = varUse.get(p.variable)
						return srcBindings.forall[rule.lhs.source===it.eContainer.eContainer] && 
							postBindings.forall[rule.rhs.source===it.eContainer.eContainer]
							&& {
								val srcBindingsInfo = srcBindings.map[(it.eContainer as Node).name -> it.type].toSet
								val postBindingsInfo = postBindings.map[(it.eContainer as Node).name -> it.type].toSet
								
								srcBindingsInfo.equals(postBindingsInfo)
							}
					]
				}
			} else return false
		}
	}
	
	def collectVariableBindings(Rule rule) {
		rule.eAllContents.filter [
			it instanceof Property &&
				(it as Property).value instanceof VariableRef
		].map[(it as Property)].groupBy[(it.value as VariableRef).variable]
	}
	
	def isVariableOrTupleOfVariables(Argument arg) {
		switch arg {
			VariableRef:true
			Tuple:arg.arguments.forall[it instanceof VariableRef]
			default: false
		}
	}
	
	
	
//	def boolean isBidirectional(Condition condition) {
//		if(condition instanceof TriplePattern) {
//			condition.source.isEmptyPattern && condition.target.isEmptyPattern && condition.correspondence.isBridgePattern
//		} else false
//	}
	
//	def boolean isSourceVariable(Argument argument, boolean rhs) {
//		switch argument {
//			VariableRef : {
//				val inRightPos = if(rhs) argument.variable.isDefinedIn(BiggPackage.Literals.RULE__RHS) else argument.variable.isDefinedIn(BiggPackage.Literals.RULE__LHS)
//				val inRightBinding = (argument.variable.triplePattern.source?.nodes ?: Collections.emptyList).exists[it.properties.exists[value instanceof VariableRef && (value as VariableRef).variable===argument.variable]]
//				inRightPos && inRightBinding
//			}
//			Tuple : argument.arguments.forall[arg | arg instanceof VariableRef && (arg as VariableRef).variable.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__SOURCE)]
//			default : false
//		}
//	}
	
//	private def findBindings(Variable v, ) {
//		
//	}

	def isCompleteRelation(Node corrNode) {
		val pattern = corrNode.eContainer as Pattern
		val nodeClass = corrNode.type
		
		val allRefs = nodeClass.EAllReferences.toSet
		val allEdges = pattern.edges.filter[it.source===corrNode].map[it.type].toSet
		
		allRefs == allEdges
	}
	
	def boolean isEmptyPattern(Pattern pattern) {
		pattern===null || (pattern.nodes.empty && pattern.edges.empty) 
	}
	
	def boolean isBridgePattern(Pattern pattern) {
		if(pattern===null) return false
		if(pattern.nodes.size!==1) return false
		val patEdges = pattern.edges.groupBy[e| e.source]
		if(patEdges.keySet != pattern.nodes.toSet) return false
		else {
			pattern.nodes.forall[n|
				val eClass = n.type
				val edges = patEdges.get(n)
				val covered = edges.map[e|e.type].toSet
				return covered==(eClass.EAllReferences.toSet)
			]
		}
	}
	
	def boolean isDefinedIn(Node n, TriplePattern pattern) {
		n.eContainer.eContainer===pattern
	}
	
	def boolean isDefinedIn(EObject o, EObject container) {
		if(o===null) false
		else if(o===container) true
		else o.eContainer.isDefinedIn(container)
	}
	
	def boolean isDefinedIn(EObject o, EReference ref) {
		if(o===null) false
		else if(o.eContainmentFeature === ref) true
		else o.eContainer.isDefinedIn(ref)
	}
	
//	def isBiGUL(Argument arg) {
//		if(arg instanceof FunctionCall) {
//			arg.arguments.size===2 && 
//				arg.arguments.get(0).isSourceVariable(false) && 
//				arg.arguments.get(1).isTargetVariable
//		} else false
//	}
	
	def isSourceNode(Node node) {
		node.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__SOURCE)
	}
	
	def TriplePattern getTriplePattern(EObject object) {
		if(object===null) null
		else if(object instanceof TriplePattern) object
		else object.eContainer.triplePattern
	}
	
	def Rule getRule(EObject object) {
		if(object===null) null
		else if(object instanceof Rule) object
		else object.eContainer.rule
	}
	
//	def boolean isTargetVariable(Argument argument) {
//		switch argument {
//			VariableRef : {
//				val inRightPos = argument.variable.isDefinedIn(BiggPackage.Literals.RULE__LHS)
//				val inRightBinding = (argument.variable.triplePattern.target?.nodes ?: Collections.emptyList).exists[it.properties.exists[value instanceof VariableRef && (value as VariableRef).variable===argument.variable]]
//				inRightPos && inRightBinding
//			}
//			Tuple : argument.arguments.forall[arg | arg instanceof VariableRef && (arg as VariableRef).variable.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__TARGET)]
//			default : false
//		}
//	}

	
	
	def getDeletedNodes(Rule rule) {
		val diff = rule.diffRule
		return  diff.leftDelta.source.nodes+diff.leftDelta.correspondence.nodes+diff.leftDelta.target.nodes
	}
	
	def getCreatedNodes(Rule rule) {
		val diff = rule.diffRule
		return  diff.rightDelta.source.nodes+diff.rightDelta.correspondence.nodes+diff.rightDelta.target.nodes
	}
	
	def getReferredVariables(TriplePattern pattern) {
		if(pattern===null) Collections.emptyList
		
		val vars = new HashSet;
		
		pattern.allNodes.forEach[it.eAllContents.forEach[
			if(it instanceof VariableRef) vars += it.variable
		]]
		
		return vars
	}
	
	def void reduceVariables(Rule rule) {
		val varOcc = rule.eAllContents.filter[it instanceof VariableRef].map[it as VariableRef].groupBy[variable]

		val removedVars = rule.variables.filter[
			val occ = varOcc.get(it)
			if(occ===null || occ.empty || occ.size===1) true
			else {
				val reducedOcc = occ.fold(new ArrayList<VariableRef>)[reduced, cur|
					var uniqueOcc = true
					val cContainer = cur.eContainer
					if (cContainer instanceof Property) {
						for (r : reduced) {
							val rContainer = r.eContainer
							if(rContainer instanceof Property) {
								if(cContainer.type===rContainer.type && (cContainer.eContainer as Node).name==(rContainer.eContainer as Node).name) {
									uniqueOcc = false
								}
							}
						}
					}
					if(uniqueOcc) {
						reduced.add(cur)
					}
					return reduced
				]
				
				reducedOcc.size <= 1
			}
		].toSet
		
		rule.variables.removeAll(removedVars)
		
		rule.lhs.allNodes.forEach[n|
			n.properties.removeIf[
				if(value instanceof VariableRef) {
					removedVars.contains((value as VariableRef).variable)
				} else false
			]
		]
		
		rule.rhs.allNodes.forEach[n|
			n.properties.removeIf[
				if(value instanceof VariableRef) {
					removedVars.contains((value as VariableRef).variable)
				} else false
			]
		]
		
		// this should not happen 
		rule.nacs.removeIf[
			it.eAllContents.exists[it instanceof VariableRef && removedVars.contains((it as VariableRef).variable)]
		]
	}
	
	def extractRelation(Rule rule, Node corNode) {
		val relation = {
			val Copier copier = new Copier() 
			val result = copier.copy(rule) as Rule
			copier.copyReferences()
			
			val copiedCorNode = copier.get(corNode)
			
			copiedCorNode -> result
		}
		
		val copiedCorNode = relation.key as Node
		
		val relationBody = BiggFactory.eINSTANCE.createTriplePattern
		relationBody.source = BiggFactory.eINSTANCE.createPattern
		relationBody.correspondence = BiggFactory.eINSTANCE.createPattern
		relationBody.target = BiggFactory.eINSTANCE.createPattern
		
		val triplePat = copiedCorNode.triplePattern
		if(triplePat.eContainmentFeature===BiggPackage.Literals.RULE__LHS || triplePat.eContainmentFeature===BiggPackage.Literals.RULE__RHS) {
			val corEdges = triplePat.correspondence.edges.filter[it.source===copiedCorNode]
			val srcNodes = triplePat.source?.nodes?.filter[n|corEdges.exists[e|e.target===n]] ?: emptyList
			val tarNodes = triplePat.target?.nodes?.filter[n|corEdges.exists[e|e.target===n]] ?: emptyList
			
			val srcEdges = triplePat.source?.edges?.filter[e|srcNodes.contains(e.source) && srcNodes.contains(e.target)] ?: emptyList
			val tarEdges = triplePat.target?.edges?.filter[e|srcNodes.contains(e.source) && srcNodes.contains(e.target)] ?: emptyList
			
			relationBody.source.nodes += srcNodes
			relationBody.source.edges += srcEdges
			
			relationBody.correspondence.nodes += copiedCorNode
			relationBody.correspondence.edges += corEdges
			
			relationBody.target.nodes += tarNodes
			relationBody.target.edges += tarEdges
			
		} else { // in nac
			val corEdges = triplePat.correspondence.edges.filter[it.source===copiedCorNode]
			val srcNodes = ((triplePat.source?.nodes?:emptyList)+(rule.lhs?.source?.nodes ?: emptyList)).filter[n|corEdges.exists[e|e.target===n]]
			val tarNodes = ((triplePat.target?.nodes?:emptyList)+(rule.lhs?.target?.nodes ?: emptyList)).filter[n|corEdges.exists[e|e.target===n]]
			
			val srcEdges = ((triplePat.source?.edges?:emptyList)+(rule.lhs?.source?.edges ?: emptyList)).filter[e|srcNodes.contains(e.source) && srcNodes.contains(e.target)]
			val tarEdges = ((triplePat.target?.edges?:emptyList)+(rule.lhs?.target?.edges ?: emptyList)).filter[e|srcNodes.contains(e.source) && srcNodes.contains(e.target)]
			
			relationBody.source.nodes += srcNodes
			relationBody.source.edges += srcEdges
			
			relationBody.correspondence.nodes += copiedCorNode
			relationBody.correspondence.edges += corEdges
			
			relationBody.target.nodes += tarNodes
			relationBody.target.edges += tarEdges
		}
		
		relation.value.lhs = relationBody
		relation.value.rhs = null
		
		return relation.value
	}
	
	def getFocusedRelationNodes(Rule rule) {
		if(rule.isStructureIdentical) {
			return null
		} else {			
			val nac = rule.nacs.findFirst[it.isForwardable]
			if(nac===null) {
				val created = rule.createdNodes
				val deleted = rule.deletedNodes
				
				val delCors = rule.lhs?.correspondence?.nodes?.filter[deleted.contains(it)] ?: emptyList
				val creCors = rule.rhs?.correspondence?.nodes?.filter[created.contains(it)] ?: emptyList
				
				return (creCors+delCors).toSet
			} else {
				if(nac instanceof TriplePattern)
					return (nac as TriplePattern).correspondence.nodes.toSet
				else {
					if (rule.isStructureIdentical) rule.lhs.correspondence.nodes
					else emptyList
				}
			}
		}
	}
	
	extension BiggFactory = BiggFactory.eINSTANCE
	
	def normalizeRule(Rule rule, boolean keepRelationalValueNAC) {
		if(rule.lhs===null) rule.lhs = createTriplePattern
		if(rule.rhs===null) rule.rhs = createTriplePattern
		rule.lhs.normalizeTriplePattern
		rule.rhs.normalizeTriplePattern
		rule.nacs.forEach[if(it instanceof TriplePattern) it.normalizeTriplePattern]
		if(!keepRelationalValueNAC) rule.removeAlignmentValueCondition
		rule.reduceVariables
		rule.completePropertyMapping
		rule.normailizeNAC
	}
	
	def boolean removeAlignmentValueCondition(Rule rule) {
		val relationValueNACs = rule.nacs.filter[it instanceof FunctionCall].map[it as FunctionCall].filter[
			! it.eAllContents.filter[it instanceof VariableRef].map[it as VariableRef].map[
				val head = it.variable.referredNodes.head
				if(head===null) null
				else head.eContainer.eContainmentFeature
			].fold((null as EReference)->true)[result, ref|
				if(ref===null) result
				else if(result.value==false) result
				else ref->(ref===result.key)
			].value
		].toSet
		
		rule.nacs.removeAll(relationValueNACs)
	}
	
	private def void completePropertyMapping(Rule rule) {
		rule.rhs.allNodes.forEach[
			val name = it.name
			val lhsNode = rule.lhs.getNode(name)
			if(lhsNode!==null) {
				lhsNode.properties.forEach[lp|
					if(!it.properties.exists[rp| lp.type===rp.type]) {
						it.properties += lp.copy
					}
				]
			}
		]
	}
	
	def boolean connecting(Edge e, Node n) {
		e.source===n || e.target === n
	}
	
	private def void normailizeNAC(Rule rule) {
		val nacPat = rule.nacs.filter[it instanceof TriplePattern].map[it as TriplePattern]
		val newPat = new ArrayList<TriplePattern>
		nacPat.forEach[pat|
			if(pat.correspondence.nodes.size>1) {
				val patEdges = pat.correspondence.edges.groupBy[it.source]
				
				val retained = pat.correspondence.nodes.filter[
					val covered = patEdges.get(it).map[type].toSet
					covered.equals(it.eClass.EAllReferences)
				].toSet
				
				val toBeMovedOut = pat.allNodes.toSet
				toBeMovedOut.removeAll(retained)
				val allEdges = pat.allEdges
				
				var oldSize = 0;
				do {
					oldSize = retained.size
					val retainedDelta = toBeMovedOut.filter[n|
						allEdges.exists[e|
							e.connecting(n) && (retained.contains(e.source) || retained.contains(e.target))
						]
					].toSet
					retained.addAll(retained)
					toBeMovedOut.removeAll(retainedDelta)
				}while(oldSize!=retained.size)
				
				val toBeMovedEdges = allEdges.filter[toBeMovedOut.contains(it.source) || toBeMovedOut.contains(it.target)].toSet
				
				newPat += createTriplePattern => [
					source = createPattern => [
						nodes += pat.source.nodes.filter[toBeMovedOut.contains(it)]
						edges += pat.source.edges.filter[toBeMovedEdges.contains(it)]
					]
					correspondence = createPattern => [
						nodes += pat.correspondence.nodes.filter[toBeMovedOut.contains(it)]
						edges += pat.correspondence.edges.filter[toBeMovedEdges.contains(it)]
					]
					target = createPattern => [
						nodes += pat.target.nodes.filter[toBeMovedOut.contains(it)]
						edges += pat.target.edges.filter[toBeMovedEdges.contains(it)]
					]
				]
			}
		]
	}
	
	private def findRelatedNodes(Pattern pattern, Collection<Node> corr, Collection<Edge> corEdges) {
		val realtedNodes = pattern.nodes.filter[corEdges.exists[e|
			e.target===it && !corr.contains(e.source)
		]].toSet
		
		var oldSize = 0
		do {
			oldSize = realtedNodes.size
			realtedNodes.removeIf[
				pattern.edges.exists[e|
					(e.source===it && !realtedNodes.contains(e.target))
					|| (e.target===it && !realtedNodes.contains(e.source))
				]
			]
		}while(oldSize!==realtedNodes.size);
		
		return realtedNodes
	}
	
	def <T extends EObject> T copy(T obj) {
		EcoreUtil.copy(obj) as T
	}
	
	def <T extends EObject> Collection<T> copyAll(Collection<T> col) {
		EcoreUtil.copyAll(col)
	}
	
	private def normalizeTriplePattern(TriplePattern tp) {
		if(tp.source===null) tp.source = createPattern
		if(tp.correspondence===null) tp.correspondence = createPattern
		if(tp.target===null) tp.target = createPattern
	}
	
	def findNode(Pattern pattern, Node node) {
		pattern?.nodes?.findFirst[it.name==node.name]
	}
	
	def isValueRule(Rule normalizedRule) {
		val forwardable = normalizedRule.nacs.filter[it instanceof FunctionCall].filter[it.isForwardable]
		if(forwardable.size!==1) {
			false -> null
		} else {
			val diff = normalizedRule.diffRule;
			(diff.leftDelta.isSourceValueChangedOnly && diff.rightDelta.isSourceValueChangedOnly) -> diff
		}
	}
	
	def isSourceIntegrityRule(Rule normalizedRule) {
		val errNac = normalizedRule.nacs.filter[it.error].toList;
		
		(errNac.size===1 && {
			val err = errNac.get(0)
			err.isRelationNAC
		}) -> null
	}
	
	/**
	 * A rule destroys the source because if it does not do it, the get will create the view
	 * 
	 * Condition:
	 * 1. A unique NAC triple pattern is invertible
	 * 2. The rule changes source only (LHS + NAC implies the relation)
	 * 
	 * In forward direction, a source destroyer derives a relation creator
	 */
	def isSourceDestroyer(Rule normalizedRule) {
		val forwardable = normalizedRule.nacs.filter[it.invariant===false].filter[it instanceof TriplePattern].filter[it.isForwardable]
		if(forwardable.size!==1) {
			false -> null
		} else {
			val diff = normalizedRule.diffRule;
			(!(diff.leftDelta.source.isEmpty && diff.rightDelta.source.isEmpty) && diff.leftDelta.correspondence.isEmpty && diff.rightDelta.correspondence.isEmpty) -> diff
		}
	}
	
	/**
	 * A rule that creates a relation if it creates one correspondence node
	 * 
	 * Condition:
	 * 1. The rule creates one correspondence node and does not delete any relations (RHS implies the relation)
	 * 
	 * A relation creator may be bidirectional if it creates a cor node only (maybe omitted)
	 * 
	 * PS.
	 * 1. NACs that do not conflict with LHS->RHS must be used as preconditions of the forward rule
	 * 2. Other correspondence nodes are context, which should not be considered to be invertible
	 * 
	 * Because the rule must be idem, so the forward rule should not trigger the backward one
	 * 
	 * In forward direction, a relation creator may derive a view destroyer;
	 * A bidirectional relation creator is copied to forward transformation
	 */
	def isRelationCreator(Rule normalizedRule) {
		val diff = normalizedRule.diffRule
		return (diff.leftDelta.correspondence.empty && diff.rightDelta.correspondence.nodes.size===1 &&
			diff.rightDelta.correspondence.nodes.get(0).completeRelation
		)->diff
	}
	
	def isBidirectionalRelationCreator(Rule normalizedRule) {
		val result = normalizedRule.isRelationCreator
		return (if (result.key) {
			(!result.value.isSourceChanged && !result.value.isTargetChanged)
		} else
			false) -> result
	}
	
	/**
	 * A rule that deletes a relation if it deletes one correspondence node
	 * 
	 * Condition:
	 * 1. The rule deletes one correspondence node and does not create any relations (LHS implies the relation)
	 * 2. There are relational NACs because there is not view deletion so all view nodes must be referred by those NACs
	 * 3. TODO: relationNAC and *negative view filter condition* can be inverted
	 * 
	 * PS.
	 * 1. Relational NACs must be considered as context combined with LHS
	 * 2. Other correspondence nodes are context, which should not be considered to be invertible
	 * 3. Other NACs will be used to constrain forward rules (we derive minimal forward rules only)
	 * 
	 * A relation breaker may be bidirectional if it deletes a cor node only
	 * 
	 * We ensure that all derived forward rules do not trigger backward rules because we use NAC as PAC
	 * All derived relation with NACs is valid if it never triggers other backward rules 
	 * 
	 * Relations minimize if rel_A => rel_B, rel_A can be removed
	 * 
	 * In forward direction, a relation breaker derives another relation breaker;
	 * A bidirectional relation breaker is copied to the forward transformation
	 */
	def isRelationBreaker(Rule normalizedRule) {
		val diff = normalizedRule.diffRule
		
		return (diff.rightDelta.correspondence.empty && diff.leftDelta.correspondence.nodes.size===1
			&& normalizedRule.nacs.exists[it.relationNAC]
			&& diff.leftDelta.correspondence.nodes.get(0).completeRelation
		) -> diff
	}
	
	def isRelationNAC(Condition condition) {
		if(condition instanceof TriplePattern) {
			val corNodes = condition.correspondence?.nodes ?: <Node>emptyList
//			val corEdges = condition.correspondence?.edges ?: <Edge>emptyList
//			val edgeGrp = corEdges.groupBy[it.source]
			!condition.isInvariant && corNodes.forall[n|
//				val edges = edgeGrp.getOrDefault(n, <Edge>emptyList)
//				val covered = edges.map[it.type].toSet
//				covered==n.eClass.EAllReferences.toSet
				n.completeRelation
			]
		} else false
	}
	
	def isBidirectionalRelationBreaker(Rule normalizedRule) {
		val result = normalizedRule.isRelationBreaker
		return (if (result.key)
			(!result.value.isSourceChanged && !result.value.isTargetChanged)
		else
			false) -> result
	}
	
	/**
	 * A structural rule is a rule that does not change correspondences
	 */
	def isStructuralRule(Rule normalizedRule) {
		val diff = normalizedRule.diffRule
		
		if ((!diff.leftDelta.empty || !diff.rightDelta.empty) && diff.leftDelta.correspondence.empty &&
			diff.rightDelta.correspondence.empty && normalizedRule.nacs.forall [
				it instanceof TriplePattern && (it as TriplePattern).correspondence.emptyPattern
			])
			true
		else
			false
	}
	
	val Map<Rule, RuleDelta> ruleDiffMap = new HashMap
	
	def getRuleDiffMap() {
		ruleDiffMap
	}
	
	def diffRule(Rule rule) {
		ruleDiffMap.computeIfAbsent(rule) [
			diffTripleGraph(rule.lhs, rule.rhs)		
		]
	}
	
	def clearCache() {
		ruleDiffMap.clear
	}
	
	/**
	 * compute right-left
	 */
	def diffTripleGraph(TriplePattern left, TriplePattern right) {
		val diff = new RuleDelta
		
		val allLeftNodes = left?.allNodes ?: Collections.emptyList
		val allLeftEdges = left?.allEdges ?: Collections.emptyList
		
		val allRightNodes = right?.allNodes ?: Collections.emptyList
		val allRightEdges = right?.allEdges ?: Collections.emptyList
		
		diff(diff.leftDelta, allLeftNodes, allLeftEdges, allRightNodes, allRightEdges)
		diff(diff.rightDelta, allRightNodes, allRightEdges, allLeftNodes, allLeftEdges)
		
		diff.unchangedNodes += (allLeftNodes+allRightNodes).filter[!it.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__TARGET)].filter[!diff.leftDelta.contains(it) && !diff.rightDelta.contains(it)]
		diff.unchangedEdges += (allLeftEdges+allRightEdges).filter[!it.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__TARGET)].filter[!diff.leftDelta.contains(it) && !diff.rightDelta.contains(it)]
		
		
		return diff
	}
	
	protected def void diff(TripleGraphFragment delta, List<Node> allLeftNodes, List<Edge> allLeftEdges, List<Node> allRightNodes, List<Edge> allRightEdges) {
		allLeftNodes.forEach[ln|
			val rn = allRightNodes.findFirst[it.name==ln.name]
			if(rn===null) {
				if(ln.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__SOURCE)) {
					delta.source.nodes += ln
					delta.source.properties+=ln.properties
				} else if(ln.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__CORRESPONDENCE)) {
					delta.correspondence.nodes += ln
					delta.correspondence.properties+=ln.properties
				} else if(ln.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__TARGET)) {
					delta.target.nodes += ln
					delta.target.properties+=ln.properties
				}
			} else {
				val props = if(ln.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__SOURCE)) {
					delta.source.properties
				} else if(ln.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__CORRESPONDENCE)) {
					delta.correspondence.properties
				} else if(ln.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__TARGET)) {
					delta.target.properties
				}
				
				ln.properties.forEach[lp|
					val rp = rn.properties.findFirst[it.type===lp.type && it.value.isSameAs(lp.value)]
					if(rp===null) {
						props += lp
					}
				]
			}
		]
		
		allLeftEdges.forEach[le|
			val re = allRightEdges.findFirst[re | le.type===re.type && le.source.name==re.source.name && le.target.name==re.target.name]
			if(re===null) {
				if(le.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__SOURCE)) {
					delta.source.edges += le
				} else if(le.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__CORRESPONDENCE)) {
					delta.correspondence.edges += le
				} else if(le.isDefinedIn(BiggPackage.Literals.TRIPLE_PATTERN__TARGET)) {
					delta.target.edges += le
				}
			}
		]
	}
	
	def boolean isSameAs(Value a, Value b) {
		if(a.class===b.class) {
			switch a {
				VariableRef : a.variable===(b as VariableRef).variable
				Constant : a.stringRepresentation == (b as Constant).stringRepresentation
				default: false
			}
		} else false
	}
	
	def createRuleAnnotation(String key, EObject... objs) {
		EcoreFactory.eINSTANCE.createEAnnotation => [
			source = 'http://www.ustb.edu.cn/sei/mde/bigg/rule/'+key
			references += objs
		]
	}
	
	static public val RELATION_KEY = 'http://www.ustb.edu.cn/sei/mde/bigg/relation'
	
	def createRelationAnnotation(Pair<String,String>... details) {
		EcoreFactory.eINSTANCE.createEAnnotation => [a|
			a.source = RELATION_KEY
			details.forEach[pair|
				a.details.put(pair.key, pair.value)
			]
		]
	}
	
	def mergeTo(TriplePattern additive, TriplePattern result) {
		val copy = if(additive.eContainer===null) additive else {additive.copy => [
			it.allNodes.forEach[it.name = it.name + '_']
		]}
		
		copy.source.mergeTo(result.source)
		copy.correspondence.mergeTo(result.correspondence)
		copy.target.mergeTo(result.target)
	}
	
	def mergeTo(Pattern additive, Pattern result) {
		result.nodes += additive.nodes
		result.edges += additive.edges
	}
	
	def void removeSubGraph(TriplePattern pattern, Node pCorr, Set<String> unchanged) {
		val corr = pattern.correspondence.findNode(pCorr)
		
		val contextNodes = pattern.correspondence.nodes.filter[it!==corr].toSet
		
		pattern.correspondence.edges.forEach[e|
			if(e.source.in(contextNodes)) contextNodes += e.target
		]
		
		val kerNodes = new HashSet<Node>
		kerNodes += corr
		pattern.correspondence.edges.forEach[e|
			if(e.source.in(kerNodes) && !e.target.in(contextNodes)) kerNodes += e.target
		]
		
		(pattern.source.edges + pattern.target.edges).forEach[e|
			if(e.source.in(contextNodes) && !e.target.in(kerNodes)) contextNodes+=e.target
			else if(e.target.in(contextNodes) && !e.source.in(kerNodes)) contextNodes += e.source
		]
		
		val deletedNodes = pattern.allNodes.filter[!it.in(contextNodes) && !it.name.in(unchanged)].toSet
		
		pattern.source.edges.removeIf[it.source.in(deletedNodes) || it.target.in(deletedNodes)]
		pattern.correspondence.edges.removeIf[it.source.in(deletedNodes) || it.target.in(deletedNodes)]
		pattern.target.edges.removeIf[it.source.in(deletedNodes) || it.target.in(deletedNodes)]
		
		pattern.source.nodes.removeIf[it.in(deletedNodes)]
		pattern.correspondence.nodes.removeIf[it.in(deletedNodes)]
		pattern.target.nodes.removeIf[it.in(deletedNodes)]
		
		
		val container = pattern.eContainer
		if(container instanceof Rule) {
			container.nacs.removeIf[nac|
				if(nac instanceof FunctionCall) {
					false // fix me
				} else if(nac instanceof TriplePattern) {
					nac.allEdges.exists[it.source.in(deletedNodes) || it.target.in(deletedNodes)]
				} else false
			]
		}
	}
	
	/**
	 * If an NAC is a triple pattern, it is breakable if there is a match in RHS and there is no match in LHS OR there are matches in LHS and RHS and a property condition of this NAC is changed in RHS
	 * If an NAC is a function call, it is breakable if there is a variable that is referred by this NAC, bound to a property that is changed
	 * 
	 * The algorithm may be replaced with constraint solving
	 */
	def findBreakableNACs(Rule rule) {
		val ruleDelta = rule.diffRule
		rule.nacs.filter[it.idBreakableNAC(rule, ruleDelta)].toList
	}
	
	def idBreakableNAC(Condition nac, Rule rule, RuleDelta delta) {
		if(nac.isInvariant) return false
		if (nac instanceof FunctionCall) {
			// it is possible that a rule changes a property value without breaking the NAC, though it's strange
			val referredVariables = nac.eAllContents.filter[it instanceof VariableRef].
				map[(it as VariableRef).variable].toSet
			return delta.leftDelta.source.properties.exists [ p |
				p.value instanceof VariableRef && (p.value as VariableRef).variable.in(referredVariables)
			] || delta.rightDelta.source.properties.exists [ p |
				p.value instanceof VariableRef && (p.value as VariableRef).variable.in(referredVariables)
			]
		} else {
			val pat = nac as TriplePattern
			return pat.canMatch(rule.rhs, delta.rightDelta)
		}
	}
	
	def canMatch(TriplePattern pat, TriplePattern graph, TripleGraphFragment shouldBeUsed) {
		// use the simplest algorithm
		
		val globalCandidates = pat.allNodes.toMap([it])[
			val scope = if(it.isDefinedIn(pat.source)) graph.source.nodes
				else if(it.isDefinedIn(pat.correspondence)) graph.correspondence.nodes
				else if(it.isDefinedIn(pat.target)) graph.target.nodes
				else <Node>emptyList
			scope.filter[sn | it.type.canMatch(sn.type)].toList
		]
		val allGraphEdges = graph.allEdges
		val allChangedEdges = (shouldBeUsed.source.edges + shouldBeUsed.correspondence.edges + shouldBeUsed.target.edges).toSet
		
		if(globalCandidates.values.exists[empty]) return false
		else {
			// build match order
			// build edge check
			val orderedPatternNodes = pat.allNodes // without any ordering
			val allPatEdges = pat.allEdges
			val edgeChecks = new HashMap<Node, Function<Map<Node,Node>, Pair<Boolean,Boolean>>> => [
				orderedPatternNodes.forEach[pn, i|
					val preNodes = orderedPatternNodes.subList(0,i+1)
					val checkableEdges = allPatEdges.filter[e|(e.source===pn && preNodes.contains(e.target)) || (e.target===pn && preNodes.contains(e.source))].toList
					put(pn) [match|
						val res = checkableEdges.map[pe|
							val sn = match.get(pe.source)
							val tn = match.get(pe.target)
							val type = pe.type
							
							val matchedEdges = allGraphEdges.filter[it.type===type && it.source===sn && it.target===tn].toList
							val matched = (!matchedEdges.empty)
							val matchDueToEdgeChange = matchedEdges.forall[e|e.in(allChangedEdges)]
							
							return matched -> matchDueToEdgeChange
						]
						return res.forall[it.key] -> res.exists[it.value]
					]
				]
			]
			
			// enumerate
			canMatch(new HashMap, false, orderedPatternNodes, 0, globalCandidates, edgeChecks, shouldBeUsed)
		}
	}
	
	private def boolean canMatch(Map<Node,Node> match, boolean newEdgeUsed, List<Node> patNodes, int position, Map<Node, List<Node>> globalCandidates, Map<Node, Function<Map<Node,Node>, Pair<Boolean,Boolean>>> edgeChecks, TripleGraphFragment shouldBeUsed) {
		if(position===patNodes.size) {
			val newNodes = match.values.exists [
				shouldBeUsed.source.nodes.contains(it) || shouldBeUsed.correspondence.nodes.contains(it) ||
					shouldBeUsed.target.nodes.contains(it)
			]

			val propRes = match.entrySet.map [ entry |
				// property checks
				val pn = entry.key
				val mn = entry.value
				val matching = pn.properties.map [ pp |
					if (pp.value instanceof VariableRef)
						true -> false
					else {
						val pc = pp.value as Constant
						val mp = mn.properties.findFirst [ mp |
							if(mp.value instanceof VariableRef) true else !pc.isSameAs(mp.value)
						]
						if (mp === null)
							false -> false
						else {
							val isNew = shouldBeUsed.source.properties.contains(mp) ||
								shouldBeUsed.correspondence.properties.contains(mp) ||
								shouldBeUsed.target.properties.contains(mp)

							true -> isNew
						}
					}
				].toList
				// returns whether all the properties of this node are matched and whether a property is matched due to property change
				return matching.forall[it.key] -> matching.exists[it.value]
			]
			
			// returns whether all the properties of all nodes are matched and whether a property is matched due to property change
			val propMatch = propRes.forall[it.key] -> propRes.exists[it.value]

			return propMatch.key && (newNodes || propMatch.value || newEdgeUsed)
		} else {
			val curNode = patNodes.get(position)
			val scope = globalCandidates.get(curNode)
			for(cand : scope) {
				if(!match.values.contains(cand)) {
					match.put(curNode, cand)
					val checks = edgeChecks.get(curNode)
					val edgeResult = checks.apply(match)
					if(edgeResult.key) {
						if(canMatch(match, newEdgeUsed || edgeResult.value, patNodes, position+1, globalCandidates, edgeChecks, shouldBeUsed)) {
							return true
						}						
					}
				}
			}
			return false
		}
	}
	
	def canMatch(EClass pat, EClass target) {
		pat.isSuperTypeOf(target)  // in fact, we should check there they have common subtypes
	}
	
	static def in(Object object, Collection<?> col) {
		col.contains(object)
	}
	
	
	def deriveRule(Rule rule, DerivedRuleKind kind) {
		val copy = rule.copy
		val derivedRule = createDerivedRule => [
			variables += copy.variables;
			lhs = copy.lhs;
			rhs = copy.rhs;
			nacs += copy.nacs;
			name = copy.name;
			ruleKind = kind;
			derivedFrom = rule;
		]
		return derivedRule
	}
}

class GraphFragment {
	public val nodes = new ArrayList<Node>
	public val edges = new ArrayList<Edge>
	public val properties = new ArrayList<Property>
	
	def isEmpty() {
		nodes.isEmpty && edges.isEmpty && properties.isEmpty
	}
	
	def isValueChangedOnly() {
		nodes.isEmpty && edges.isEmpty && !properties.isEmpty
	}
}

class TripleGraphFragment {
	public val source = new GraphFragment
	public val correspondence = new GraphFragment
	public val target = new GraphFragment
	
	def isEmpty() {
		source.isEmpty && correspondence.isEmpty && target.isEmpty
	}
	
	def isSourceValueChangedOnly() {
		source.isValueChangedOnly && correspondence.isEmpty && target.isEmpty
	}
	
	def getAllNodes() {
		source.nodes + correspondence.nodes + target.nodes
	}
	
	def getAllEdges() {
		source.edges + correspondence.edges + target.edges
	}
	
	def getAllProperties() {
		source.properties + correspondence.properties + target.properties
	}
	
	def contains(Node n) {
		BiGGRuleUtil.in(n, source.nodes) || BiGGRuleUtil.in(n, correspondence.nodes) || BiGGRuleUtil.in(n, target.nodes)
	}
	
	def contains(Edge e) {
		BiGGRuleUtil.in(e, source.edges) || BiGGRuleUtil.in(e, correspondence.edges) || BiGGRuleUtil.in(e, target.edges)
	}
}

class RuleDelta {
	public val leftDelta = new TripleGraphFragment
	public val rightDelta = new TripleGraphFragment
	public val unchangedNodes = new ArrayList<Node>
	public val unchangedEdges = new ArrayList<Edge>
	
	def isSourceChanged() {
		!(leftDelta.source.empty && rightDelta.source.empty)
	}
	
	def isTargetChanged() {
		!(leftDelta.target.empty && rightDelta.target.empty)
	}
	
	def isCorrespondenceChanged() {
		!(leftDelta.correspondence.empty && rightDelta.correspondence.empty)
	}
}