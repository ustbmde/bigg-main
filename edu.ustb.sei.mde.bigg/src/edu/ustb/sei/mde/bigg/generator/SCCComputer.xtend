package edu.ustb.sei.mde.bigg.generator

import java.util.List
import java.util.Set
import java.util.function.Function
import java.util.HashMap
import java.util.ArrayList
import java.util.Map

class SCCComputer<NODE> {
	val List<NODE> nodeLists
	val Function<NODE, Set<NODE>> neighborNodes
	
	new(List<NODE> nodeLists,Function<NODE, Set<NODE>> neighborNodes) {
		this.nodeLists = nodeLists
		this.neighborNodes = neighborNodes
		
		stack1.ensureCapacity(nodeLists.size)
		stack2.ensureCapacity(nodeLists.size)
	}
	
	val visited = new HashMap<NODE, Integer>
	val scc = new HashMap<NODE, Integer>
	val stack1 = new ArrayList<NODE>
	val stack2 = new ArrayList<NODE>
	
	var int sig = 0
	var int scc_num = 0
	
	private def void visit(NODE cur) {
		sig = sig + 1 // at least 1
		
		visited.put(cur, sig)
		stack1.add(cur)
		stack2.add(cur)
		
		val neighbors = neighborNodes.apply(cur)
		neighbors.forEach[neighbor|
			if(visited.get(neighbor)===null) {
				visit(neighbor)
			} else {
				val sccID = scc.get(neighbor)
				if(sccID === null) {
					val neighborTime = visited.getOrDefault(neighbor, 0)
					while(visited.getOrDefault(stack2.top,0) > neighborTime) {
						stack2.removeTop // remove top
					}
				}
			}
		]
		
		if(stack2.top === cur){
			stack2.removeTop
			scc_num ++
			
			var NODE top = null
			
			do {
				top = stack1.removeTop
				scc.put(top, scc_num)
			} while(top!==cur)
		}
	}
	
	def computeSCC() {
		nodeLists.forEach[node|
			if(visited.getOrDefault(node,0)===0) {
				node.visit
			}
		]
		
		val Map<Integer, Set<NODE>> sccIdMap = new HashMap<Integer, Set<NODE>>
		
		scc.forEach[info, sccID|
			val set = sccIdMap.computeIfAbsent(sccID, [newHashSet])
			set.add(info)
		]
		
		val Map<NODE, Set<NODE>> sccMap = new HashMap<NODE, Set<NODE>>
		sccIdMap.values.forEach[set|
			set.forEach[n|
				sccMap.put(n, set)
			]
		]
		
		sccMap
	}
	
	private def NODE top(List<NODE> stack) {
		stack.get(stack.size - 1)
	}
	
	private def removeTop(List<NODE> stack) {
		stack.remove(stack.size - 1)
	}
}