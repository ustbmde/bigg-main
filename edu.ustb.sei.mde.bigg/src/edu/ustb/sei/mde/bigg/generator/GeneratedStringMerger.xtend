package edu.ustb.sei.mde.bigg.generator

import java.util.List
import java.util.ArrayList
import java.util.Map
import java.util.HashSet
import java.util.HashMap

class GeneratedStringMerger {
	
	static val String MARKER_PREFIX = "//@"
	static val String MARKER_SUFFIX = " "
	
	public static val String MARKER_BEGIN = MARKER_PREFIX+"begin"+MARKER_SUFFIX
	public static val String MARKER_END = MARKER_PREFIX+"end"+MARKER_SUFFIX
	
	static def begin(CharSequence id) {
		'''«MARKER_BEGIN»«id»'''
	}
	
	static def end(CharSequence id) {
		'''«MARKER_END»«id»'''
	}
	
	static def String mergeStrings(String oldString, String newString) {
		val oldRegions = extractGeneratedRegions(oldString)
		val newRegions = extractGeneratedRegions(newString)
		
		val builder = new StringBuilder
		
		mergeStringRegions(oldRegions,newRegions,newString,0,newString.length, builder)
		
		if(builder.length===0) ''
		else builder.toString
	}
	
	static def void mergeStringRegions(List<GeneratedRegion> oldRegions, List<GeneratedRegion> newRegions, String newString, int superBegin, int superEnd, StringBuilder builder) {
		val alignment = alignRegions(oldRegions, newRegions)
		
		var cur = superBegin
		
		for(nr : newRegions) {
			if(nr.beginLOC!==cur) {
				builder.append(newString.subSequence(cur, nr.beginLOC))
			}
			val match = alignment.get(nr)
			if(match!==null) {
				if(nr.subRegions.empty) {
					builder.append(match.text)
				} else {
					mergeStringRegions(match.subRegions, nr.subRegions, newString, nr.beginLOC, nr.endLOC, builder)
				}
			} else {
				builder.append(nr.text)
			}
			
			cur = nr.endLOC
		}
		
		if(cur!==superEnd) {
			builder.append(newString.subSequence(cur, superEnd))
		}
	}
	
	static def Map<GeneratedRegion,GeneratedRegion> alignRegions(List<GeneratedRegion> oldRegions, List<GeneratedRegion> newRegions) {
		val matched = new HashSet<GeneratedRegion>()
		val map = new HashMap<GeneratedRegion,GeneratedRegion>
		
		newRegions.forEach[nr|
			val match = oldRegions.findFirst[or|
				or.id==nr.id && !matched.contains(or)
			]
			matched += match
			
			map.put(nr, match)
		]
		
		map
	}
	
	static def List<GeneratedRegion> extractGeneratedRegions(String string) {
		var int cur = 0;
		regionStack.clear
		rootRegions.clear
		
		while(cur>=0 && cur < string.length) {
			cur = moveToNextMarker(string, cur)
			
			if(cur>=0) {
				if(string.startsWith(MARKER_BEGIN, cur)) cur=beginRegion(string, cur)
				else if(string.startsWith(MARKER_END, cur)) cur=endRegion(string, cur)
			}
		}
		
		val result = new ArrayList(rootRegions)
		return result
	}
	
	static val regionStack = new ArrayList<GeneratedRegion>(32)
	static val rootRegions = new ArrayList<GeneratedRegion>(32)
	
	static def int moveToNextMarker(String string, int fromLOC) {
		string.indexOf(MARKER_PREFIX, fromLOC)
	}
	
	static def int beginRegion(String string, int fromLOC) {
		var eol = string.indexOf('\n', fromLOC)
		if(eol===-1) eol=string.length;
		val end = (eol===string.length? eol : eol + 1);
		
		val mark = string.substring(fromLOC, eol)
		
		if(mark.startsWith(MARKER_BEGIN)) {
			val id = mark.substring(MARKER_BEGIN.length).trim
			val region = new GeneratedRegion(string)
			region.beginLOC = fromLOC
			region.id = id
			regionStack.add(region)
		} else {}
		
		return end
	}
	
	static def int endRegion(String string, int fromLOC) {
		var eol = string.indexOf('\n', fromLOC)
		if(eol===-1) eol=string.length;
		val end = (eol===string.length? eol : eol + 1);
		
		val mark = string.substring(fromLOC, eol)
		
		if(mark.startsWith(MARKER_END)) {
			val id = mark.substring(MARKER_END.length).trim
			
			while(!regionStack.empty) {
				val last = regionStack.remove(regionStack.size - 1)
				if(last.id==id) {
					last.endLOC = end
					if(!regionStack.empty) {
						regionStack.get(regionStack.size - 1).subRegions += last
					} else {
						rootRegions += last
					}
					return end
				}
			}
		} else return end
	}
}

abstract class TextRegion {
	public var int beginLOC;
	public var int endLOC;
	public var String source;
	
	new(String src) {
		source = src
	}
	
	def text() {
		source.subSequence(beginLOC, endLOC)
	}
}

class ConstantRegion extends TextRegion {
	new(String src) {
		super(src)
	}
	
}

class GeneratedRegion extends TextRegion {
	public var CharSequence id;
	public val List<GeneratedRegion> subRegions = new ArrayList
	
	new(String src) {
		super(src)
	}
	
}