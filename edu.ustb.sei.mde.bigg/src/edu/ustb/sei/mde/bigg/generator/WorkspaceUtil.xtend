package edu.ustb.sei.mde.bigg.generator

import java.io.ByteArrayInputStream
import java.util.function.Supplier
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IWorkspaceRoot
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.Path
import org.eclipse.emf.common.util.URI

class WorkspaceUtil {
	val IWorkspaceRoot workspaceRoot;
	new() {
		workspaceRoot = ResourcesPlugin.workspace.root
	}
	
	def IFile getFile(IPath path) {
		workspaceRoot.getFile(path)
	}
	
	def IPath getPath(URI uri) {
		if(uri.isPlatform)
			new Path(uri.toPlatformString(true))
		else new Path(uri.toFileString)
	}
	
	def write(IFile file, Supplier<CharSequence> content) {
		val proj = file.project
		if(proj===null || proj.exists===false) false
		else {
			val seq = content.get.toString
			val source = new ByteArrayInputStream(seq.bytes);
			if(file.exists===false) {
				file.create(source, true,null)
			} else {
				file.setContents(source, true, false, null)
			}
			return true
		}
	}
}