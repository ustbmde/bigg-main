package edu.ustb.sei.mde.bigg.generator

import edu.ustb.sei.mde.bigg.Argument
import edu.ustb.sei.mde.bigg.BiggPackage
import edu.ustb.sei.mde.bigg.BiggProgram
import edu.ustb.sei.mde.bigg.Constant
import edu.ustb.sei.mde.bigg.DerivedRule
import edu.ustb.sei.mde.bigg.DerivedRuleKind
import edu.ustb.sei.mde.bigg.EnumConstant
import edu.ustb.sei.mde.bigg.FunctionCall
import edu.ustb.sei.mde.bigg.Node
import edu.ustb.sei.mde.bigg.PersistentMark
import edu.ustb.sei.mde.bigg.Rule
import edu.ustb.sei.mde.bigg.TriplePattern
import edu.ustb.sei.mde.bigg.Tuple
import edu.ustb.sei.mde.bigg.VariableRef
import java.util.Collections
import java.util.List
import java.util.Map
import java.util.function.Predicate
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EcorePackage
import java.util.Set
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EAttribute
import java.util.HashMap
import java.util.HashSet

class RuleConstraintGenerator {
	extension BiGGRuleUtil
	extension ModelConstraintGenerator = new ModelConstraintGenerator
	extension RuleAffection = new RuleAffection
	var Map<String, List<FunctionInfomation>> functionInformation
	
	new() {
		_biGGRuleUtil = new BiGGRuleUtil
	}
	
	new(BiGGRuleUtil ruleUtil) {
		_biGGRuleUtil = ruleUtil
	}
	
	
	
	def generateModelConstraints(String modelName, BiggProgram program) {
		val constraints = program.extraConstraints.filter[type=='model']
		
		'''
		«modelName.generateTripleGraph(program.source, program.correspondence, program.target)»
		
		«FOR c : constraints.indexed»
		// constraint «c.key» for «c.value.type»
		«c.value.code»
		
		«ENDFOR»
		'''
	}
	
	/*
	 * We need integrity constraints from checkers because the solver may construct some invalid fragments.
	 * For example, it may create {cm->c, tm, t, cm<-m2m->tm, c<-c2t->t}. For class destroyer rule, the fragment
	 * may trigger this rule because the missing of tm->t. But it is not the case we want to verify.
	 * 
	 * However, integrity rule may also cause some problems. Because it requires extra nodes, the estimated size 
	 * 	bound may be too small to find a solution. So we may get false negatives. E.g., UpdateSuperAttribute, where
	 *   not all context nodes occur in the rule 
	 * 
	 * Reason: When a relation occurs in a NAC, we need to assure the integrity of the relation.  
	 * 
	 * Possible solution:
	 *  refine bound estimation
	 */
	def generateCheckerRuleIntegrityConstraints(String modelName, List<DerivedRule> rules) {
		val groups = rules.groupBy[name]
		
		// check consistency
		val corNodeMap = new HashMap<String, EClass>
		
		groups.forEach[name, group|
			val pair = group.map[
				val delta = it.diffRule
				val corNode = delta.rightDelta.correspondence.properties.get(0).eContainer as Node
				return it.needsIntegrity -> corNode.type
			].toSet
			
			if(pair.size!==1) {
				System.err.println(name + ' checkers are not consistent!')
			} else {
				val first = pair.iterator.next
				if(first.key) corNodeMap.put(name, first.value)
			}
		]
		
		'''
		open «modelName»Model
		open «modelName»RuleUtil
		open «modelName»Functions
		
		// extra integrity constraints
		«FOR groupPair : groups.entrySet»
		«IF corNodeMap.containsKey(groupPair.key)»
		«val type = corNodeMap.get(groupPair.key)»
		«val indexedRules = groupPair.value.indexed»
		«FOR ruleIndex : indexedRules»
		«val cnt = ruleIndex.key»
		«val rule = ruleIndex.value»
		«val direction = rule.ruleKind»
		«val delta = rule.diffRule»
		«val focusedCorr = delta.rightDelta.correspondence.properties.get(0).eContainer as Node»
		«val posMap = focusedCorr.type.EAllReferences.indexed.toMap([it.value])[it.key]»
		«val focusedCorrEdges = rule.rhs.correspondence.edges.filter[it.source===focusedCorr].sortBy[posMap.get(it)]»
		«val focusedNodeNames = focusedCorrEdges.map[target].map[name].toSet => [it += focusedCorr.name]»
		pred INT_«rule.ruleName(direction,cnt)»[«focusedCorr.name» : «focusedCorr.type.generateScope», «FOR n:focusedCorrEdges.map[target] SEPARATOR ', '»«n.name» : «n.type.generateScope»«ENDFOR», graph : «modelName.graphName»] {
			«FOR n : rule.lhs.allNodes.filter[!focusedNodeNames.contains(it.name)] BEFORE 'some ' SEPARATOR ', ' AFTER ' |'»«n.name» : «n.type.generateScope»«ENDFOR»
			«FOR n : rule.lhs.allNodes.filter[!focusedNodeNames.contains(it.name)] SEPARATOR ' && ' AFTER ' &&'»«n.name» in graph.nodes«ENDFOR»
			«FOR e : rule.lhs.allEdges.filter[!(focusedNodeNames.contains(it.source.name) && focusedNodeNames.contains(it.target.name))] SEPARATOR ' && '»«e.source.name»->«e.target.name» in graph.@«e.type.fieldName»«ENDFOR»
«««		ideally, we should also check nacs
«««		all nodes | LHS+cor && RHS+cor => COR and vice-versa
		}
		
		«ENDFOR»
		
		pred «groupPair.key»Integrity[graph : «modelName.graphName», n0 : «type.generateScope»] {
«««			all graph : «modelName.graphName», n0 : «type.generateScope» |
			«FOR i : 1..type.EReferences.size BEFORE 'let ' SEPARATOR ', '»n«i» = n0.(graph.@«type.EReferences.get(i-1).fieldName»)«ENDFOR» |
			  (n0 in graph.nodes && «FOR i : 1..type.EReferences.size SEPARATOR ' && '»some n«i» && n«i» in «type.EReferences.get(i-1).EReferenceType.generateScope»«ENDFOR» &&
			    n0->kept in graph.@«BiggPackage.Literals.TRANSLATION_OBJECT__STATUS.fieldName») => (
			      «FOR ruleIndex : indexedRules SEPARATOR ' || '»INT_«ruleIndex.value.ruleName(ruleIndex.value.ruleKind,ruleIndex.key)»[«FOR i : 0..type.EReferences.size SEPARATOR ', '»n«i»«ENDFOR», graph]«ENDFOR»
			    )
		}
		
		«ENDIF»
		«ENDFOR»
		'''
	}
	
//	def generateCheckerRuleConstraints(String modelName, List<DerivedRule> rules) {
//		val groups = rules.groupBy[name]
//		
//		// check consistency
//		val corNodeMap = new HashMap<String, EClass>
//		
//		groups.forEach[name, group|
//			val pair = group.map[
//				val delta = it.diffRule
//				val corNode = delta.rightDelta.correspondence.properties.get(0).eContainer as Node
//				return it.needsIntegrity -> corNode.type
//			].toSet
//			
//			if(pair.size!==1) {
//				System.err.println(name + ' checkers are not consistent!')
//			} else {
//				val first = pair.iterator.next
//				if(first.key) corNodeMap.put(name, first.value)
//			}
//		]
//		
//		'''
//		«modelName.generateRuleConstraints(rules)»
//		
//		// extra integrity constraints
//		«FOR groupPair : groups.entrySet»
//		«IF corNodeMap.containsKey(groupPair.key)»
//		«val type = corNodeMap.get(groupPair.key)»
//		fact «groupPair.key»Integrity {
//			all graph : «modelName.graphName», n0 : «type.generateScope» |
//			«FOR i : 1..type.EReferences.size BEFORE 'let ' SEPARATOR ', '»n«i» = n0.(graph.@«type.EReferences.get(i-1).fieldName»)«ENDFOR» |
//			  (n0 in graph.nodes && «FOR i : 1..type.EReferences.size SEPARATOR ' && '»some n«i» && n«i» in «type.EReferences.get(i-1).EReferenceType.generateScope»«ENDFOR» &&
//			    n0->kept in graph.@«BiggPackage.Literals.TRANSLATION_OBJECT__STATUS.fieldName») => (
//			      «FOR ruleIndex : groupPair.value.indexed SEPARATOR ' || '»INT_«ruleIndex.value.ruleName(ruleIndex.value.ruleKind,ruleIndex.key)»[«FOR i : 0..type.EReferences.size SEPARATOR ', '»n«i»«ENDFOR», graph]«ENDFOR»
//			    )
//		}
//		
//		«ENDIF»
//		«ENDFOR»
//		'''
//	}
	
	def generateRuleConstraints(String modelName, List<DerivedRule> rules) {
		'''
		open «modelName»Model
		open «modelName»RuleUtil
		open «modelName»Functions
		
		«FOR entry : rules.groupBy[name].entrySet»
		«FOR pair : entry.value.indexed»
		«generateRuleConstraint(modelName, pair.value, pair.key)»
		«ENDFOR»
		«ENDFOR»
		'''
	}
	
	
	
	def generateFunctions(String modelName, Iterable<DerivedRule> rules) {
		val allFunctions = <FunctionCall>newArrayList()
		
		rules.forEach[rule|
			rule.eAllContents.forEachRemaining[e|
				if(e instanceof FunctionCall) {
					if(!e.callee.isBuiltInFunction) {
						allFunctions.add(e)
					}
				}
			]
		]
		
		functionInformation = allFunctions.map[func |
			new FunctionInfomation(func.callee, func.arguments.size)
		].toSet.groupBy[name]
		
		'''
		open «modelName»Model
		
		«FOR fs : functionInformation.entrySet»
		«FOR f : fs.value»
		«GeneratedStringMerger.begin('''«f.name»«f.parameterCount»''')»
		fun «f.name»«f.parameterCount»[«FOR i : 1..f.parameterCount SEPARATOR ', '»parm«i»:set Int->univ«ENDFOR»] : set Int->univ {
			«IF 'replace'==f.name»
			parm«f.parameterCount»
			«ELSE»
			// users must provide
			none->none
			«ENDIF»
		}
		«GeneratedStringMerger.end('''«f.name»«f.parameterCount»''')»
		
		«ENDFOR»
		«ENDFOR»
		'''
	}
	
	def generateCommonRuleUtil(String modelName) {
		'''
		open «modelName»Model
		
		abstract sig «modelName.toFirstUpper»Rule {
			_deletedNodes : set «modelName.rootNodeName»,
			_createdNodes : set «modelName.rootNodeName»
		}
				
		fact NoTwoRulesCreateTheSameNodes {
			all r1,r2 : «modelName.toFirstUpper»Rule | r1!=r2 => no r1._createdNodes & r2._createdNodes
		}
		
		abstract sig OneStepExecution {
			m0 : one «modelName.graphName»,
			m1 : lone «modelName.graphName»,
			r1 : one «modelName.toFirstUpper»Rule
		}
		
		abstract sig TwoStepExecution {
			m0 : one «modelName.graphName»,
			m1 : lone «modelName.graphName»,
			m2 : lone «modelName.graphName»,
			r1 : one «modelName.toFirstUpper»Rule,
			r2 : one «modelName.toFirstUpper»Rule
		} {
			r1 != r2 && some m2 => some m1
		}
		'''
	}
	
	def generateVerificationsOfConflict(String modelName, List<DerivedRule> rules, String ruleFile, Predicate<DerivedRuleKind> ruleFilter) {
		val indexedRules = rules.groupBy[name].values.map[indexed].flatten.filter[ruleFilter.test(it.value.ruleKind)].toList
		val size = indexedRules.size
		
		'''
		open «modelName»Model
		open «modelName»RuleUtil
		open «modelName»Functions
		open «modelName»Integrity
		open «modelName»«ruleFile»
		
		«FOR a : 0..<size»
		«val ruleAWithIndex = indexedRules.get(a)»
		«FOR b : a..<size»
		«val ruleBWithIndex = indexedRules.get(b)»
		«generateVerificationOfMutalConflict(modelName, ruleAWithIndex, ruleBWithIndex)»
		«ENDFOR»
		«ENDFOR»
		'''
	}
	
	def generateVerificationsOfConflict(String modelName, List<DerivedRule> aRules, String aRuleFile, List<DerivedRule> bRules, String bRuleFile, Predicate<DerivedRuleKind> ruleFilter) {
		val indexedARules = aRules.groupBy[name].values.map[indexed].flatten.filter[ruleFilter.test(it.value.ruleKind)].toList
		val indexedBRules = bRules.groupBy[name].values.map[indexed].flatten.filter[ruleFilter.test(it.value.ruleKind)].toList
		
		'''
		open «modelName»Model
		open «modelName»RuleUtil
		open «modelName»Functions
		open «modelName»Integrity
		open «modelName»«aRuleFile»
		open «modelName»«bRuleFile»
		
		«FOR ruleAWithIndex : indexedARules»
		«FOR ruleBWithIndex : indexedBRules»
		«generateVerificationOfMutalConflict(modelName, ruleAWithIndex, ruleBWithIndex)»
		
		«ENDFOR»
		«ENDFOR»
		'''
	}
	
	protected def generateExecutionPredicateForOneStep(Rule rule1, String direction1, int cnt1) {
		'''
		r1 in «rule1.ruleName(direction1,cnt1)»
		
		PRE_«rule1.ruleName(direction1,cnt1)»[r1,m0] => (one m1 && POST_«rule1.ruleName(direction1,cnt1)»[r1, m0, m1]) else no m1
		
		'''
	}
	
	protected def generateExecutionPredicate(Rule rule1, String direction1, int cnt1, Rule rule2, String direction2, int cnt2) {
		'''
		r1 in «rule1.ruleName(direction1,cnt1)» && r2 in «rule2.ruleName(direction2,cnt2)»
		
		PRE_«rule1.ruleName(direction1,cnt1)»[r1,m0] => (one m1 && POST_«rule1.ruleName(direction1,cnt1)»[r1, m0, m1]) else no m1
		
		(one m1 && PRE_«rule2.ruleName(direction2,cnt2)»[r2,m1]) => (one m2 && POST_«rule2.ruleName(direction2,cnt2)»[r2, m1, m2]) else no m2
		'''
	}
	
	protected def estimateBound(Rule rule) {
		estimateBound(rule, true)
	}
	
	protected def estimateBound(Rule rule, boolean needRHS) {
		val delta = rule.diffRule
		
		val boundMap = new BoundMap
		
		rule.lhs.estimateBound(boundMap);
		
		val nacBound = if(needRHS) {
			delta.rightDelta.allNodes.forEach[boundMap.increaseBound(it.type)];
			rule.nacs.filter[it instanceof TriplePattern].map[(it as TriplePattern)].map[
				val nacBound = new BoundMap
				it.estimateBound(nacBound)
				return nacBound
			].reduce[b1, b2| b1.union(b2)]			
		}
		
		if(nacBound===null) boundMap
		else boundMap.add(nacBound)
	}
	
	protected def void increaseBound(BoundMap boundMap, EClass clazz) {
		// if clazz does not have concrete super/subclass, add this clazz's scope
		// otherwise, add to all
		
		if(clazz.hasSeparateScope) {
			boundMap.add(clazz)
		} else {
			boundMap.add(EcorePackage.Literals.EOBJECT)
		}
	}
	
	protected def void estimateBound(TriplePattern pattern, BoundMap boundMap) {
		if(pattern === null) {}
		else {
			pattern.source?.nodes?.forEach[n|
				boundMap.increaseBound(n.type)
			]
			pattern.target?.nodes?.forEach[n|
				boundMap.increaseBound(n.type)
			]
			pattern.correspondence?.nodes?.forEach[n|
				boundMap.increaseBound(n.type)
				val types = pattern.correspondence?.edges?.filter[it.source===n]?.map[it.type]?.toSet
				if(types!==null) {
					n.type.EReferences.filter[!types.contains(it)].forEach[ref|
						val target = ref.EReferenceType
						boundMap.increaseBound(target)
					]
				}
			]
		}
	}
	
	protected def generateVerificationOfMutalConflict(String modelName, Pair<Integer,DerivedRule> ruleInfoA, Pair<Integer,DerivedRule> ruleInfoB) {
		'''
		«generateVerificationOfConflict(modelName, ruleInfoA, ruleInfoB)»
		«IF ruleInfoA!==ruleInfoB»
		«generateVerificationOfConflict(modelName, ruleInfoB, ruleInfoA)»
		«ENDIF»
		'''
	}
	
	private def isCheckerOrCreator(DerivedRuleKind kind) {
		kind === DerivedRuleKind.CHECKER || kind === DerivedRuleKind.FORWARD_RELATION_CREATOR || kind === DerivedRuleKind.BACKWARD_RELATION_CREATOR || kind === DerivedRuleKind.BI_RELATION_CREATOR 
	}
	
	def generateVerificationsOfKeepDeleteConflict(String modelName, List<DerivedRule> checkerRules, String checkerRuleFile, List<DerivedRule> breakerRules, String breakerRuleFile, Predicate<DerivedRuleKind> ruleFilter) {
		val indexedARules = checkerRules.groupBy[name].values.map[indexed].flatten.filter[ruleFilter.test(it.value.ruleKind)].toList
		val indexedBRules = breakerRules.groupBy[name].values.map[indexed].flatten.filter[ruleFilter.test(it.value.ruleKind)].toList
		
		'''
		open «modelName»Model
		open «modelName»RuleUtil
		open «modelName»Functions
		open «modelName»Integrity
		open «modelName»«checkerRuleFile»
		open «modelName»«breakerRuleFile»
		
		«FOR ruleAWithIndex : indexedARules»
		«FOR ruleBWithIndex : indexedBRules»
		«generateVerificationOfKeepDeleteConflict(modelName, ruleAWithIndex, ruleBWithIndex)»
		«ENDFOR»
		«ENDFOR»
		'''
	}
	
	def generateVerificationsOfDependency(String modelName, List<DerivedRule> creatorRules, String creatorRuleFile, List<DerivedRule> breakerRules, String breakerRuleFile) {
		val indexedCreatorRules = creatorRules.groupBy[name].values.map[indexed].flatten.filter[it.value.ruleKind===DerivedRuleKind.BACKWARD_RELATION_CREATOR || it.value.ruleKind===DerivedRuleKind.FORWARD_RELATION_CREATOR || it.value.ruleKind===DerivedRuleKind.BI_RELATION_CREATOR].toList
		val indexedBreakerRules = breakerRules.groupBy[name].values.map[indexed].flatten.filter[it.value.ruleKind===DerivedRuleKind.BACKWARD_RELATION_BREAKER || it.value.ruleKind===DerivedRuleKind.FORWARD_RELATION_BREAKER || it.value.ruleKind===DerivedRuleKind.BI_RELATION_BREAKER || it.value.ruleKind===DerivedRuleKind.VIEW_DESTROYER || it.value.ruleKind===DerivedRuleKind.SOURCE_DESTROYER].toList
		
		'''
		open «modelName»Model
		open «modelName»RuleUtil
		open «modelName»Functions
		open «modelName»Integrity
		open «modelName»«creatorRuleFile»
		«IF creatorRuleFile!=breakerRuleFile»
		open «modelName»«breakerRuleFile»
		«ENDIF»
		
		«FOR ruleAWithIndex : indexedCreatorRules»
		«FOR ruleBWithIndex : indexedBreakerRules»
		«generateVerificationOfDependency(modelName, creatorRuleFile, ruleAWithIndex, breakerRuleFile, ruleBWithIndex)»
		«ENDFOR»
		«ENDFOR»
		'''
	}
	
	def generateVerificationsOfDependency2(String modelName, List<DerivedRule> creatorRules, String creatorRuleFile, List<DerivedRule> breakerRules, String breakerRuleFile) {
		val indexedCreatorRules = creatorRules.groupBy[name].values.map[indexed].map[it.filter[it.value.ruleKind===DerivedRuleKind.BACKWARD_RELATION_CREATOR || it.value.ruleKind===DerivedRuleKind.FORWARD_RELATION_CREATOR || it.value.ruleKind===DerivedRuleKind.BI_RELATION_CREATOR].toList]
		val indexedBreakerRules = breakerRules.groupBy[name].values.map[indexed].flatten.filter[it.value.ruleKind===DerivedRuleKind.BACKWARD_RELATION_BREAKER || it.value.ruleKind===DerivedRuleKind.FORWARD_RELATION_BREAKER || it.value.ruleKind===DerivedRuleKind.BI_RELATION_BREAKER || it.value.ruleKind===DerivedRuleKind.VIEW_DESTROYER || it.value.ruleKind===DerivedRuleKind.SOURCE_DESTROYER].toList
		
		'''
		open «modelName»Model
		open «modelName»RuleUtil
		open «modelName»Functions
		open «modelName»Integrity
		open «modelName»«creatorRuleFile»
		«IF creatorRuleFile!=breakerRuleFile»
		open «modelName»«breakerRuleFile»
		«ENDIF»
		
		«FOR ruleAWithIndex : indexedCreatorRules»
		«FOR ruleBWithIndex : indexedBreakerRules»
		«generateVerificationOfDependency(modelName, creatorRuleFile, ruleAWithIndex, breakerRuleFile, ruleBWithIndex)»
		«ENDFOR»
		«ENDFOR»
		'''
	}
	
	protected def hasKeepDeleteConflict(DerivedRule checker, DerivedRule breaker) {
		val delta = breaker.diffRule
		
		delta.leftDelta.allNodes.exists[it.type.willCancelDueToDeletion(checker)] ||
			delta.leftDelta.allEdges.exists[it.type.willCancelDueToDeletion(checker)] ||
			delta.leftDelta.allProperties.exists[(it.eContainer as Node).type.willCancelDueToDeletion(it.type, checker)]

//		true
	}
	
	protected def generateVerificationOfKeepDeleteConflict(String modelName, Pair<Integer,DerivedRule> checkerInfo, Pair<Integer,DerivedRule> breakerInfo) {
		// generate a verification to check whether two rules conflict with each other
		val directionA = checkerInfo.value.ruleKind
		val directionB = breakerInfo.value.ruleKind
		
		val checkerRuleName = checkerInfo.value.ruleName(directionA, checkerInfo.key)
		val breakerRuleName = breakerInfo.value.ruleName(directionB, breakerInfo.key)
		
		val boundA = checkerInfo.value.estimateBound
		val boundB = breakerInfo.value.estimateBound
		
		boundA.add(boundB)
		
		var defaultBound = boundA.remove(EcorePackage.Literals.EOBJECT) ?: 0
		
		defaultBound = defaultBound + boundA.allBounds.map[value].fold(0)[l,r|l+r]
		
		'''
		sig FindKeepDeleteConflictBetween«checkerRuleName»And«breakerRuleName» extends OneStepExecution {
			r2 : one «breakerRuleName»
		} {
			r1 in «checkerRuleName»
			
			REAPP_«checkerRuleName»[r1,m0] && PRE_«breakerRuleName»[r2, m0]
			
			one m1 && POST_«breakerRuleName»[r2, m0, m1] && (not REAPP_«checkerRuleName»[r1, m1])
			
			NoUnknownMarks[m0]
		}
		
		«IF hasKeepDeleteConflict(checkerInfo.value, breakerInfo.value)===false»// Should not have conflict«ENDIF»
		run findKeepDeleteConflictBetween«checkerRuleName»And«breakerRuleName» {}
		  for 0 but «defaultBound» «modelName.rootNodeName», exactly 1 FindKeepDeleteConflictBetween«checkerRuleName»And«breakerRuleName», «IF checkerInfo===breakerInfo»exactly 2 «checkerRuleName»«ELSE»exactly 1 «checkerRuleName», exactly 1 «breakerRuleName»«ENDIF», exactly 2 «modelName.graphName»«FOR entry : boundA.allBounds», exactly «entry.value» «entry.key.generateScope»«ENDFOR»
		
		'''
	}
	
	
	protected def hasDependency(DerivedRule creator, DerivedRule breaker) {
		val deltaA = breaker.diffRule
		
		// if -t.willCancelDueToDeletion(rule) <=> +t.willBeActivatedByCreation
		
		deltaA.rightDelta.allNodes.exists[it.type.willCancelDueToDeletion(creator)] ||
			deltaA.rightDelta.allEdges.exists[it.type.willCancelDueToDeletion(creator)] ||
			deltaA.rightDelta.allProperties.exists[(it.eContainer as Node).type.willCancelDueToDeletion(it.type, creator)] ||
		deltaA.leftDelta.allNodes.exists[it.type.willCancelDueToCreation(creator)] ||
			deltaA.leftDelta.allEdges.exists[it.type.willCancelDueToCreation(creator)] ||
			deltaA.leftDelta.allProperties.exists[(it.eContainer as Node).type.willCancelDueToCreation(it.type, creator)]
//		true
	}
	
	protected def generateVerificationOfDependency(String modelName, String creatorRuleFile, List<Pair<Integer,DerivedRule>> creatorsInfo, String breakerRuleFile, Pair<Integer,DerivedRule> breakerInfo) {
		
		// generate a verification to check whether two rules conflict with each other		
		val directionB = breakerInfo.value.ruleKind
		val breakerRuleName = breakerInfo.value.ruleName(directionB, breakerInfo.key)
		val boundB = breakerInfo.value.estimateBound
		
		val boundA = creatorsInfo.map[it.value.estimateBound].reduce[b1,b2|b1.union(b2)]
		
//		val directionA = creatorInfo.value.ruleKind
		val creatorRuleName = creatorsInfo.get(0).value.name+creatorRuleFile
		
		boundA.add(boundB)
		
		var defaultBound = boundA.remove(EcorePackage.Literals.EOBJECT) ?: 0
		
		defaultBound = defaultBound + boundA.allBounds.map[value].fold(0)[l,r|l+r]
		
		'''
		// the breaker rule activates a creator rule
		sig FindDependencyBetween«creatorRuleName»And«breakerRuleName»_NEW extends OneStepExecution {
		} {
			r1 in «modelName»«breakerRuleFile»/«breakerRuleName»
			
			not («FOR creator : creatorsInfo SEPARATOR ' || '»«modelName»«creatorRuleFile»/PRE_«creator.value.ruleName(creator.value.ruleKind, creator.key)»[m0]«ENDFOR»)
			
			«modelName»«breakerRuleFile»/PRE_«breakerRuleName»[r1, m0]
			
			one m1 && «modelName»«breakerRuleFile»/POST_«breakerRuleName»[r1, m0, m1]
			
			«FOR creator : creatorsInfo SEPARATOR ' || '»«modelName»«creatorRuleFile»/PRE_«creator.value.ruleName(creator.value.ruleKind, creator.key)»[m1]«ENDFOR»
			
			NoUnknownMarks[m0]
		}
		
		run findDependencyBetween«creatorRuleName»And«breakerRuleName»_NEW {}
		  for 0 but «defaultBound» «modelName.rootNodeName», exactly 1 FindDependencyBetween«creatorRuleName»And«breakerRuleName»_NEW, exactly 1 «modelName»«breakerRuleFile»/«breakerRuleName», exactly 2 «modelName.graphName»«FOR entry : boundA.allBounds», exactly «entry.value» «entry.key.generateScope»«ENDFOR»
		
		'''
	}
	
	protected def generateVerificationOfDependency(String modelName, String creatorRuleFile, Pair<Integer,DerivedRule> creatorInfo, String breakerRuleFile, Pair<Integer,DerivedRule> breakerInfo) {
		
		// generate a verification to check whether two rules conflict with each other
		val directionA = creatorInfo.value.ruleKind
		val directionB = breakerInfo.value.ruleKind
		
		val creatorRuleName = creatorInfo.value.ruleName(directionA, creatorInfo.key)
		val breakerRuleName = breakerInfo.value.ruleName(directionB, breakerInfo.key)
		
		val boundA = creatorInfo.value.estimateBound
		val boundB = breakerInfo.value.estimateBound
		
		boundA.add(boundB)
		
		var defaultBound = boundA.remove(EcorePackage.Literals.EOBJECT) ?: 0
		
		defaultBound = defaultBound + boundA.allBounds.map[value].fold(0)[l,r|l+r]
		
		'''
		// the breaker rule activates a creator rule
		sig FindDependencyBetween«creatorRuleName»And«breakerRuleName» extends OneStepExecution {
			r2 : one «modelName»«breakerRuleFile»/«breakerRuleName»
		} {
			r1 in «modelName»«creatorRuleFile»/«creatorRuleName»
			
			«modelName»«creatorRuleFile»/VALID_«creatorRuleName»[r1, m0]
			
			(not «modelName»«creatorRuleFile»/PRE_«creatorRuleName»[r1,m0]) && «modelName»«breakerRuleFile»/PRE_«breakerRuleName»[r2, m0]
			
			one m1 && «modelName»«breakerRuleFile»/POST_«breakerRuleName»[r2, m0, m1] && «modelName»«creatorRuleFile»/PRE_«creatorRuleName»[r1, m1]
			
			NoUnknownMarks[m0]
		}
		
		«IF hasDependency(creatorInfo.value, breakerInfo.value)===false»// Should not have dependency«ENDIF»
		run findDependencyBetween«creatorRuleName»And«breakerRuleName» {}
		  for 0 but «defaultBound» «modelName.rootNodeName», exactly 1 FindDependencyBetween«creatorRuleName»And«breakerRuleName», «IF creatorInfo===breakerInfo»exactly 2 «modelName»«creatorRuleFile»/«creatorRuleName»«ELSE»exactly 1 «modelName»«creatorRuleFile»/«creatorRuleName», exactly 1 «modelName»«breakerRuleFile»/«breakerRuleName»«ENDIF», exactly 2 «modelName.graphName»«FOR entry : boundA.allBounds», exactly «entry.value» «entry.key.generateScope»«ENDFOR»
		
		'''
	}
	
	protected def hasConflict(DerivedRule ruleA, DerivedRule ruleB) {
		// A cancel B
		val deltaA = ruleA.diffRule
		// if A deletes something required by B or A creates something forbad by B
		
		deltaA.leftDelta.allNodes.exists[it.type.willCancelDueToDeletion(ruleB)] ||
			deltaA.leftDelta.allEdges.exists[it.type.willCancelDueToDeletion(ruleB)] ||
			deltaA.leftDelta.allProperties.exists[(it.eContainer as Node).type.willCancelDueToDeletion(it.type,ruleB)] ||
		deltaA.rightDelta.allNodes.exists[it.type.willCancelDueToCreation(ruleB)] ||
			deltaA.rightDelta.allEdges.exists[it.type.willCancelDueToCreation(ruleB)] ||
			deltaA.rightDelta.allProperties.exists[(it.eContainer as Node).type.willCancelDueToCreation(it.type, ruleB)]
//		true
	}
	
	protected def generateVerificationOfConflict(String modelName, Pair<Integer,DerivedRule> ruleInfoA, Pair<Integer,DerivedRule> ruleInfoB) {
		// generate a verification to check whether two rules conflict with each other
		val directionA = ruleInfoA.value.ruleKind
		val directionB = ruleInfoB.value.ruleKind
		
		val creator = directionA.isCheckerOrCreator && directionB.isCheckerOrCreator 
		val breaker = !directionA.isCheckerOrCreator && !directionB.isCheckerOrCreator
		
		val ruleAName = ruleInfoA.value.ruleName(directionA, ruleInfoA.key)
		val ruleBName = ruleInfoB.value.ruleName(directionB, ruleInfoB.key)
		
		val boundA = ruleInfoA.value.estimateBound
		val boundB = ruleInfoB.value.estimateBound
		
		boundA.add(boundB)
		
		var defaultBound = boundA.remove(EcorePackage.Literals.EOBJECT) ?: 0
		
		defaultBound = defaultBound + boundA.allBounds.map[value].fold(0)[l,r|l+r]
		
		'''
		sig FindConflictBetween«ruleAName»And«ruleBName» extends OneStepExecution {
			r2 : one «ruleBName»
		} {
			r1 in «ruleAName»«IF ruleInfoA===ruleInfoB» && IsDifferent«ruleAName»[r1,r2]«ENDIF»
			
			PRE_«ruleAName»[r1,m0] && PRE_«ruleBName»[r2,m0]
			
			one m1 && POST_«ruleAName»[r1, m0, m1] && (not PRE_«ruleBName»[r2,m1])
			
			«IF creator»NoDeletionMarks[m0]«ENDIF»
			«IF breaker»NoUnknownMarks[m0]«ENDIF»
		}
		
		«IF hasConflict(ruleInfoA.value, ruleInfoB.value)===false»// It should not have conflict«ENDIF»
		run findConflictBetween«ruleAName»And«ruleBName» {}
		  for 0 but «defaultBound» «modelName.rootNodeName», exactly 1 FindConflictBetween«ruleAName»And«ruleBName», «IF ruleInfoA===ruleInfoB»exactly 2 «ruleAName»«ELSE»exactly 1 «ruleAName», exactly 1 «ruleBName»«ENDIF», exactly 2 «modelName.graphName»«FOR entry : boundA.allBounds», exactly «entry.value» «entry.key.generateScope»«ENDFOR»
		
		
		sig FindDivergenceBetween«ruleAName»And«ruleBName» {
			m0 : one «modelName.graphName»,
			m1 : one «modelName.graphName»,
			m2 : one «modelName.graphName»,
			r1 : one «ruleAName»,
			r2 : one «ruleBName»
		} {
			«IF ruleInfoA===ruleInfoB» r1 != r2 && IsDifferent«ruleAName»[r1,r2]«ENDIF»
			
			PRE_«ruleAName»[r1,m0] && PRE_«ruleBName»[r2,m0]
			
			POST_«ruleAName»[r1, m0, m1] && POST_«ruleBName»[r2, m0, m2]
			
			not PRE_«ruleAName»[r1,m2]
			
			not PRE_«ruleBName»[r2,m1]
		}
		
		// require manual checks if an instance is found
		run findDivergenceBetween«ruleAName»And«ruleBName» {}
			for 0 but «defaultBound» «modelName.rootNodeName», exactly 1 FindDivergenceBetween«ruleAName»And«ruleBName», «IF ruleInfoA===ruleInfoB»exactly 2 «ruleAName»«ELSE»exactly 1 «ruleAName», exactly 1 «ruleBName»«ENDIF», exactly 3 «modelName.graphName»«FOR entry : boundA.allBounds», exactly «entry.value» «entry.key.generateScope»«ENDFOR»
		
		'''
	}
	
	def isBuiltInFunction(String string) {
		string=='=' || string=='<>' || string=='!=' || string=='[]'
	}
	
	def ruleName(Rule rule, String direction, int cnt) '''«rule.name.toFirstUpper»Rule«direction»_«cnt»'''
	
	def ruleName(Rule rule, DerivedRuleKind kind, int cnt) '''«rule.name.toFirstUpper»Rule«kind.literal.toUpperCase»_«cnt»'''
	
	def isNotDeleted(Constant c) {
		if(c instanceof EnumConstant) {
			c.value === BiggPackage.Literals.PERSISTENT_MARK.getEEnumLiteral(PersistentMark.NOT_DELETED.literal)
		} else false
	}
	
	protected def boolean needsIntegrity(DerivedRule rule) {
		rule.ruleKind===DerivedRuleKind.CHECKER && rule.lhs.correspondence.nodes.size > 1
	}
	
	protected def generateRuleConstraint(String modelName, DerivedRule rule, int cnt) {
		val delta = rule.diffRule
		
		val allLHSNodes = rule.lhs.allNodes
		val allRHSNodes = rule.rhs.allNodes
		
		val allLHSEdges = rule.lhs.allEdges
		val allRHSEdges = rule.rhs.allEdges
		
		val deletedNodes = delta.leftDelta.allNodes
		val createdNodes = delta.rightDelta.allNodes
		val keptNodes = allLHSNodes.filter[l|allRHSNodes.exists[r|l.name==r.name]]
		
		val deletedEdges = delta.leftDelta.allEdges
		val createdEdges = delta.rightDelta.allEdges
		
//		val corrLinks = correspondenceLinks
		
		val lhsProperties = rule.lhs.allProperties.toList // should contain const and variable ref only
		val rhsProperties = rule.rhs.allProperties.toList // should contain const, variable ref (defined in source), and functioncall
		
		val changedProps = delta.leftDelta.allProperties
		
		val lhsVars = rule.lhs.getReferredVariables
		
		var trigger = new Trigger
		
		val direction = rule.ruleKind
		
//		val needsExtraIntegrity = rule.needsIntegrity
//		val focusedCorr = needsExtraIntegrity ? delta.rightDelta.correspondence.properties.get(0).eContainer as Node : null
//		val posMap = needsExtraIntegrity ? focusedCorr.type.EAllReferences.indexed.toMap([it.value])[it.key] : null
//		val focusedCorrEdges = needsExtraIntegrity ? rule.rhs.correspondence.edges.filter[it.source===focusedCorr].sortBy[posMap.get(it)] : null
//		val focusedNodeNames = needsExtraIntegrity ? focusedCorrEdges.map[target].map[name].toSet => [
//			it += focusedCorr.name
//		] : null
		
		'''
		// structure for «rule.ruleName(direction,cnt)»
		sig «rule.ruleName(direction,cnt)» extends «modelName.toFirstUpper»Rule {
			«FOR node : deletedNodes BEFORE '// deleted\n'»
			«node.name» : one «generateScope(node.type)»,
			«ENDFOR»
			«FOR node : keptNodes BEFORE '// kept\n'»
			«node.name» : one «generateScope(node.type)»,
			«ENDFOR»
			«FOR node : createdNodes BEFORE '// created\n'»
			«node.name» : one «generateScope(node.type)»,
			«ENDFOR»
			«FOR varEntry : lhsVars BEFORE '// property variables\n'»
			«varEntry.name» : one «generateScope(varEntry.type)»,
			«ENDFOR»
		} {
			_deletedNodes = «IF deletedNodes.isEmpty»none«ELSE»«FOR node : deletedNodes SEPARATOR '+'»«node.name»«ENDFOR»«ENDIF»
			
			_createdNodes = «IF createdNodes.isEmpty»none«ELSE»«FOR node : createdNodes SEPARATOR '+'»«node.name»«ENDFOR»«ENDIF»
			«IF (deletedNodes + keptNodes + createdNodes).size > 1»
			
			disj[«FOR node : deletedNodes + keptNodes + createdNodes SEPARATOR ', '»«node.name»«ENDFOR»]
			«ENDIF»
		}
		
		pred IsDifferent«rule.ruleName(direction,cnt)»[r1,r2:«rule.ruleName(direction,cnt)»] {
			«FOR node : deletedNodes SEPARATOR ' || '»r1.«node.name» != r2.«node.name»«ENDFOR»«IF !deletedNodes.empty && !keptNodes.empty» || «ENDIF»«FOR node : keptNodes SEPARATOR ' || '»r1.«node.name» != r2.«node.name»«ENDFOR»«IF (!deletedNodes.empty || !keptNodes.empty) && !lhsVars.empty» || «ENDIF»«FOR varEntry : lhsVars SEPARATOR ' || '»r1.«varEntry.name» != r2.«varEntry.name»«ENDFOR»
«««			
«««			«FOR node : createdNodes SEPARATOR ' || '»r1.«node.name» != r2.«node.name»«ENDFOR»
		}
		
		// nacs for «rule.ruleName(direction,cnt)»
		«FOR nac : rule.nacs»
		pred NAC_«rule.nacs.indexOf(nac)»_«rule.ruleName(direction,cnt)»[rule : «rule.ruleName(direction,cnt)», graph : «graphName(modelName)»] {
		«IF nac instanceof TriplePattern»
			«FOR node : nac.allNodes BEFORE 'some ' SEPARATOR ', ' AFTER ' | '»«node.name»:«generateScope(node.type)»«ENDFOR»
			«FOR node : nac.allNodes SEPARATOR ' && '»«node.name» in graph.nodes«trigger.set»«ENDFOR»
			«IF nac.allNodes.size>1» && disj[«FOR node : deletedNodes + keptNodes + createdNodes SEPARATOR ', '»rule.@«node.name»«ENDFOR», «FOR node : nac.allNodes SEPARATOR ', '»«node.name»«ENDFOR»]«ENDIF»«IF trigger.testAndRestart» && «ENDIF»
			«FOR edge : nac.allEdges SEPARATOR ' && '»«trigger.set»«IF edge.source.isDefinedIn(nac)»«edge.source.name»«ELSE»rule.@«edge.source.name»«ENDIF»->«IF edge.target.isDefinedIn(nac)»«edge.target.name»«ELSE»rule.@«edge.target.name»«ENDIF» in graph.«fieldName(edge.type)»«ENDFOR»«IF trigger.testAndRestart» && «ENDIF»
			«FOR prop : nac.allProperties SEPARATOR ' && '»«IF prop.value instanceof VariableRef»«IF (prop.eContainer as Node).isDefinedIn(nac)»«(prop.eContainer as Node).name»«ELSE»rule.@«(prop.eContainer as Node).name»«ENDIF»->rule.@«(prop.value as VariableRef).variable.name» in graph.«fieldName(prop.type)»«ELSE»«IF (prop.value as Constant).isNotDeleted» («IF (prop.eContainer as Node).isDefinedIn(nac)»«(prop.eContainer as Node).name»«ELSE»rule.@«(prop.eContainer as Node).name»«ENDIF»->kept in graph.«fieldName(prop.type)» || «IF (prop.eContainer as Node).isDefinedIn(nac)»«(prop.eContainer as Node).name»«ELSE»rule.@«(prop.eContainer as Node).name»«ENDIF»->unknown in graph.«fieldName(prop.type)»)«ELSE»«IF (prop.eContainer as Node).isDefinedIn(nac)»«(prop.eContainer as Node).name»«ELSE»rule.@«(prop.eContainer as Node).name»«ENDIF»->«(prop.value as Constant).toLiteral» in graph.«fieldName(prop.type)»«ENDIF»«ENDIF»«ENDFOR»
		«ELSE»
			«(nac as FunctionCall).toFunctionCall('rule.@')»
		«ENDIF»
		}
		pred NAC_«rule.nacs.indexOf(nac)»_«rule.ruleName(direction,cnt)»[«FOR ln : allLHSNodes SEPARATOR ', '»«ln.name»:«ln.type.generateScope»«ENDFOR», «FOR lp : lhsVars SEPARATOR ', ' AFTER ', '»«lp.name»:«lp.type.generateScope»«ENDFOR»graph : «graphName(modelName)»] {
		«IF nac instanceof TriplePattern»
			«FOR node : nac.allNodes BEFORE 'some ' SEPARATOR ', ' AFTER ' | '»«node.name»:«generateScope(node.type)»«ENDFOR»
			«FOR node : nac.allNodes SEPARATOR ' && '»«node.name» in graph.nodes«trigger.set»«ENDFOR»
			«IF nac.allNodes.size>1» && disj[«FOR node : allLHSNodes SEPARATOR ', '»«node.name»«ENDFOR», «FOR node : nac.allNodes SEPARATOR ', '»«node.name»«ENDFOR»]«ENDIF»
			«FOR edge : nac.allEdges BEFORE '''«IF trigger.testAndRestart» && «ENDIF»''' SEPARATOR ' && '»«trigger.set»«edge.source.name»->«edge.target.name» in graph.«fieldName(edge.type)»«ENDFOR»
			«FOR prop : nac.allProperties BEFORE '''«IF trigger.testAndRestart» && «ENDIF»''' SEPARATOR ' && '»«IF prop.value instanceof VariableRef»«(prop.eContainer as Node).name»->«(prop.value as VariableRef).variable.name» in graph.«fieldName(prop.type)»«ELSE»«IF (prop.value as Constant).isNotDeleted» («(prop.eContainer as Node).name»->kept in graph.«fieldName(prop.type)» || «(prop.eContainer as Node).name»->unknown in graph.«fieldName(prop.type)»)«ELSE»«(prop.eContainer as Node).name»->«(prop.value as Constant).toLiteral» in graph.«fieldName(prop.type)»«ENDIF»«ENDIF»«ENDFOR»
		«ELSE»
			«(nac as FunctionCall).toFunctionCall('')»
		«ENDIF»
		}
		«ENDFOR»
		
		// pre-condition for «rule.ruleName(direction,cnt)»
		pred VALID_«rule.ruleName(direction,cnt)»[rule : «rule.ruleName(direction,cnt)», graph : «graphName(modelName)»] {
			«FOR node : createdNodes»
			not rule.@«node.name» in graph.nodes
			
			«ENDFOR»
		}
		
		pred PRE_«rule.ruleName(direction,cnt)»[rule : «rule.ruleName(direction,cnt)», graph : «graphName(modelName)»] {
			«FOR node : allLHSNodes»
			rule.@«node.name» in graph.nodes
			
			«ENDFOR»
			«FOR edge : allLHSEdges»
			rule.@«edge.source.name»->rule.@«edge.target.name» in graph.«fieldName(edge.type)»
			
			«ENDFOR»
			«FOR prop : lhsProperties»
			«IF prop.value instanceof VariableRef»
			rule.@«(prop.eContainer as Node).name»->rule.@«(prop.value as VariableRef).variable.name» in graph.«fieldName(prop.type)»
			«ELSE»
			«IF (prop.value as Constant).isNotDeleted»
			(rule.@«(prop.eContainer as Node).name»->kept in graph.«fieldName(prop.type)» || rule.@«(prop.eContainer as Node).name»->unknown in graph.«fieldName(prop.type)»)
			«ELSE»
			rule.@«(prop.eContainer as Node).name»->«(prop.value as Constant).toLiteral» in graph.«fieldName(prop.type)»
			«ENDIF»
			«ENDIF»
			
			«ENDFOR»
			VALID_«rule.ruleName(direction,cnt)»[rule, graph]
«««			«FOR node : createdNodes»
«««			not rule.@«node.name» in graph.nodes
«««						
«««			«ENDFOR»
«««			«IF rule.ruleKind===DerivedRuleKind.SOURCE_DESTROYER || rule.ruleKind===DerivedRuleKind.VIEW_DESTROYER»
«««			«val nac = rule.findInvertibleNAC»
«««			«val negCorr = nac.correspondence.nodes.get(0)»
«««			«val forbadCorsEdges = nac.correspondence.edges.filter[it.source===negCorr && deletedNodes.contains(it.target)]»
«««			no __«negCorr.name» : «negCorr.type.generateScope» | __«negCorr.name»->kept in graph.@«BiggPackage.Literals.TRANSLATION_OBJECT__STATUS.fieldName» && «FOR e : forbadCorsEdges SEPARATOR ' && '»(__«negCorr.name»->rule.@«e.target.name» in graph.@«fieldName(e.type)»)«ENDFOR»
«««			
«««			«ENDIF»
«««			may be removed
«««			«FOR edge : createdEdges»
«««			not rule.@«edge.source.name»->rule.@«edge.target.name» in graph.«fieldName(edge.type)»
«««						
«««			«ENDFOR»

			«FOR nac : rule.nacs»
			not NAC_«rule.nacs.indexOf(nac)»_«rule.ruleName(direction,cnt)»[rule, graph]
			
			«ENDFOR»
		}
		pred PRE_«rule.ruleName(direction,cnt)»[graph : «graphName(modelName)»] {«trigger.reset»
			«FOR ln : allLHSNodes BEFORE 'some ' SEPARATOR ', ' AFTER '|'»«ln.name»:«ln.type.generateScope»«ENDFOR»
			«FOR lp : lhsVars BEFORE 'some ' SEPARATOR ', ' AFTER '|'»«lp.name»:«lp.type.generateScope»«ENDFOR»
			«FOR node : allLHSNodes SEPARATOR ' && '»«node.name» in graph.nodes«trigger.set»«ENDFOR»
			«FOR edge : allLHSEdges» && «edge.source.name»->«edge.target.name» in graph.«fieldName(edge.type)»«ENDFOR»
			«FOR prop : lhsProperties BEFORE '''«IF trigger.testAndRestart»&& «ENDIF»''' SEPARATOR ' && '»«trigger.set»«IF prop.value instanceof VariableRef»«(prop.eContainer as Node).name»->«(prop.value as VariableRef).variable.name» in graph.«fieldName(prop.type)»«ELSE»«IF (prop.value as Constant).isNotDeleted»(«(prop.eContainer as Node).name»->kept in graph.«fieldName(prop.type)» || «(prop.eContainer as Node).name»->unknown in graph.«fieldName(prop.type)»)«ELSE»«(prop.eContainer as Node).name»->«(prop.value as Constant).toLiteral» in graph.«fieldName(prop.type)»«ENDIF»«ENDIF»«ENDFOR»
			«FOR nac : rule.nacs BEFORE '''«IF trigger.testAndRestart»&& «ENDIF»''' SEPARATOR ' && '»not NAC_«rule.nacs.indexOf(nac)»_«rule.ruleName(direction,cnt)»[«FOR ln : allLHSNodes SEPARATOR ', '»«ln.name»«ENDFOR», «FOR lp : lhsVars SEPARATOR ', ' AFTER ', '»«lp.name»«ENDFOR»graph]«ENDFOR»
		}
		
		// post-condition for «rule.ruleName(direction,cnt)»
		pred POST_«rule.ruleName(direction,cnt)»[rule : «rule.ruleName(direction,cnt)», pre, post : «graphName(modelName)»] {
			let _curNodes = (pre.nodes - rule._deletedNodes) + rule._createdNodes |
				let _potentialEdges = _curNodes -> _curNodes |
					post.nodes = pre.nodes - rule._deletedNodes + rule._createdNodes
					«FOR r : allReferences»
					&& post.«fieldName(r)» = ((pre.«fieldName(r)» & _potentialEdges)«FOR dl : deletedEdges.filter[it.type===r] BEFORE ' - (' SEPARATOR '+' AFTER ')'»rule.@«dl.source.name»->rule.@«dl.target.name»«ENDFOR»)«FOR cl : createdEdges.filter[it.type===r] BEFORE ''' «IF r.many»+«ELSE»++«ENDIF» (''' SEPARATOR '+' AFTER ')'»rule.@«cl.source.name»->rule.@«cl.target.name»«ENDFOR»
					«ENDFOR»
					«FOR r : allAttributes»
					&& post.«fieldName(r)» = ((_curNodes<:pre.«fieldName(r)»)«FOR dl : changedProps.filter[it.type===r] BEFORE ' - (' SEPARATOR '+' AFTER ')'»rule.@«(dl.eContainer as Node).name»->«IF dl.value instanceof VariableRef»rule.@«(dl.value as VariableRef).variable.name»«ELSE»«IF (dl.value as Constant).isNotDeleted»unknown«ELSE»«(dl.value as Constant).toLiteral»«ENDIF»«ENDIF»«ENDFOR»)«FOR cl : rhsProperties.filter[it.type===r] BEFORE ''' «IF r.many»+«ELSE»++«ENDIF» (''' SEPARATOR '+' AFTER ')'»rule.@«(cl.eContainer as Node).name»->«IF cl.value instanceof VariableRef»rule.@«(cl.value as VariableRef).variable.name»«ELSEIF cl.value instanceof Constant»«(cl.value as Constant).toLiteral»«ELSE»(let «nextRandomVar» = «(cl.value as FunctionCall).toFunctionCall('rule.@')» | «currentRandomVar»)«ENDIF»«ENDFOR»
					«ENDFOR»
		}
		
		«IF rule.ruleKind===DerivedRuleKind.CHECKER»
		pred REAPP_«rule.ruleName(direction,cnt)»[rule : «rule.ruleName(direction,cnt)», graph : «graphName(modelName)»] {
			«FOR node : allLHSNodes»
			rule.@«node.name» in graph.nodes
			
			«ENDFOR»
			«FOR edge : allLHSEdges»
			rule.@«edge.source.name»->rule.@«edge.target.name» in graph.«fieldName(edge.type)»
			
			«ENDFOR»
			«FOR prop : lhsProperties»
			«IF prop.value instanceof VariableRef»
			rule.@«(prop.eContainer as Node).name»->rule.@«(prop.value as VariableRef).variable.name» in graph.«fieldName(prop.type)»
			«ELSE»
			«IF (prop.value as Constant).isNotDeleted»
			rule.@«(prop.eContainer as Node).name»->kept in graph.«fieldName(prop.type)»
			«ELSEIF (prop.value as Constant).isUnknown»
			rule.@«(prop.eContainer as Node).name»->kept in graph.«fieldName(prop.type)»
			«ELSE»
			rule.@«(prop.eContainer as Node).name»->«(prop.value as Constant).toLiteral» in graph.«fieldName(prop.type)»
			«ENDIF»
			«ENDIF»
			
			«ENDFOR»
			«FOR node : createdNodes»
			not rule.@«node.name» in graph.nodes
						
			«ENDFOR»
«««			may be removed
«««			«FOR edge : createdEdges»
«««			not rule.@«edge.source.name»->rule.@«edge.target.name» in graph.«fieldName(edge.type)»
«««						
«««			«ENDFOR»
			«FOR nac : rule.nacs»
			not NAC_«rule.nacs.indexOf(nac)»_«rule.ruleName(direction,cnt)»[rule, graph]
			
			«ENDFOR»
		}
		
«««		«IF needsExtraIntegrity»
«««		pred INT_«rule.ruleName(direction,cnt)»[«focusedCorr.name» : «focusedCorr.type.generateScope», «FOR n:focusedCorrEdges.map[target] SEPARATOR ', '»«n.name» : «n.type.generateScope»«ENDFOR», graph : «modelName.graphName»] {
«««			«FOR n : rule.lhs.allNodes.filter[!focusedNodeNames.contains(it.name)] BEFORE 'some ' SEPARATOR ', ' AFTER ' |'»«n.name» : «n.type.generateScope»«ENDFOR»
«««			«FOR n : rule.lhs.allNodes.filter[!focusedNodeNames.contains(it.name)] SEPARATOR ' && ' AFTER ' &&'»«n.name» in graph.nodes«ENDFOR»
«««			«FOR e : rule.lhs.allEdges.filter[!(focusedNodeNames.contains(it.source.name) && focusedNodeNames.contains(it.target.name))] SEPARATOR ' && '»«e.source.name»->«e.target.name» in graph.@«e.type.fieldName»«ENDFOR»
««««««			ideally, we should also check nacs
«««		}
«««		«ENDIF»
		«ENDIF»
		'''
	}
	
	def boolean isUnknown(Constant c) {
		if(c instanceof EnumConstant) {
			c.value === BiggPackage.Literals.PERSISTENT_MARK.getEEnumLiteral(PersistentMark.UNKNOWN.literal)
		} else false
	}
	
	var randomVarCnt = 0
	
	def currentRandomVar() {
		'''__var«randomVarCnt-1»'''
	}
	
	def nextRandomVar() {
		'''__var«randomVarCnt++»'''
	}
	
	def toFunctionCall(FunctionCall call, String prefix) {
		val hasSetArg = call.arguments.exists[it instanceof FunctionCall || it instanceof Tuple]
		switch call.callee {
			case '[]' : '''«(call.arguments.get(1) as Constant).toLiteral».(«call.arguments.get(0).generateExpression(prefix,true)»)'''
			case '!=' : '''«call.arguments.get(0).generateExpression(prefix,hasSetArg)» != «call.arguments.get(1).generateExpression(prefix,hasSetArg)»'''
			case '<>' : '''«call.arguments.get(0).generateExpression(prefix,hasSetArg)» != «call.arguments.get(1).generateExpression(prefix,hasSetArg)»'''
			case '=' : '''«call.arguments.get(0).generateExpression(prefix,hasSetArg)» = «call.arguments.get(1).generateExpression(prefix,hasSetArg)»'''
			default : {
				'''«call.callee»«call.arguments.size»[«FOR arg : call.arguments SEPARATOR ', '»«arg.generateExpression(prefix,true)»«ENDFOR»]'''
			}
		}
	}
	
	def CharSequence generateExpression(Argument arg, String prefix, boolean toMap) {
		switch arg {
			FunctionCall : arg.toFunctionCall(prefix)
			VariableRef : '''«IF toMap»0->«ENDIF»(«prefix»«arg.variable.name»)'''
			Constant : '''«IF toMap»0->«ENDIF»«arg.stringRepresentation»''' // do not work for string constant!!
			Tuple : '''«FOR a : arg.arguments.indexed BEFORE '(' SEPARATOR '+' AFTER ')'»«a.key»->«a.value.generateExpression(prefix,false)»«ENDFOR»'''
		}
	}
	
	def toLiteral(Constant value) {
		value.stringRepresentation
	}
	
	def allProperties(TriplePattern pattern) {
		if(pattern===null) Collections.emptyList
		else {
			((pattern.source?.nodes ?: Collections.emptyList)
			+ (pattern.correspondence?.nodes ?: Collections.emptyList)
			+ (pattern.target?.nodes ?: Collections.emptyList)).map[it.properties].flatten
		}
	}
	
	val Map<String, Set<EClass>> ruleCorrespondenceTypes = new HashMap
	
	def analyze(java.util.List<edu.ustb.sei.mde.bigg.DerivedRule> checkers) {
		checkers.forEach[
			val delta = it.diffRule
			val corrTypes = delta.rightDelta.allProperties.filter[it.type===BiggPackage.Literals.TRANSLATION_OBJECT__STATUS].map[(it.eContainer as Node).type]
			val set = ruleCorrespondenceTypes.computeIfAbsent(it.name)[new HashSet]
			set.addAll(corrTypes)
		]
	}
	
}

class Trigger {
	var flag = false
	def void set() {
		flag = true
	}
	def testAndRestart() {
		val f = flag
		flag = false
		return f
	}
	def test() {
		flag
	}
	def void reset() {
		flag = false
	}
}

class RuleAffection {
	val Map<Rule, Set<EClass>> requiredClasses = newHashMap;
	val Map<Rule, Set<EClass>> forbadClasses = newHashMap;
	val Map<Rule, Set<EReference>> requiredReferences = newHashMap;
	val Map<Rule, Set<EReference>> forbadReferences = newHashMap;
	val Map<Rule, Set<Pair<EClass,EAttribute>>> requiredAttributes = newHashMap;
	val Map<Rule, Set<Pair<EClass,EAttribute>>> forbadAttributes = newHashMap;
	
	new() {}
	
	protected def void collectRequired(Rule rule) {
		val allNodes = rule.lhs.allNodes
		
		requiredClasses.computeIfAbsent(rule)[allNodes.map[type].toSet]
		requiredReferences.computeIfAbsent(rule)[rule.lhs.allEdges.map[type].toSet]
		requiredAttributes.computeIfAbsent(rule)[allNodes.map[n|n.properties.map[n.type->type]].flatten.toSet]
	}
	
	protected def void collectForbad(Rule rule) {
		forbadClasses.computeIfAbsent(rule)[nacs.filter[it instanceof TriplePattern].map[it as TriplePattern].map[allNodes.map[type]].flatten.toSet]
		forbadReferences.computeIfAbsent(rule)[nacs.filter[it instanceof TriplePattern].map[it as TriplePattern].map[allEdges.map[type]].flatten.toSet]
		forbadAttributes.computeIfAbsent(rule)[nacs.filter[it instanceof TriplePattern].map[it as TriplePattern].map[allNodes.map[n|n.properties.map[n.type->type]].flatten].flatten.toSet]
	}
	
	def boolean willCancelDueToCreation(EClass type, Rule rule) {
		var set = forbadClasses.get(rule)
		if(set === null) {
			collectForbad(rule)
			set = forbadClasses.get(rule)
		}
		
		return set.contains(type)
	}
	
	def boolean willCancelDueToCreation(EReference type, Rule rule) {
		var set = forbadReferences.get(rule)
		if(set === null) {
			collectForbad(rule)
			set = forbadReferences.get(rule)
		}
		
		return set.contains(type)
	}
	
	def boolean willCancelDueToCreation(EClass nodeType, EAttribute type, Rule rule) {
		var set = forbadAttributes.get(rule)
		if(set === null) {
			collectForbad(rule)
			set = forbadAttributes.get(rule)
		}
		
		set.exists[pair| pair.value===type && pair.key.isSuperTypeOf(nodeType) ]
	}
	
	def boolean willCancelDueToDeletion(EClass type, Rule rule) {
		var set = requiredClasses.get(rule)
		if(set === null) {
			collectRequired(rule)
			set = requiredClasses.get(rule)
		}
		
		return set.contains(type)
	}
	
	def boolean willCancelDueToDeletion(EReference type, Rule rule) {
		var set = requiredReferences.get(rule)
		if(set === null) {
			collectRequired(rule)
			set = requiredReferences.get(rule)
		}
		
		return set.contains(type)
	}
	
	def boolean willCancelDueToDeletion(EClass nodeType, EAttribute type, Rule rule) {
		var set = requiredAttributes.get(rule)
		if(set === null) {
			collectRequired(rule)
			set = requiredAttributes.get(rule)
		}
		
		set.exists[pair| pair.value===type && pair.key.isSuperTypeOf(nodeType) ]
	}
}

class BoundMap {
	val boundMap = new HashMap<EClass, Integer>
	def allBounds() {
		boundMap.entrySet
	}
	
	def get(EClass type) {
		boundMap.getOrDefault(type, 0)
	}
	def void add(EClass type) {
		val size = boundMap.getOrDefault(type, 0)
		boundMap.put(type, size + 1)
	}
	
	static def add(BoundMap l, BoundMap r) {
		val o = new BoundMap
		val keySet = new HashSet<EClass>
		keySet += l.boundMap.keySet
		keySet += r.boundMap.keySet
		
		keySet.forEach[k|
			val ls = l.get(k)
			val rs = r.get(k)
			if(ls===0 && rs===0) return;
			o.boundMap.put(k, ls + rs)
		]
		
		return o
	}
	
	def union(BoundMap r) {
		val l = this
		val o = this
		
		val keySet = new HashSet<EClass>
		keySet += l.boundMap.keySet
		keySet += r.boundMap.keySet
		
		keySet.forEach[k|
			val ls = l.get(k)
			val rs = r.get(k)
			if(ls===0 && rs===0) return;
			o.boundMap.put(k, Math.max(ls, rs))
		]
		
		return o
	}
	def remove(EClass clazz) {
		boundMap.remove(clazz)
	}
	def add(BoundMap r) {
		val l = this
		val o = this
		
		val keySet = new HashSet<EClass>
		keySet += l.boundMap.keySet
		keySet += r.boundMap.keySet
		
		keySet.forEach[k|
			val ls = l.get(k)
			val rs = r.get(k)
			if(ls===0 && rs===0) return;
			o.boundMap.put(k, ls + rs)
		]
		
		return o
	}
}
