package edu.ustb.sei.mde.bigg.generator

import edu.ustb.sei.mde.bigg.Rule
import edu.ustb.sei.mde.bigg.TriplePattern
import edu.ustb.sei.mde.bigg.Condition
import edu.ustb.sei.mde.bigg.Pattern
import edu.ustb.sei.mde.bigg.Variable
import edu.ustb.sei.mde.bigg.VariableRef
import edu.ustb.sei.mde.bigg.FunctionCall
import edu.ustb.sei.mde.bigg.Tuple
import edu.ustb.sei.mde.bigg.BiggFactory
import edu.ustb.sei.mde.bigg.Node
import edu.ustb.sei.mde.bigg.DerivedRuleKind
import edu.ustb.sei.mde.bigg.BiggPackage
import java.util.HashMap
import edu.ustb.sei.mde.bigg.PersistentMark

class RuleRelationUtil {
	extension BiGGRuleUtil = new BiGGRuleUtil
	
	static public val DESTROYER_KEY = 'destroyer'
	static public val BICREATOR_KEY = 'biCreator'
	static public val CREATOR_KEY = 'creator'
	static public val BIBREAKER_KEY = 'biBreaker'
	static public val BREAKER_KEY = 'breaker'
	static public val VALUE_KEY = 'value'
	static public val SOURCE_INT_KEY = 'sourceIntegrity'
	
	def deriveChecker(Rule normalizedRule) {

		val destoryer = normalizedRule.sourceDestroyer
		val relCreator = normalizedRule.bidirectionalRelationCreator
		val relBreaker = normalizedRule.bidirectionalRelationBreaker
		val value = normalizedRule.valueRule
		val srcInt = normalizedRule.sourceIntegrityRule
		
		val checker = if(srcInt.key) {
				normalizedRule.deriveCheckerFromSourceIntegrity
			} else if (destoryer.key) {
				normalizedRule.deriveCheckerFromSourceDestoryer
			} else if (relCreator.key) {
				normalizedRule.deriveCheckerFromBidirectionalRelationCreator
			} else if (relCreator.value.key) {
				normalizedRule.deriveCheckerFromRelationCreator
			} else if (relBreaker.key) {
				//normalizedRule.deriveCheckerFromBidirectionalRelationBreaker
				null
			} else if (relBreaker.value.key) {
				//normalizedRule.deriveCheckerFromRelationBreaker
				null
			} else if (value.key) {
				null
			} else
				null

		if(checker !== null) checker.forEach[it.normalizeRelation(value.key)]
		
		return checker
	}
	
	def deriveBackwardOperation(Rule normalizedRule) {
		val destoryer = normalizedRule.sourceDestroyer
		val relCreator = normalizedRule.bidirectionalRelationCreator
		val relBreaker = normalizedRule.bidirectionalRelationBreaker
		val value = normalizedRule.valueRule
		val srcInt = normalizedRule.sourceIntegrityRule
		
		val bwdRules = if(srcInt.key) {
				null
			} else if (destoryer.key) {
				normalizedRule.deriveBackwardRuleFromSourceDestoryer
			} else if (relCreator.key) {
				normalizedRule.deriveBackwardRuleFromBidirectionalRelationCreator
			} else if (relCreator.value.key) {
				normalizedRule.deriveBackwardRuleFromRelationCreator
			} else if (relBreaker.key) {
				normalizedRule.deriveBackwardRuleFromBidirectionalRelationBreaker
			} else if (relBreaker.value.key) {
				normalizedRule.deriveBackwardRuleFromRelationBreaker
			} else if (value.key) {
				normalizedRule.deriveBackwardRuleFromValueRule
			} else
				null

//		if(bwdRules !== null) bwdRules.forEach[it.normalizeRelation(value.key)]
		
		return bwdRules
	}
	
	def deriveForwardOperation(Rule normalizedRule) {
		val destoryer = normalizedRule.sourceDestroyer
		val relCreator = normalizedRule.bidirectionalRelationCreator
		val relBreaker = normalizedRule.bidirectionalRelationBreaker
		val value = normalizedRule.valueRule
		val srcInt = normalizedRule.sourceIntegrityRule
		
		val fwdRules = if (srcInt.key) {
				normalizedRule.deriveForwardRuleFromSourceIntegrityRule
			} else if (destoryer.key) {
				normalizedRule.deriveForwardRuleFromSourceDestoryer
			} else if (relCreator.key) {
				normalizedRule.deriveForwardRuleFromBidirectionalRelationCreator
			} else if (relCreator.value.key) {
				normalizedRule.deriveForwardRuleFromRelationCreator
			} else if (relBreaker.key) {
				normalizedRule.deriveForwardRuleFromBidirectionalRelationBreaker
			} else if (relBreaker.value.key) {
				normalizedRule.deriveForwardRuleFromRelationBreaker
			} else if (value.key) {
				normalizedRule.deriveForwardRuleFromValueRule
			} else
				null

//		if(fwdRules !== null) fwdRules.forEach[it.normalizeRelation(value.key)]
		
		return fwdRules
	}
	
	def deriveBackwardRuleFromValueRule(Rule rule) {
		val bwdRule = rule.deriveRule(DerivedRuleKind.BACKWARD_VALUE)
		
		val forwardable = bwdRule.nacs.findFirst[it.forwardable];
		if(forwardable!==null && (forwardable instanceof FunctionCall)) {
			// the function must be s' = bx(s,v)
			val args = (forwardable as FunctionCall).arguments;

			val postValue = if(args.get(0).isVariableOrTupleOfVariables) args.get(0) else args.get(1)
			val bigul = (if(args.get(0) === postValue) args.get(1) else args.get(0)) as FunctionCall
			val sourceValue = bigul.arguments.get(0)
			
			val postValuePosMap = if(postValue instanceof Tuple) {
				postValue.arguments.indexed.toMap([(it.value as VariableRef).variable])[
					it.key
				]
			} else {
				new HashMap<Variable,Integer> => [
					put((postValue as VariableRef).variable, 0)
				]
			}
			
			bwdRule.rhs.source.nodes.forEach [ n |
				n.properties.forEach[p|
					if(p.value instanceof VariableRef) {
						val id = postValuePosMap.get((p.value as VariableRef).variable)
						if(id!==null) {
							p.value = BiggFactory.eINSTANCE.createFunctionCall => [
								callee = '[]'
								arguments += bigul.copy
								arguments += BiggFactory.eINSTANCE.createIntegerConstant => [
									value = id
								]
							]
						}
					}
				]
			]
			
			if(args.get(0).isVariableOrTupleOfVariables) args.set(0, sourceValue.copy) else args.set(1, sourceValue.copy)
				
			#[bwdRule]
		} else #[]
	}
	
	def deriveForwardRuleFromValueRule(Rule rule) {
		val fwdRule = rule.deriveRule(DerivedRuleKind.FORWARD_VALUE)
		
		val forwardable = fwdRule.nacs.findFirst[it.forwardable];
		if(forwardable!==null && (forwardable instanceof FunctionCall)) {
			// the function must be s' = bx(s,v)
			val args = (forwardable as FunctionCall).arguments;

			val postValue = if(args.get(0).isVariableOrTupleOfVariables) args.get(0) else args.get(1)
			val bigul = (if(args.get(0) === postValue) args.get(1) else args.get(0)) as FunctionCall
			val viewValue = bigul.arguments.get(1)
			
			val viewValuePosMap = if(viewValue instanceof Tuple) {
				viewValue.arguments.indexed.toMap([(it.value as VariableRef).variable])[
					it.key
				]
			} else {
				new HashMap<Variable,Integer> => [
					put((viewValue as VariableRef).variable, 0)
				]
			}
			
			fwdRule.rhs = fwdRule.lhs.copy
			
			fwdRule.rhs.target.nodes.forEach [ n |
				n.properties.forEach[p|
					if(p.value instanceof VariableRef) {
						val id = viewValuePosMap.get((p.value as VariableRef).variable)
						if(id!==null) {
							p.value = BiggFactory.eINSTANCE.createFunctionCall => [
								callee = '[]'
								arguments += bigul.copy => [
									it.arguments.remove(1)
								]
								arguments += BiggFactory.eINSTANCE.createIntegerConstant => [
									value = id
								]
							]
						}
					}
				]
			]
			
			if(args.get(0).isVariableOrTupleOfVariables) args.set(0, viewValue.copy) else args.set(1, viewValue.copy)
			bigul.arguments.remove(1)
				
			#[fwdRule]
		} else #[]
	}
	
//	// FIXME
//	def deriveRelationFromValueRule(Rule rule) {
//		val relation = rule.deriveRule(DerivedRuleKind.RELATION)
//		
//		val forwardable = relation.nacs.findFirst[it.forwardable];
//		if(forwardable!==null && (forwardable instanceof FunctionCall)) {
//			// the function must be s' != bx(s,v)
//			val args = (forwardable as FunctionCall).arguments;
//
//			val postValue = if(args.get(0).isVariableOrTupleOfVariables) args.get(0) else args.get(1)
//			val bigul = (if(args.get(0) === postValue) args.get(1) else args.get(0)) as FunctionCall
//			val sourceValue = bigul.arguments.get(0)
//
//			if (args.get(0) === postValue) {
//				args.set(0, sourceValue.copy)
//			} else {
//				args.set(1, sourceValue.copy)
//			}
//
//			(forwardable as FunctionCall).callee = '<>'
//
//			val sourceValueMap = relation.lhs.source.nodes.toMap([it.name]) [
//				it.properties.filter[value instanceof VariableRef].toMap([it.type])[(value as VariableRef).variable]
//			]
//
//			relation.rhs.source.nodes.forEach [ n |
//				n.properties.filter[value instanceof VariableRef].forEach [ p |
//					val sv = sourceValueMap.get(n.name)?.get(p.type)
////					relation.variables.remove((p.value as VariableRef).variable)
//					if (sv !== null) {
//						(p.value as VariableRef).variable = sv
//					}
//				]
//			]
//				
//				
//			#[relation]
//		} else #[]
//	}
	
	def normalizeRelation(Rule relation, boolean valueRel) {
		if(!valueRel) relation.removeAlignmentValueCondition
		
		// remove all vars that only occur in right hand side
		val varsOccInLhs = new HashMap<Variable, Integer>
		relation.lhs.allNodes.forEach[n|
			n.properties.forEach[p|
				p.eAllContents.forEach[
					if(it instanceof VariableRef) {
						val occ = varsOccInLhs.get(it.variable)
						if(occ===null) varsOccInLhs.put(it.variable,1)
						else varsOccInLhs.put(it.variable, occ+1)
					}
				]
			]
		]
		
		val varsOccInRhs = new HashMap<Variable, Integer>
		relation.rhs.allNodes.forEach[n|
			n.properties.forEach[p|
				p.eAllContents.forEach[
					if(it instanceof VariableRef) {
						val occ = varsOccInRhs.get(it.variable)
						if(occ===null) varsOccInRhs.put(it.variable,1)
						else varsOccInRhs.put(it.variable, occ+1)
					}
				]
			]
		]
		
		val varsOccInNac = new HashMap<Variable, Integer>
		relation.nacs.forEach[it.eAllContents.forEach[
			if (it instanceof VariableRef) {
				val occ = varsOccInNac.get(it.variable)
				if(occ === null) varsOccInNac.put(it.variable, 1) else varsOccInNac.put(it.variable, occ + 1)
			}
		]]
		
		val variablesTobeRemoved = relation.variables.filter[
			// lhs 0 rhs 0 nac 0, sum = 0, --
			// lhs 1 rhs 0 nac 0, sum = 1, --
			// lhs >1 rhs ? nac ?, sum >1
			// lhs 1 rhs 1 nac 0, sum = 1, --
			// lhs ? rhs >1 nac ?, sum >1,
			// lhs 1 rhs ? nac >=1, sum >1
			// lhs ? rhs 1 nac >=1, sum >1
			Math.max(varsOccInLhs.getOrDefault(it,0),varsOccInRhs.getOrDefault(it,0)) + varsOccInNac.getOrDefault(it,0) <= 1
		].toSet
		
		relation.lhs.allNodes.forEach[n|
			n.properties.removeIf[p|
				p.value instanceof VariableRef && variablesTobeRemoved.contains((p.value as VariableRef).variable)
			]
		]
		
		relation.rhs.allNodes.forEach[n|
			n.properties.removeIf[p|
				p.value instanceof VariableRef && variablesTobeRemoved.contains((p.value as VariableRef).variable)
			]
		]
		
		relation.variables.removeAll(variablesTobeRemoved)
		
		return relation
	}
	
	
	
//	def deriveRelationFromSourceDestoryer(Rule rule) {
//		val relation = rule.deriveRule(DerivedRuleKind.RELATION)
//		val delta = rule.diffRule
//		val forwardable = relation.nacs.findFirst[it.forwardable];
//		if(forwardable!==null && (forwardable instanceof TriplePattern)) {
//			val corr = (forwardable as TriplePattern).correspondence.nodes.get(0)
//
//			relation.nacs.remove(forwardable)
//			(forwardable as TriplePattern).mergeTo(relation.lhs)
//			relation.rhs = relation.lhs.copy
//			
//			relation.lhs.removeSubGraph(corr, delta.unchangedNodes.map[name].toSet)
//			
//			#[relation]
//		} else #[]
//	}

	def deriveCheckerFromSourceIntegrity(Rule rule) {
		val checker = rule.deriveRule(DerivedRuleKind.CHECKER)
		
		val forwardable = checker.nacs.findFirst[it.error];
		if(forwardable!==null) {
			val corr = (forwardable as TriplePattern).correspondence.nodes.get(0)

			checker.nacs.remove(forwardable)
			(forwardable as TriplePattern).mergeTo(checker.lhs)
			checker.rhs = checker.lhs.copy
			
			checker.makeRuleCorrStatus(corr, false)
			
			#[checker]
		} else #[]
	}
	
	def deriveCheckerFromSourceDestoryer(Rule rule) {
		val checker = rule.deriveRule(DerivedRuleKind.CHECKER)
		
		val forwardable = checker.nacs.findFirst[it.forwardable];
		if(forwardable!==null && (forwardable instanceof TriplePattern)) {
			val corr = (forwardable as TriplePattern).correspondence.nodes.get(0)

			checker.nacs.remove(forwardable)
			
			(forwardable as TriplePattern).mergeTo(checker.lhs)
			checker.rhs = checker.lhs.copy
			
			checker.makeRuleCorrStatus(corr, false)
			
			#[checker]
		} else #[]
	}
	
	def deriveBackwardRuleFromSourceDestoryer(Rule rule) {
		val bwdRule = rule.deriveRule(DerivedRuleKind.SOURCE_DESTROYER)
		
		val forwardable = bwdRule.nacs.findFirst[it.forwardable];
		if(forwardable!==null && (forwardable instanceof TriplePattern)) {
			val corr = (forwardable as TriplePattern).correspondence.nodes.get(0)

			bwdRule.makeRuleCorrStatus(corr, true)
			
			#[bwdRule]
		} else #[]
	}

	def deriveForwardRuleFromSourceDestoryer(Rule rule) {
		val fwdRule = rule.deriveRule(DerivedRuleKind.FORWARD_RELATION_CREATOR)
		val forwardable = fwdRule.nacs.findFirst[it.forwardable];
		if(forwardable!==null && (forwardable instanceof TriplePattern)) {
			val corr = (forwardable as TriplePattern).correspondence.nodes.get(0)

			fwdRule.rhs = fwdRule.lhs.copy;
			(forwardable as TriplePattern).mergeTo(fwdRule.rhs)
			
			fwdRule.makeRuleCorrStatus(corr, false)
			
			#[fwdRule]
		} else #[]
	}
	
	def deriveForwardRuleFromSourceIntegrityRule(Rule rule) {
		val fwdRule = rule.deriveRule(DerivedRuleKind.FORWARD_RELATION_CREATOR)
		val forwardable = fwdRule.nacs.findFirst[it.error];
		if(forwardable!==null) {
			val corr = (forwardable as TriplePattern).correspondence.nodes.get(0)

			fwdRule.rhs = fwdRule.lhs.copy;
			(forwardable as TriplePattern).mergeTo(fwdRule.rhs)
			
			fwdRule.makeRuleCorrStatus(corr, false)
			
			#[fwdRule]
		} else #[]
	}
	
	def void makeRuleCorrStatus(Rule checker, Node corr, boolean isDeletion) {
		
		checker.lhs.correspondence.nodes.forEach[n|
			if(n.name==corr.name) {
				n.makeCorrStatus(isDeletion ? PersistentMark.DELETED : PersistentMark.UNKNOWN)
			} else {
				n.makeCorrStatus(PersistentMark.KEPT)
			}
		]
		
		checker.nacs.filter[it instanceof TriplePattern].map[it as TriplePattern].forEach[pat|
			pat.correspondence.nodes.forEach[n|
				n.makeCorrStatus(isDeletion ? PersistentMark.KEPT : PersistentMark.NOT_DELETED)
			]
		]
			
		checker.rhs.correspondence.nodes.forEach[n|
			n.makeCorrStatus(PersistentMark.KEPT)
		]
	}
	
	def void makeCorrStatus(Node node, PersistentMark status) {
		if(node.properties.exists[it.type=== BiggPackage.Literals.TRANSLATION_OBJECT__STATUS]) {}
		else {
			node.properties += BiggFactory.eINSTANCE.createProperty => [
				type = BiggPackage.Literals.TRANSLATION_OBJECT__STATUS
				value = BiggFactory.eINSTANCE.createEnumConstant => [
					value = BiggPackage.Literals.PERSISTENT_MARK.getEEnumLiteral(status.literal)
				]
			]			
		}
	}
	
//	def deriveRelationFromBidirectionalRelationCreator(Rule rule) {
//		val relation = rule.deriveRule(DerivedRuleKind.RELATION)
//		val delta = rule.diffRule
//		
//		val breakable = relation.findBreakableNACs
//		relation.nacs.removeAll(breakable)
//		
//		relation.lhs = relation.rhs
//		relation.rhs = relation.lhs.copy
//		relation.nacs.forEach[it.fixNAC(relation.lhs)]
//		
//		relation.lhs.removeSubGraph(delta.rightDelta.correspondence.nodes.get(0), delta.unchangedNodes.map[name].toSet)
//		
//		return #[relation]
//	}
	
	def deriveCheckerFromBidirectionalRelationCreator(Rule rule) {
		val checker = rule.deriveRule(DerivedRuleKind.CHECKER)
		val delta = rule.diffRule
		
		val breakable = checker.findBreakableNACs
		checker.nacs.removeAll(breakable)
		
		checker.lhs = checker.rhs
		checker.rhs = checker.lhs.copy
		checker.nacs.forEach[it.fixNAC(checker.lhs)]
		
		checker.makeRuleCorrStatus(delta.rightDelta.correspondence.nodes.get(0), false)
		
		return #[checker]
	}
	
	def deriveBackwardRuleFromBidirectionalRelationCreator(Rule rule) {
		val bwdRule = rule.deriveRule(DerivedRuleKind.BI_RELATION_CREATOR)
		val delta = rule.diffRule
		val originalCorr = delta.rightDelta.correspondence.nodes.get(0)
		
		bwdRule.makeRuleCorrStatus(originalCorr, false)
		
		return #[bwdRule]
	}

	def deriveForwardRuleFromBidirectionalRelationCreator(Rule rule) {
		val fwdRule = rule.deriveRule(DerivedRuleKind.BI_RELATION_CREATOR)
		val delta = rule.diffRule
		val corr = delta.rightDelta.correspondence.nodes.get(0)
		
		fwdRule.makeRuleCorrStatus(corr, false)
		
		return #[fwdRule]
	}
	
//	def deriveRelationFromRelationCreator(Rule rule) {
//		val relation = rule.deriveRule(DerivedRuleKind.RELATION)
//		val delta = rule.diffRule
//		
//		val breakable = relation.findBreakableNACs
//		relation.nacs.removeAll(breakable)
//		
//		relation.lhs = relation.rhs
//		relation.rhs = relation.lhs.copy
//		relation.nacs.forEach[it.fixNAC(relation.lhs)]
//		
//		val diff = rule.diffRule
//		relation.lhs.removeSubGraph(diff.rightDelta.correspondence.nodes.get(0), delta.unchangedNodes.map[name].toSet)
//		
//		return #[relation]
//	}
	
	def deriveCheckerFromRelationCreator(Rule rule) {
		val checker = rule.deriveRule(DerivedRuleKind.CHECKER)
		val delta = rule.diffRule
		
		val breakable = checker.findBreakableNACs
		checker.nacs.removeAll(breakable)
		
		checker.lhs = checker.rhs
		checker.rhs = checker.lhs.copy
		checker.nacs.forEach[it.fixNAC(checker.lhs)]
		
		checker.makeRuleCorrStatus(delta.rightDelta.correspondence.nodes.get(0), false)
		
		return #[checker]
	}
	
	def deriveBackwardRuleFromRelationCreator(Rule rule) {
		val bwdRule = rule.deriveRule(DerivedRuleKind.BACKWARD_RELATION_CREATOR)
		val delta = rule.diffRule
		val originalCorr = delta.rightDelta.correspondence.nodes.get(0)
		
		bwdRule.makeRuleCorrStatus(originalCorr, false)
		
		return #[bwdRule]
	}
	
	def deriveForwardRuleFromRelationCreator(Rule rule) {
		val fwdRule = rule.deriveRule(DerivedRuleKind.VIEW_DESTROYER)
		val delta = rule.diffRule
		val corr = delta.rightDelta.correspondence.nodes.get(0)
		
		val viewNodes = fwdRule.rhs.correspondence.edges.filter[it.source.name==corr.name].map[it.target].filter[
			fwdRule.rhs.target.nodes.contains(it) && !fwdRule.lhs.correspondence.edges.exists[e| it.name==e.target.name]
		].map[it.name].toSet
		
		fwdRule.rhs = fwdRule.lhs.copy;
		
		fwdRule.rhs.target.nodes.removeIf[viewNodes.contains(it.name)]
		fwdRule.rhs.target.edges.removeIf[
			viewNodes.contains(it.source.name) || viewNodes.contains(it.target.name)
		]
		
		fwdRule.makeRuleCorrStatus(corr, true)
		
		return #[fwdRule]
	}
	
	def void fixNAC(Condition condition, TriplePattern pattern) {
		if(condition instanceof TriplePattern) {
			condition.source.replace(pattern)
			condition.correspondence.replaceCorr(pattern)
			condition.target.replace(pattern)
		}
	}
	
	private def void replace(Pattern pattern, TriplePattern newScope) {
		pattern.nodes.replaceAll[
			newScope.getNode(it.name) ?: it
		]
		
		pattern.edges.forEach[
			it.source = newScope.getNode(it.source.name) ?: it.source
			it.target = newScope.getNode(it.target.name) ?: it.target
		]
	}
	
	private def void replaceCorr(Pattern pattern, TriplePattern newScope) {
		pattern.edges.forEach[
			it.target = newScope.getNode(it.target.name) ?: it.target
		]
	}
	
//	def deriveRelationFromBidirectionalRelationBreaker(Rule rule) {
//		val relNACs = rule.nacs.indexed.filter[it.value.isRelationNAC].toList
//		val delta = rule.diffRule
//		val unchanged = delta.unchangedNodes.map[name].toSet
//		val relations = relNACs.map[pair|
//			val relation = rule.deriveRule(DerivedRuleKind.RELATION)
//			val nac = relation.nacs.get(pair.key) as TriplePattern
//			val toBeDeleted = relNACs.map[relation.nacs.get(it.key)]
//			relation.nacs.removeAll(toBeDeleted)
//			nac.mergeTo(relation.lhs)
//			relation.rhs = relation.lhs.copy
//			relation.lhs.removeSubGraph(delta.leftDelta.correspondence.nodes.get(0), unchanged)
//			relation
//		]
//		
//		return relations
//	}
	
	@Deprecated
	def deriveCheckerFromBidirectionalRelationBreaker(Rule rule) {
		val relNACs = rule.nacs.indexed.filter[it.value.isRelationNAC].toList
		val delta = rule.diffRule
		val checkers = relNACs.map[pair|
			val checker = rule.deriveRule(DerivedRuleKind.CHECKER)
			val nac = checker.nacs.get(pair.key) as TriplePattern
			val toBeDeleted = relNACs.map[checker.nacs.get(it.key)]
			checker.nacs.removeAll(toBeDeleted)
			nac.mergeTo(checker.lhs)
			checker.rhs = checker.lhs.copy
			checker.makeRuleCorrStatus(delta.leftDelta.correspondence.nodes.get(0), true)
			checker
		]
		
		return checkers
	}
	
	def deriveBackwardRuleFromBidirectionalRelationBreaker(Rule rule) {
		val bwdRule = rule.deriveRule(DerivedRuleKind.BI_RELATION_BREAKER)
		val delta = rule.diffRule
		bwdRule.makeRuleCorrStatus(delta.leftDelta.correspondence.nodes.get(0), true)
		return #[bwdRule]
	}

	def deriveForwardRuleFromBidirectionalRelationBreaker(Rule rule) {
		val fwdRule = rule.deriveRule(DerivedRuleKind.BI_RELATION_BREAKER)
		val delta = rule.diffRule
		val corr = delta.leftDelta.correspondence.nodes.get(0)
		
		fwdRule.makeRuleCorrStatus(corr, true)
		
		return #[fwdRule]
	}
	
//	def deriveRelationFromRelationBreaker(Rule rule) {
//		val relNACs = rule.nacs.indexed.filter[it.value.isRelationNAC].toList
//		val delta = rule.diffRule
//		val unchanged = delta.unchangedNodes.map[name].toSet
//		val relations = relNACs.map[pair|
//			val relation = rule.deriveRule(DerivedRuleKind.RELATION)
//			val nac = relation.nacs.get(pair.key) as TriplePattern
//			val toBeDeleted = relNACs.map[relation.nacs.get(it.key)]
//			relation.nacs.removeAll(toBeDeleted)
//			nac.mergeTo(relation.lhs)
//			relation.rhs = relation.lhs.copy
//			relation.lhs.removeSubGraph(delta.leftDelta.correspondence.nodes.get(0), unchanged)
//			relation
//		]
//		
//		return relations
//	}
	
	@Deprecated
	def deriveCheckerFromRelationBreaker(Rule rule) {
		val relNACs = rule.nacs.indexed.filter[it.value.isRelationNAC].toList
		val delta = rule.diffRule
		val checkers = relNACs.map[pair|
			val checker = rule.deriveRule(DerivedRuleKind.CHECKER)
			val nac = checker.nacs.get(pair.key) as TriplePattern
			val toBeDeleted = relNACs.map[checker.nacs.get(it.key)]
			checker.nacs.removeAll(toBeDeleted)
			nac.mergeTo(checker.lhs)
			checker.rhs = checker.lhs.copy
			checker.makeRuleCorrStatus(delta.leftDelta.correspondence.nodes.get(0), true)
			checker
		]
		
		return checkers
	}
	
	def deriveBackwardRuleFromRelationBreaker(Rule rule) {
		val bwdRule = rule.deriveRule(DerivedRuleKind.BACKWARD_RELATION_BREAKER)
		val delta = rule.diffRule
		bwdRule.makeRuleCorrStatus(delta.leftDelta.correspondence.nodes.get(0), true)
		return #[bwdRule]
	}
	
	def deriveForwardRuleFromRelationBreaker(Rule rule) {
		val fwdRule = rule.deriveRule(DerivedRuleKind.FORWARD_RELATION_BREAKER)
		val delta = rule.diffRule
		val corr = delta.leftDelta.correspondence.nodes.get(0)
		
		val srcNodes = fwdRule.lhs.correspondence.edges.filter[it.source.name==corr.name].map[it.target].filter[
			fwdRule.lhs.source.nodes.contains(it) && !fwdRule.rhs.correspondence.edges.exists[e| it.name==e.target.name]
		].map[it.name].toSet
		
		fwdRule.rhs = fwdRule.lhs.copy
		
		fwdRule.rhs.source.nodes.removeIf[srcNodes.contains(it.name)]
		fwdRule.rhs.source.edges.removeIf[
			srcNodes.contains(it.source.name) || srcNodes.contains(it.target.name)
		]
		
		fwdRule.makeRuleCorrStatus(corr, true)
		return #[fwdRule]
	}
}