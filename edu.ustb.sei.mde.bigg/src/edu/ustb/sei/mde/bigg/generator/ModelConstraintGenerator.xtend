package edu.ustb.sei.mde.bigg.generator

import edu.ustb.sei.mde.bigg.BiggPackage
import java.util.ArrayList
import java.util.Collections
import java.util.HashMap
import java.util.HashSet
import java.util.Map
import java.util.Set
import java.util.function.Function
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import java.util.Collection
import edu.ustb.sei.mde.bigg.BiggProgram

class ModelConstraintGenerator {
	
	def static void main(String[] args) {
		new ModelConstraintGenerator().generateTripleGraph(
				'CD2RDBMS',
				URI.createFileURI('/Volumes/Macintosh HD Data/Eclipse Projects/xtext_new/edu.ustb.sei.mde.bigg/model/test/CD.ecore'),
				URI.createFileURI('/Volumes/Macintosh HD Data/Eclipse Projects/xtext_new/edu.ustb.sei.mde.bigg/model/test/Corr.ecore'),
				URI.createFileURI('/Volumes/Macintosh HD Data/Eclipse Projects/xtext_new/edu.ustb.sei.mde.bigg/model/test/rdbms.ecore')
		)
	}
	
	def generateTripleGraph(String modelName, EPackage left, EPackage corr, EPackage right) {
		val ePackages = #[left, corr, right]
		this.correspondencePackage = ePackages.get(1)
		generateMetamodel(ePackages, modelName, null)
	}
	
	def generateTripleGraph(String modelName, URI left, URI corr, URI right) {
		val ePackages = #[left, corr, right].map[uri| loadMetamodel(uri)]
		this.correspondencePackage = ePackages.get(1)
		generateMetamodel(ePackages, modelName, null)
	}
	
	val ResourceSet resourceSet;
	
	new() {
		resourceSet = new ResourceSetImpl
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
    		Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
    	resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
    		BiggPackage.eNAME, new XMIResourceFactoryImpl());
	}
	
	def EPackage loadMetamodel(URI uri) {
		val resource = resourceSet.getResource(uri, true)
	    val ePackage = resource.contents.get(0) as EPackage
	    return ePackage
	}
	
	def generateMetamodel(EPackage[] ePackages,String modelName, Function<Map<EClass, Set<EClass>>, CharSequence> additional) {
    	ePackages.forEach[ePackage|
	    	ePackage.iterateEPackage
    	]
    	
    	if(!attributes.contains(BiggPackage.Literals.TRANSLATION_OBJECT__STATUS))
    		attributes += BiggPackage.Literals.TRANSLATION_OBJECT__STATUS
    	
    	generateMetamodel(modelName, additional)
	}
	
	val allPackages = new HashSet<EPackage>
	var EPackage correspondencePackage 
	
	val typeScope = new HashMap<EClass, Set<EClass>>
	val allConcreteClasses = new HashSet<EClass>
	val references = new ArrayList<EReference>
	val attributes = new ArrayList<EAttribute>
	val enumerations = new HashSet<EEnum>
	
	def graphName(String modelName) {
		'''«modelName.toFirstUpper»Graph'''
	}
	
	protected def generateMetamodel(String modelName, Function<Map<EClass, Set<EClass>>, CharSequence> additional) {
		
		val handled = new HashSet<EReference>
		val containments = references.filter[containment].toList
		val corr = correspondenceLinks
		
		'''
		abstract sig «modelName.rootNodeName» {}
		enum PersistentMark {
			unknown,
			deleted,
			kept
		}
		abstract sig CorrNode extends «modelName.rootNodeName» {}
		sig ExternalNode extends «modelName.rootNodeName» {}
		enum Boolean {true, false}
		
		«FOR e : enumerations.filter[it!==BiggPackage.Literals.PERSISTENT_MARK]»
		enum «e.name» {«FOR l : e.ELiterals SEPARATOR ','»«l.name»«ENDFOR»}
		«ENDFOR»
		
		«FOR c : allConcreteClasses»
		sig «c.toSigName» extends «IF c.isCorr»CorrNode«ELSE»«modelName.rootNodeName»«ENDIF» {}
		«ENDFOR»
		
		sig «modelName.graphName» {
			nodes : set «modelName.toFirstUpper»Node,
			
			«FOR r : references»
			«r.fieldName» : (nodes & («r.EContainingClass.generateScope»)) «r.generateSourceMultiplicity»-> «r.generateTargetMultiplicity» (nodes & («r.EReferenceType.generateScope»)),
			«ENDFOR»
			
			«FOR r : attributes»
			«IF r===BiggPackage.Literals.TRANSLATION_OBJECT__STATUS»
			«r.fieldName» : (nodes & CorrNode) -> one PersistentMark,
			«ELSE»
			«r.fieldName» : (nodes & («r.EContainingClass.generateScope»)) -> «r.generateTargetMultiplicity» «r.EAttributeType.generateScope»,
			«ENDIF»
			«ENDFOR»
		} {
			«IF containments.empty===false && containments.mayBeCyclic»
			// Constraints for containment
			let allContainmentReferences = «FOR r : containments SEPARATOR '+'»«r.fieldName»«ENDFOR» |
			  (all n1,n2 : nodes | n1!=n2 => disj[n1.allContainmentReferences, n2.allContainmentReferences])
			  && no iden & ^allContainmentReferences
			
			«ENDIF»
			«FOR r : references.filter[r|r.acyclic && r.mayBeCyclic] BEFORE '// Constraints for acyclic references\n'»
			no iden & ^«r.fieldName»
			
			«ENDFOR»
			«FOR r : references.filter[r|r.EOpposite!==null] BEFORE '// Constraints for opposite\n'»
			«IF !handled.contains(r.EOpposite) && handled.add(r)»
			«r.fieldName»=~«r.EOpposite.fieldName»
			
			«ENDIF»
			«ENDFOR»
			
			// soft cons
			«FOR r : references.filter[r|r.many && (r.upperBound>0 || r.lowerBound>0)]  BEFORE '// Constraints for multiplicity\n'»
			all n : (nodes & («r.EContainingClass.generateScope»)) | let size = #(n.«r.fieldName») | «IF r.lowerBound===r.upperBound»size=«r.lowerBound»«ELSEIF r.lowerBound>0 && r.upperBound>0»size>=«r.lowerBound» and size<=«r.upperBound»«ELSEIF r.lowerBound>0»size>=«r.lowerBound»«ELSEIF r.upperBound>0»size<=«r.upperBound»«ENDIF»
			
			«ENDFOR»
			«IF !corr.empty»
			// Constraints for correspondence
			«FOR l : corr»
			nodes<:«l.fieldName» = «l.fieldName»
			
			«ENDFOR»
			«ENDIF»
		}
		
		// mi[?=>mb] = mo
		pred ResetModelTA[mi, mb, mo : «modelName.graphName»] {
			mo.nodes = mi.nodes
			
			«FOR r : references»
			mo.«r.fieldName» = mi.«r.fieldName»
			
			«ENDFOR»
			
			«FOR r : attributes»
			«IF r===BiggPackage.Literals.TRANSLATION_OBJECT__STATUS»
			mo.«r.fieldName» = (mi.«r.fieldName») ++ (mo.nodes<:(mb.«r.fieldName»))
			«ELSE»
			mo.«r.fieldName» = mi.«r.fieldName»
			«ENDIF»
			
			«ENDFOR»
		}
		
		// mi[A=>B] = mo
		pred SetModelTA[mi, mo : «modelName.graphName», ta, tb : PersistentMark] {
			mo.nodes = mi.nodes
			
			«FOR r : references»
			mo.«r.fieldName» = mi.«r.fieldName»
			
			«ENDFOR»
			
			«FOR r : attributes»
			«IF r===BiggPackage.Literals.TRANSLATION_OBJECT__STATUS»
			mo.«r.fieldName» = (mi.«r.fieldName») ++ ((ta.~(mi.«r.fieldName»))->tb)
			«ELSE»
			mo.«r.fieldName» = mi.«r.fieldName»
			«ENDIF»
			
			«ENDFOR»
		}
		
		// mi(+)A[S] = mo
		pred SetModelTA[mi, mo : «modelName.graphName», ta : PersistentMark, ns : set «modelName.toFirstUpper»Node] {
			mo.nodes = mi.nodes
			
			«FOR r : references»
			mo.«r.fieldName» = mi.«r.fieldName»
			
			«ENDFOR»
			
			«FOR r : attributes»
			«IF r===BiggPackage.Literals.TRANSLATION_OBJECT__STATUS»
			mo.«r.fieldName» = (mi.«r.fieldName») ++ ((ns & mi.nodes)->ta)
			«ELSE»
			mo.«r.fieldName» = mi.«r.fieldName»
			«ENDIF»
			
			«ENDFOR»
		}
		
		pred SoftModelCons[model:«modelName.graphName»] {
			«FOR r : references.filter[r|r.many && (r.upperBound>0 || r.lowerBound>0)]  BEFORE '// Constraints for multiplicity\n'»
				all n : (model.nodes & («r.EContainingClass.generateScope»)) | let size = #(n.«r.fieldName») | «IF r.lowerBound===r.upperBound»size=«r.lowerBound»«ELSEIF r.lowerBound>0 && r.upperBound>0»size>=«r.lowerBound» and size<=«r.upperBound»«ELSEIF r.lowerBound>0»size>=«r.lowerBound»«ELSEIF r.upperBound>0»size<=«r.upperBound»«ENDIF»
				
			«ENDFOR»
		}
		
		pred NoDeletionMarks[model:«modelName.graphName»] {
			not deleted in model.nodes.(model.«fieldName(BiggPackage.Literals.TRANSLATION_OBJECT__STATUS)»)
		}
		
		pred NoUnknownMarks[model:«modelName.graphName»] {
			not unknown in model.nodes.(model.«fieldName(BiggPackage.Literals.TRANSLATION_OBJECT__STATUS)»)
		}
		
«««		pred IsIsomorphism[m1,m2:«modelName.graphName»] {
«««			some mappings : m1.nodes one -> one m2.nodes |
«««				(all n : m1.nodes & m2.nodes | n.mappings = n)
«««				«FOR r : references»
«««				&& (all n1, n2 : m1.nodes | (n1->n2 in m1.«r.fieldName» => n1.mappings -> n2.mappings in m2.«r.fieldName»))
«««				&& (all n1, n2 : m2.nodes | (n1->n2 in m2.«r.fieldName» => n1.~mappings -> n2.~mappings in m1.«r.fieldName»))
«««				«ENDFOR»
«««				«FOR r : attributes»
«««				&& (all n1 : m1.nodes | n1.(m1.«r.fieldName») = n1.mappings.(m2.«r.fieldName»))
«««				&& (all n1 : m2.nodes | n1.(m2.«r.fieldName») = n1.~mappings.(m1.«r.fieldName»))
«««				«ENDFOR»
«««		}
«««		
«««		pred DeleteSubGraph[deletedNodes : set «modelName.toFirstUpper»Node, deletedEdges : set «modelName.toFirstUpper»Node -> «modelName.toFirstUpper»Node, pre, post : «modelName.toFirstUpper»Graph] {
«««			«IF !corr.empty»let corrEdges = pre.(«FOR l : corr SEPARATOR '+'»«l.fieldName»«ENDFOR») |«ENDIF»
«««			let actualDeletedNodes = deletedNodes«IF !corr.empty» + deletedNodes.~corrEdges + (pre.nodes).(~(deletedEdges & corrEdges))«ENDIF» |
«««			  let actualDeletedEdges = deletedEdges + (actualDeletedNodes<:pre.edges) + (pre.edges:>actualDeletedNodes) |
«««			    post.nodes = pre.nodes - actualDeletedNodes && post.edges = pre.edges - actualDeletedEdges
«««		}
		«IF additional!==null»«additional.apply(typeScope)»«ENDIF»
		'''
	}
	
	def boolean isCorr(EClass clazz) {clazz.EPackage===correspondencePackage}
	
	
	protected def boolean mayBeCyclic(EReference reference) {
		mayBeCyclic(Collections.singleton(reference))
	}
	protected def boolean mayBeCyclic(Collection<EReference> references) {
		val srcClasses = references.map[EContainingClass].toSet
		val tarClasses = references.map[EReferenceType].toSet
		
		srcClasses.exists[sc|
			val sts = typeScope.get(sc) 
			tarClasses.exists[tc|
				val tts = typeScope.get(tc)
				return !Collections.disjoint(sts, tts)
			]
		]
	} 
	
	static val EdgeConstraint = 'http://www.ustb.edu.cn/sei/mde/metamodel/EdgeConstraint'
	
	def generateSourceMultiplicity(EReference reference) {
		val ann = reference.getEAnnotation(EdgeConstraint)
		
		if(ann===null) ''''''
		else {
			ann.details.get('source') ?: ''
		}
	}
	
	def generateTargetMultiplicity(EStructuralFeature reference) {
		if(reference.many) {
			if(reference.lowerBound===0) 'set'
			else 'some'
		} else {
			if(reference.EContainingClass.isCorr) 'lone'
			else if(reference.lowerBound===0) 'lone'
			else 'one'
		}
	}
	
	def isAcyclic(EReference reference) {
		if(reference.isContainment) true
		else {
			val ann = reference.getEAnnotation(EdgeConstraint)
			val source = ann?.details?.get('acyclic')
			if(source===null) false
			else Boolean.parseBoolean(source)
		}
	}
	
	def rootNodeName(String string) {
		'''«string.toFirstUpper»Node'''
	}
	
	def toSigName(EClass class1) '''«class1.EPackage.name.toFirstUpper»_«class1.name»'''
	
	protected def correspondenceTypes() {
		if(correspondencePackage===null) Collections.emptySet
		else allConcreteClasses.filter[n|n.EPackage===correspondencePackage]
	}
	
	def correspondenceLinks() {
		if(correspondencePackage===null) Collections.emptyList
		else references.filter[r|r.EContainingClass.EPackage===correspondencePackage] 
	}
	
	protected def void iterateEPackage(EPackage ePackage) {
		allPackages.add(ePackage)
		
		val iterator = ePackage.eAllContents
		while(iterator.hasNext) {
			val e = iterator.next
			if(e instanceof EClass) {
				if(e.abstract===false) allConcreteClasses.add(e)
				val scope = typeScope.computeIfAbsent(e)[new HashSet]
				if(e.abstract===false) scope.add(e)
				e.EAllSuperTypes.forEach[sc|
					val superScope = typeScope.computeIfAbsent(sc)[new HashSet]
					if(e.abstract===false) superScope.add(e)
				]
				e.EReferences.forEach[r|
					references.add(r)
				]
				e.EAttributes.forEach[a|
					attributes.add(a)
					if(a.EAttributeType instanceof EEnum) enumerations.add(a.EAttributeType as EEnum)
				]
			} else iterator.prune
		}
	}
	
	def fieldName(EStructuralFeature f) {
		'''«f.EContainingClass.toSigName»_«f.name»'''
	}
	
	def generateScope(EDataType type) {
		if(type instanceof EEnum) type.name
		else {
			switch (type.instanceTypeName){
				case 'java.lang.String' : 'Int'
				case 'java.lang.Integer' : 'Int'
				case 'int' : 'Int'
				case 'java.lang.Boolean' : 'Boolean'
				case 'boolean' : 'Boolean'
				default: 'Int'
			}			
		}
	}
	
	def generateScope(EClass clazz) {
		val scope = typeScope.getOrDefault(clazz, Collections.emptySet)
		if(scope.empty) '''ExternalNode'''
		else '''«FOR s : scope SEPARATOR '+'»«s.toSigName»«ENDFOR»'''
	}
	
	def hasSeparateScope(EClass clazz) {
		clazz.EAllSuperTypes.forall[it===clazz || it.isAbstract] && typeScope.getOrDefault(clazz, Collections.emptySet).size===1
	}
	
	
	def allReferences() {references}
	def allAttributes() {attributes}
}

