/**
 */
package edu.ustb.sei.mde.bigg;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bigg.Condition#isInvariant <em>Invariant</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.Condition#isError <em>Error</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getCondition()
 * @model abstract="true"
 * @generated
 */
public interface Condition extends EObject {

	/**
	 * Returns the value of the '<em><b>Invariant</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Invariant</em>' attribute.
	 * @see #setInvariant(boolean)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getCondition_Invariant()
	 * @model default="false"
	 * @generated
	 */
	boolean isInvariant();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.Condition#isInvariant <em>Invariant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Invariant</em>' attribute.
	 * @see #isInvariant()
	 * @generated
	 */
	void setInvariant(boolean value);

	/**
	 * Returns the value of the '<em><b>Error</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Error</em>' attribute.
	 * @see #setError(boolean)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getCondition_Error()
	 * @model default="false"
	 * @generated
	 */
	boolean isError();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.Condition#isError <em>Error</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Error</em>' attribute.
	 * @see #isError()
	 * @generated
	 */
	void setError(boolean value);
} // Condition
