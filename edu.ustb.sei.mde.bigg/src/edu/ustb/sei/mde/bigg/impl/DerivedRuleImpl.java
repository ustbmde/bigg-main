/**
 */
package edu.ustb.sei.mde.bigg.impl;

import edu.ustb.sei.mde.bigg.BiggPackage;
import edu.ustb.sei.mde.bigg.DerivedRule;
import edu.ustb.sei.mde.bigg.DerivedRuleKind;
import edu.ustb.sei.mde.bigg.Rule;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Derived Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bigg.impl.DerivedRuleImpl#getDerivedFrom <em>Derived From</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.impl.DerivedRuleImpl#getRuleKind <em>Rule Kind</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DerivedRuleImpl extends RuleImpl implements DerivedRule {
	/**
	 * The cached value of the '{@link #getDerivedFrom() <em>Derived From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedFrom()
	 * @generated
	 * @ordered
	 */
	protected Rule derivedFrom;

	/**
	 * The default value of the '{@link #getRuleKind() <em>Rule Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuleKind()
	 * @generated
	 * @ordered
	 */
	protected static final DerivedRuleKind RULE_KIND_EDEFAULT = DerivedRuleKind.RELATION;
	/**
	 * The cached value of the '{@link #getRuleKind() <em>Rule Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuleKind()
	 * @generated
	 * @ordered
	 */
	protected DerivedRuleKind ruleKind = RULE_KIND_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DerivedRuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BiggPackage.Literals.DERIVED_RULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Rule getDerivedFrom() {
		if (derivedFrom != null && derivedFrom.eIsProxy()) {
			InternalEObject oldDerivedFrom = (InternalEObject)derivedFrom;
			derivedFrom = (Rule)eResolveProxy(oldDerivedFrom);
			if (derivedFrom != oldDerivedFrom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BiggPackage.DERIVED_RULE__DERIVED_FROM, oldDerivedFrom, derivedFrom));
			}
		}
		return derivedFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rule basicGetDerivedFrom() {
		return derivedFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDerivedFrom(Rule newDerivedFrom) {
		Rule oldDerivedFrom = derivedFrom;
		derivedFrom = newDerivedFrom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BiggPackage.DERIVED_RULE__DERIVED_FROM, oldDerivedFrom, derivedFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DerivedRuleKind getRuleKind() {
		return ruleKind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRuleKind(DerivedRuleKind newRuleKind) {
		DerivedRuleKind oldRuleKind = ruleKind;
		ruleKind = newRuleKind == null ? RULE_KIND_EDEFAULT : newRuleKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BiggPackage.DERIVED_RULE__RULE_KIND, oldRuleKind, ruleKind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BiggPackage.DERIVED_RULE__DERIVED_FROM:
				if (resolve) return getDerivedFrom();
				return basicGetDerivedFrom();
			case BiggPackage.DERIVED_RULE__RULE_KIND:
				return getRuleKind();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BiggPackage.DERIVED_RULE__DERIVED_FROM:
				setDerivedFrom((Rule)newValue);
				return;
			case BiggPackage.DERIVED_RULE__RULE_KIND:
				setRuleKind((DerivedRuleKind)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BiggPackage.DERIVED_RULE__DERIVED_FROM:
				setDerivedFrom((Rule)null);
				return;
			case BiggPackage.DERIVED_RULE__RULE_KIND:
				setRuleKind(RULE_KIND_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BiggPackage.DERIVED_RULE__DERIVED_FROM:
				return derivedFrom != null;
			case BiggPackage.DERIVED_RULE__RULE_KIND:
				return ruleKind != RULE_KIND_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (ruleKind: ");
		result.append(ruleKind);
		result.append(')');
		return result.toString();
	}

} //DerivedRuleImpl
