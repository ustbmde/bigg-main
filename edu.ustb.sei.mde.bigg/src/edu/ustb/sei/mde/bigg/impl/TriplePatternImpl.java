/**
 */
package edu.ustb.sei.mde.bigg.impl;

import edu.ustb.sei.mde.bigg.BiggPackage;
import edu.ustb.sei.mde.bigg.Edge;
import edu.ustb.sei.mde.bigg.Node;
import edu.ustb.sei.mde.bigg.Pattern;
import edu.ustb.sei.mde.bigg.TriplePattern;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Triple Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bigg.impl.TriplePatternImpl#getSource <em>Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.impl.TriplePatternImpl#getCorrespondence <em>Correspondence</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.impl.TriplePatternImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.impl.TriplePatternImpl#getAllNodes <em>All Nodes</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.impl.TriplePatternImpl#getAllEdges <em>All Edges</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TriplePatternImpl extends ConditionImpl implements TriplePattern {
	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected Pattern source;

	/**
	 * The cached value of the '{@link #getCorrespondence() <em>Correspondence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondence()
	 * @generated
	 * @ordered
	 */
	protected Pattern correspondence;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected Pattern target;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TriplePatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BiggPackage.Literals.TRIPLE_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Pattern getSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSource(Pattern newSource, NotificationChain msgs) {
		Pattern oldSource = source;
		source = newSource;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BiggPackage.TRIPLE_PATTERN__SOURCE, oldSource, newSource);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSource(Pattern newSource) {
		if (newSource != source) {
			NotificationChain msgs = null;
			if (source != null)
				msgs = ((InternalEObject)source).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BiggPackage.TRIPLE_PATTERN__SOURCE, null, msgs);
			if (newSource != null)
				msgs = ((InternalEObject)newSource).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BiggPackage.TRIPLE_PATTERN__SOURCE, null, msgs);
			msgs = basicSetSource(newSource, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BiggPackage.TRIPLE_PATTERN__SOURCE, newSource, newSource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Pattern getCorrespondence() {
		return correspondence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCorrespondence(Pattern newCorrespondence, NotificationChain msgs) {
		Pattern oldCorrespondence = correspondence;
		correspondence = newCorrespondence;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BiggPackage.TRIPLE_PATTERN__CORRESPONDENCE, oldCorrespondence, newCorrespondence);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCorrespondence(Pattern newCorrespondence) {
		if (newCorrespondence != correspondence) {
			NotificationChain msgs = null;
			if (correspondence != null)
				msgs = ((InternalEObject)correspondence).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BiggPackage.TRIPLE_PATTERN__CORRESPONDENCE, null, msgs);
			if (newCorrespondence != null)
				msgs = ((InternalEObject)newCorrespondence).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BiggPackage.TRIPLE_PATTERN__CORRESPONDENCE, null, msgs);
			msgs = basicSetCorrespondence(newCorrespondence, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BiggPackage.TRIPLE_PATTERN__CORRESPONDENCE, newCorrespondence, newCorrespondence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Pattern getTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTarget(Pattern newTarget, NotificationChain msgs) {
		Pattern oldTarget = target;
		target = newTarget;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BiggPackage.TRIPLE_PATTERN__TARGET, oldTarget, newTarget);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTarget(Pattern newTarget) {
		if (newTarget != target) {
			NotificationChain msgs = null;
			if (target != null)
				msgs = ((InternalEObject)target).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BiggPackage.TRIPLE_PATTERN__TARGET, null, msgs);
			if (newTarget != null)
				msgs = ((InternalEObject)newTarget).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BiggPackage.TRIPLE_PATTERN__TARGET, null, msgs);
			msgs = basicSetTarget(newTarget, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BiggPackage.TRIPLE_PATTERN__TARGET, newTarget, newTarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Node> getAllNodes() {
		EList<Node> allNodes = new org.eclipse.emf.common.util.BasicEList<Node>();
		if(this.getSource()!=null) allNodes.addAll(getSource().getNodes());
		if(this.getCorrespondence()!=null) allNodes.addAll(getCorrespondence().getNodes());
		if(this.getTarget()!=null) allNodes.addAll(getTarget().getNodes());
		return allNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Edge> getAllEdges() {
		EList<Edge> allEdges = new org.eclipse.emf.common.util.BasicEList<Edge>();
		if(this.getSource()!=null) allEdges.addAll(getSource().getEdges());
		if(this.getCorrespondence()!=null) allEdges.addAll(getCorrespondence(). getEdges());
		if(this.getTarget()!=null) allEdges.addAll(getTarget(). getEdges());
		return allEdges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Node getNode(final String name) {
		EList<Node> nodes = getAllNodes();
		for(Node n : nodes) {
		  if(name==n.getName() || name.equals(n.getName())) return n;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BiggPackage.TRIPLE_PATTERN__SOURCE:
				return basicSetSource(null, msgs);
			case BiggPackage.TRIPLE_PATTERN__CORRESPONDENCE:
				return basicSetCorrespondence(null, msgs);
			case BiggPackage.TRIPLE_PATTERN__TARGET:
				return basicSetTarget(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BiggPackage.TRIPLE_PATTERN__SOURCE:
				return getSource();
			case BiggPackage.TRIPLE_PATTERN__CORRESPONDENCE:
				return getCorrespondence();
			case BiggPackage.TRIPLE_PATTERN__TARGET:
				return getTarget();
			case BiggPackage.TRIPLE_PATTERN__ALL_NODES:
				return getAllNodes();
			case BiggPackage.TRIPLE_PATTERN__ALL_EDGES:
				return getAllEdges();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BiggPackage.TRIPLE_PATTERN__SOURCE:
				setSource((Pattern)newValue);
				return;
			case BiggPackage.TRIPLE_PATTERN__CORRESPONDENCE:
				setCorrespondence((Pattern)newValue);
				return;
			case BiggPackage.TRIPLE_PATTERN__TARGET:
				setTarget((Pattern)newValue);
				return;
			case BiggPackage.TRIPLE_PATTERN__ALL_NODES:
				getAllNodes().clear();
				getAllNodes().addAll((Collection<? extends Node>)newValue);
				return;
			case BiggPackage.TRIPLE_PATTERN__ALL_EDGES:
				getAllEdges().clear();
				getAllEdges().addAll((Collection<? extends Edge>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BiggPackage.TRIPLE_PATTERN__SOURCE:
				setSource((Pattern)null);
				return;
			case BiggPackage.TRIPLE_PATTERN__CORRESPONDENCE:
				setCorrespondence((Pattern)null);
				return;
			case BiggPackage.TRIPLE_PATTERN__TARGET:
				setTarget((Pattern)null);
				return;
			case BiggPackage.TRIPLE_PATTERN__ALL_NODES:
				getAllNodes().clear();
				return;
			case BiggPackage.TRIPLE_PATTERN__ALL_EDGES:
				getAllEdges().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BiggPackage.TRIPLE_PATTERN__SOURCE:
				return source != null;
			case BiggPackage.TRIPLE_PATTERN__CORRESPONDENCE:
				return correspondence != null;
			case BiggPackage.TRIPLE_PATTERN__TARGET:
				return target != null;
			case BiggPackage.TRIPLE_PATTERN__ALL_NODES:
				return !getAllNodes().isEmpty();
			case BiggPackage.TRIPLE_PATTERN__ALL_EDGES:
				return !getAllEdges().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BiggPackage.TRIPLE_PATTERN___GET_NODE__STRING:
				return getNode((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //TriplePatternImpl
