/**
 */
package edu.ustb.sei.mde.bigg.impl;

import edu.ustb.sei.mde.bigg.BiggPackage;
import edu.ustb.sei.mde.bigg.BiggProgram;
import edu.ustb.sei.mde.bigg.ConstraintHook;
import edu.ustb.sei.mde.bigg.Rule;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bigg.impl.BiggProgramImpl#getRules <em>Rules</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.impl.BiggProgramImpl#getSource <em>Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.impl.BiggProgramImpl#getCorrespondence <em>Correspondence</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.impl.BiggProgramImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.impl.BiggProgramImpl#getExtraConstraints <em>Extra Constraints</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BiggProgramImpl extends MinimalEObjectImpl.Container implements BiggProgram {
	/**
	 * The cached value of the '{@link #getRules() <em>Rules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRules()
	 * @generated
	 * @ordered
	 */
	protected EList<Rule> rules;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected EPackage source;

	/**
	 * The cached value of the '{@link #getCorrespondence() <em>Correspondence</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondence()
	 * @generated
	 * @ordered
	 */
	protected EPackage correspondence;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected EPackage target;

	/**
	 * The cached value of the '{@link #getExtraConstraints() <em>Extra Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtraConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<ConstraintHook> extraConstraints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BiggProgramImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BiggPackage.Literals.BIGG_PROGRAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Rule> getRules() {
		if (rules == null) {
			rules = new EObjectContainmentEList<Rule>(Rule.class, this, BiggPackage.BIGG_PROGRAM__RULES);
		}
		return rules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EPackage getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject)source;
			source = (EPackage)eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BiggPackage.BIGG_PROGRAM__SOURCE, oldSource, source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSource(EPackage newSource) {
		EPackage oldSource = source;
		source = newSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BiggPackage.BIGG_PROGRAM__SOURCE, oldSource, source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EPackage getCorrespondence() {
		if (correspondence != null && correspondence.eIsProxy()) {
			InternalEObject oldCorrespondence = (InternalEObject)correspondence;
			correspondence = (EPackage)eResolveProxy(oldCorrespondence);
			if (correspondence != oldCorrespondence) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BiggPackage.BIGG_PROGRAM__CORRESPONDENCE, oldCorrespondence, correspondence));
			}
		}
		return correspondence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetCorrespondence() {
		return correspondence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCorrespondence(EPackage newCorrespondence) {
		EPackage oldCorrespondence = correspondence;
		correspondence = newCorrespondence;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BiggPackage.BIGG_PROGRAM__CORRESPONDENCE, oldCorrespondence, correspondence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EPackage getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (EPackage)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BiggPackage.BIGG_PROGRAM__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTarget(EPackage newTarget) {
		EPackage oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BiggPackage.BIGG_PROGRAM__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ConstraintHook> getExtraConstraints() {
		if (extraConstraints == null) {
			extraConstraints = new EObjectContainmentEList<ConstraintHook>(ConstraintHook.class, this, BiggPackage.BIGG_PROGRAM__EXTRA_CONSTRAINTS);
		}
		return extraConstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BiggPackage.BIGG_PROGRAM__RULES:
				return ((InternalEList<?>)getRules()).basicRemove(otherEnd, msgs);
			case BiggPackage.BIGG_PROGRAM__EXTRA_CONSTRAINTS:
				return ((InternalEList<?>)getExtraConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BiggPackage.BIGG_PROGRAM__RULES:
				return getRules();
			case BiggPackage.BIGG_PROGRAM__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case BiggPackage.BIGG_PROGRAM__CORRESPONDENCE:
				if (resolve) return getCorrespondence();
				return basicGetCorrespondence();
			case BiggPackage.BIGG_PROGRAM__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case BiggPackage.BIGG_PROGRAM__EXTRA_CONSTRAINTS:
				return getExtraConstraints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BiggPackage.BIGG_PROGRAM__RULES:
				getRules().clear();
				getRules().addAll((Collection<? extends Rule>)newValue);
				return;
			case BiggPackage.BIGG_PROGRAM__SOURCE:
				setSource((EPackage)newValue);
				return;
			case BiggPackage.BIGG_PROGRAM__CORRESPONDENCE:
				setCorrespondence((EPackage)newValue);
				return;
			case BiggPackage.BIGG_PROGRAM__TARGET:
				setTarget((EPackage)newValue);
				return;
			case BiggPackage.BIGG_PROGRAM__EXTRA_CONSTRAINTS:
				getExtraConstraints().clear();
				getExtraConstraints().addAll((Collection<? extends ConstraintHook>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BiggPackage.BIGG_PROGRAM__RULES:
				getRules().clear();
				return;
			case BiggPackage.BIGG_PROGRAM__SOURCE:
				setSource((EPackage)null);
				return;
			case BiggPackage.BIGG_PROGRAM__CORRESPONDENCE:
				setCorrespondence((EPackage)null);
				return;
			case BiggPackage.BIGG_PROGRAM__TARGET:
				setTarget((EPackage)null);
				return;
			case BiggPackage.BIGG_PROGRAM__EXTRA_CONSTRAINTS:
				getExtraConstraints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BiggPackage.BIGG_PROGRAM__RULES:
				return rules != null && !rules.isEmpty();
			case BiggPackage.BIGG_PROGRAM__SOURCE:
				return source != null;
			case BiggPackage.BIGG_PROGRAM__CORRESPONDENCE:
				return correspondence != null;
			case BiggPackage.BIGG_PROGRAM__TARGET:
				return target != null;
			case BiggPackage.BIGG_PROGRAM__EXTRA_CONSTRAINTS:
				return extraConstraints != null && !extraConstraints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //BiggProgramImpl
