/**
 */
package edu.ustb.sei.mde.bigg;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Correspondence Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getCorrespondenceNode()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface CorrespondenceNode extends TranslationObject {

} // CorrespondenceNode
