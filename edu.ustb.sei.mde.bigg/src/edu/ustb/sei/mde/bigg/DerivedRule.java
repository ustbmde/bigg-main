/**
 */
package edu.ustb.sei.mde.bigg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Derived Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bigg.DerivedRule#getDerivedFrom <em>Derived From</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.DerivedRule#getRuleKind <em>Rule Kind</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getDerivedRule()
 * @model
 * @generated
 */
public interface DerivedRule extends Rule {
	/**
	 * Returns the value of the '<em><b>Derived From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived From</em>' reference.
	 * @see #setDerivedFrom(Rule)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getDerivedRule_DerivedFrom()
	 * @model
	 * @generated
	 */
	Rule getDerivedFrom();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.DerivedRule#getDerivedFrom <em>Derived From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Derived From</em>' reference.
	 * @see #getDerivedFrom()
	 * @generated
	 */
	void setDerivedFrom(Rule value);

	/**
	 * Returns the value of the '<em><b>Rule Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.bigg.DerivedRuleKind}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Kind</em>' attribute.
	 * @see edu.ustb.sei.mde.bigg.DerivedRuleKind
	 * @see #setRuleKind(DerivedRuleKind)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getDerivedRule_RuleKind()
	 * @model
	 * @generated
	 */
	DerivedRuleKind getRuleKind();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.DerivedRule#getRuleKind <em>Rule Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule Kind</em>' attribute.
	 * @see edu.ustb.sei.mde.bigg.DerivedRuleKind
	 * @see #getRuleKind()
	 * @generated
	 */
	void setRuleKind(DerivedRuleKind value);

} // DerivedRule
