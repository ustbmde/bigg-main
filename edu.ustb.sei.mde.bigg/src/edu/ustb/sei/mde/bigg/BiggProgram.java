/**
 */
package edu.ustb.sei.mde.bigg;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bigg.BiggProgram#getRules <em>Rules</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.BiggProgram#getSource <em>Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.BiggProgram#getCorrespondence <em>Correspondence</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.BiggProgram#getTarget <em>Target</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.BiggProgram#getExtraConstraints <em>Extra Constraints</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getBiggProgram()
 * @model
 * @generated
 */
public interface BiggProgram extends EObject {
	/**
	 * Returns the value of the '<em><b>Rules</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.bigg.Rule}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules</em>' containment reference list.
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getBiggProgram_Rules()
	 * @model containment="true"
	 * @generated
	 */
	EList<Rule> getRules();

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(EPackage)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getBiggProgram_Source()
	 * @model
	 * @generated
	 */
	EPackage getSource();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.BiggProgram#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(EPackage value);

	/**
	 * Returns the value of the '<em><b>Correspondence</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correspondence</em>' reference.
	 * @see #setCorrespondence(EPackage)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getBiggProgram_Correspondence()
	 * @model
	 * @generated
	 */
	EPackage getCorrespondence();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.BiggProgram#getCorrespondence <em>Correspondence</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Correspondence</em>' reference.
	 * @see #getCorrespondence()
	 * @generated
	 */
	void setCorrespondence(EPackage value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(EPackage)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getBiggProgram_Target()
	 * @model
	 * @generated
	 */
	EPackage getTarget();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.BiggProgram#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(EPackage value);

	/**
	 * Returns the value of the '<em><b>Extra Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.bigg.ConstraintHook}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extra Constraints</em>' containment reference list.
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getBiggProgram_ExtraConstraints()
	 * @model containment="true"
	 * @generated
	 */
	EList<ConstraintHook> getExtraConstraints();

} // BiggProgram
