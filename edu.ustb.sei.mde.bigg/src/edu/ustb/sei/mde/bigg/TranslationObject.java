/**
 */
package edu.ustb.sei.mde.bigg;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Translation Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bigg.TranslationObject#getStatus <em>Status</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getTranslationObject()
 * @model abstract="true"
 * @generated
 */
public interface TranslationObject extends EObject {
	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The default value is <code>"auto"</code>.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.bigg.PersistentMark}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see edu.ustb.sei.mde.bigg.PersistentMark
	 * @see #setStatus(PersistentMark)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getTranslationObject_Status()
	 * @model default="auto"
	 * @generated
	 */
	PersistentMark getStatus();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.TranslationObject#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see edu.ustb.sei.mde.bigg.PersistentMark
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(PersistentMark value);

} // TranslationObject
