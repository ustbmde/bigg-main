/**
 */
package edu.ustb.sei.mde.bigg;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bigg.FunctionCall#getCallee <em>Callee</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.FunctionCall#getArguments <em>Arguments</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getFunctionCall()
 * @model
 * @generated
 */
public interface FunctionCall extends Condition, Value {
	/**
	 * Returns the value of the '<em><b>Callee</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Callee</em>' attribute.
	 * @see #setCallee(String)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getFunctionCall_Callee()
	 * @model
	 * @generated
	 */
	String getCallee();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.FunctionCall#getCallee <em>Callee</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Callee</em>' attribute.
	 * @see #getCallee()
	 * @generated
	 */
	void setCallee(String value);

	/**
	 * Returns the value of the '<em><b>Arguments</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.bigg.Argument}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arguments</em>' containment reference list.
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getFunctionCall_Arguments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Argument> getArguments();

} // FunctionCall
