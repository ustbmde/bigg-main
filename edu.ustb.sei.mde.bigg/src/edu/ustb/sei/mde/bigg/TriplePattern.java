/**
 */
package edu.ustb.sei.mde.bigg;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Triple Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bigg.TriplePattern#getSource <em>Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.TriplePattern#getCorrespondence <em>Correspondence</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.TriplePattern#getTarget <em>Target</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.TriplePattern#getAllNodes <em>All Nodes</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.TriplePattern#getAllEdges <em>All Edges</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getTriplePattern()
 * @model
 * @generated
 */
public interface TriplePattern extends Condition {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' containment reference.
	 * @see #setSource(Pattern)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getTriplePattern_Source()
	 * @model containment="true"
	 * @generated
	 */
	Pattern getSource();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.TriplePattern#getSource <em>Source</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' containment reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(Pattern value);

	/**
	 * Returns the value of the '<em><b>Correspondence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correspondence</em>' containment reference.
	 * @see #setCorrespondence(Pattern)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getTriplePattern_Correspondence()
	 * @model containment="true"
	 * @generated
	 */
	Pattern getCorrespondence();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.TriplePattern#getCorrespondence <em>Correspondence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Correspondence</em>' containment reference.
	 * @see #getCorrespondence()
	 * @generated
	 */
	void setCorrespondence(Pattern value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' containment reference.
	 * @see #setTarget(Pattern)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getTriplePattern_Target()
	 * @model containment="true"
	 * @generated
	 */
	Pattern getTarget();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.TriplePattern#getTarget <em>Target</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' containment reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Pattern value);

	/**
	 * Returns the value of the '<em><b>All Nodes</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.bigg.Node}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Nodes</em>' reference list.
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getTriplePattern_AllNodes()
	 * @model transient="true" volatile="true" derived="true"
	 * @generated
	 */
	EList<Node> getAllNodes();

	/**
	 * Returns the value of the '<em><b>All Edges</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.bigg.Edge}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Edges</em>' reference list.
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getTriplePattern_AllEdges()
	 * @model transient="true" volatile="true" derived="true"
	 * @generated
	 */
	EList<Edge> getAllEdges();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Node getNode(String name);

} // TriplePattern
