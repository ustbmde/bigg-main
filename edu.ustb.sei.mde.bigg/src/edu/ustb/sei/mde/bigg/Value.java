/**
 */
package edu.ustb.sei.mde.bigg;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getValue()
 * @model abstract="true"
 * @generated
 */
public interface Value extends Argument {
	String description();
} // Value
