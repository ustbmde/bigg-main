/**
 */
package edu.ustb.sei.mde.bigg;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bigg.Rule#getLhs <em>Lhs</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.Rule#getRhs <em>Rhs</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.Rule#getNacs <em>Nacs</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bigg.Rule#getVariables <em>Variables</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getRule()
 * @model
 * @generated
 */
public interface Rule extends NamedElement, EModelElement {
	/**
	 * Returns the value of the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lhs</em>' containment reference.
	 * @see #setLhs(TriplePattern)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getRule_Lhs()
	 * @model containment="true"
	 * @generated
	 */
	TriplePattern getLhs();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.Rule#getLhs <em>Lhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lhs</em>' containment reference.
	 * @see #getLhs()
	 * @generated
	 */
	void setLhs(TriplePattern value);

	/**
	 * Returns the value of the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rhs</em>' containment reference.
	 * @see #setRhs(TriplePattern)
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getRule_Rhs()
	 * @model containment="true"
	 * @generated
	 */
	TriplePattern getRhs();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bigg.Rule#getRhs <em>Rhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rhs</em>' containment reference.
	 * @see #getRhs()
	 * @generated
	 */
	void setRhs(TriplePattern value);

	/**
	 * Returns the value of the '<em><b>Nacs</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.bigg.Condition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nacs</em>' containment reference list.
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getRule_Nacs()
	 * @model containment="true"
	 * @generated
	 */
	EList<Condition> getNacs();

	/**
	 * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.bigg.Variable}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variables</em>' containment reference list.
	 * @see edu.ustb.sei.mde.bigg.BiggPackage#getRule_Variables()
	 * @model containment="true"
	 * @generated
	 */
	EList<Variable> getVariables();

} // Rule
