/**
 */
package edu.ustb.sei.mde.bigg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.bigg.BiggPackage#getConstant()
 * @model abstract="true"
 * @generated
 */
public interface Constant extends Value {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getStringRepresentation();
} // Constant
