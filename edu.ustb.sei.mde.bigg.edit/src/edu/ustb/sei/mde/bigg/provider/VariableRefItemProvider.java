/**
 */
package edu.ustb.sei.mde.bigg.provider;


import edu.ustb.sei.mde.bigg.BiggPackage;
import edu.ustb.sei.mde.bigg.Rule;
import edu.ustb.sei.mde.bigg.TriplePattern;
import edu.ustb.sei.mde.bigg.VariableRef;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.bigg.VariableRef} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class VariableRefItemProvider extends ValueItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableRefItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addVariablePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Variable feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addVariablePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_VariableRef_variable_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_VariableRef_variable_feature",
								"_UI_VariableRef_type"),
						BiggPackage.Literals.VARIABLE_REF__VARIABLE, true, false, true, null, null, null) {
					@Override
					protected Collection<?> getComboBoxObjects(Object object) {
						if (object instanceof VariableRef) {
							Rule rule = getRule((EObject) object);
							EReference position = getPositionInRule((EObject) object);
							
							if(rule!=null && position!=null) {
								return rule.getVariables();
							} else return null;
					    }
						else return super.getComboBoxObjects(object);
					}
				});
	}
	
	private EReference getPositionInRule(EObject obj) {
		if(obj==null) return null;
		else {
			if(obj.eContainer() instanceof Rule) {
				if(obj.eContainmentFeature()==BiggPackage.Literals.RULE__LHS) return BiggPackage.Literals.RULE__LHS;
				else if(obj.eContainmentFeature()==BiggPackage.Literals.RULE__RHS) return BiggPackage.Literals.RULE__RHS;
				else if(obj.eContainmentFeature()==BiggPackage.Literals.RULE__NACS) return BiggPackage.Literals.RULE__NACS;
				else return null;
			} else return getPositionInRule(obj.eContainer());
		}
	}
	
	private Rule getRule(EObject obj) {
		if(obj==null) return null;
		else if(obj instanceof Rule) return (Rule)obj;
		else return getRule(obj.eContainer());
	}
	
	private TriplePattern getTriplePattern(EObject obj) {
		if(obj==null) return null;
		else if(obj instanceof TriplePattern) return (TriplePattern)obj;
		else return getTriplePattern(obj.eContainer());
	}

	/**
	 * This returns VariableRef.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/VariableRef"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_VariableRef_type");
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
