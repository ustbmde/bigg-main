/**
 */
package edu.ustb.sei.mde.bigg.provider;


import edu.ustb.sei.mde.bigg.BiggPackage;
import edu.ustb.sei.mde.bigg.BiggProgram;
import edu.ustb.sei.mde.bigg.Edge;
import edu.ustb.sei.mde.bigg.Node;
import edu.ustb.sei.mde.bigg.Pattern;
import edu.ustb.sei.mde.bigg.Rule;
import edu.ustb.sei.mde.bigg.TriplePattern;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.bigg.Edge} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EdgeItemProvider 
	extends TranslationObjectItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EdgeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSourcePropertyDescriptor(object);
			addTargetPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addSourcePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(new ItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_source_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_source_feature", "_UI_Edge_type"),
				 BiggPackage.Literals.EDGE__SOURCE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null) {
				
				@Override
				protected Collection<?> getComboBoxObjects(Object object) {
					if(object instanceof Edge) {
						Pattern pattern = getPattern((EObject)object);
						TriplePattern tPattern = (TriplePattern) pattern.eContainer();
						
						Set<Node> nodes = new HashSet<>();
						Predicate<Node> filter = ((Edge) object).getType()==null ? e->true : e->(((Edge) object).getType().getEContainingClass().isSuperTypeOf(e.eClass()));
						
						pattern.getNodes().stream().filter(filter).forEach(e->nodes.add(e));
						
						if(tPattern.eContainmentFeature()==BiggPackage.Literals.RULE__NACS) {
							Rule rule = (Rule) tPattern.eContainer();
							collect(rule.getLhs(), nodes, filter, false, true, false);
						}
						
						
						return nodes;
					}
					return super.getComboBoxObjects(object);
				}
			}
		);
	}
	
	private void collect(TriplePattern pattern, Set<Node> nodes, Predicate<Node> filter, boolean fromSource, boolean fromCor, boolean fromView) {
		if(pattern==null) return;
		
		if(fromSource) {
			if(pattern.getSource()!=null) {
				pattern.getSource().getNodes().stream().filter(filter).forEach(e->nodes.add(e));
			}
		}
		
		if(fromCor) {
			if(pattern.getCorrespondence()!=null) {
				pattern.getCorrespondence().getNodes().stream().filter(filter).forEach(e->nodes.add(e));
			}
		}
		
		if(fromView) {
			if(pattern.getTarget()!=null) {
				pattern.getTarget().getNodes().stream().filter(filter).forEach(e->nodes.add(e));
			}
		}
	}
	
	private Pattern getPattern(EObject obj) {
		if(obj==null) return null;
		else if(obj instanceof Pattern) return (Pattern)obj;
		else return getPattern(obj.eContainer());
	}

	/**
	 * This adds a property descriptor for the Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(new ItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_target_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_target_feature", "_UI_Edge_type"),
				 BiggPackage.Literals.EDGE__TARGET,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null) {
				
				@Override
				protected Collection<?> getComboBoxObjects(Object object) {
					if(object instanceof Edge) {
						Pattern pattern = getPattern((EObject)object);
						TriplePattern tPattern = (TriplePattern) pattern.eContainer();
						
						Set<Node> nodes = new HashSet<>();
						Predicate<Node> filter = ((Edge) object).getType()==null ? e->true : e->(((Edge) object).getType().getEReferenceType().isSuperTypeOf(e.getType()));
						
						if(pattern.eContainmentFeature()==BiggPackage.Literals.TRIPLE_PATTERN__CORRESPONDENCE) {
							collect(tPattern, nodes, filter, true, false, true);
							
							if(tPattern.eContainmentFeature()==BiggPackage.Literals.RULE__NACS) {
								Rule rule = (Rule) tPattern.eContainer();
								collect(rule.getLhs(), nodes, filter, true, true, true);
							}
						} else if(pattern.eContainmentFeature()==BiggPackage.Literals.TRIPLE_PATTERN__SOURCE) {
							pattern.getNodes().stream().filter(filter).forEach(e->nodes.add(e));
							
							if(tPattern.eContainmentFeature()==BiggPackage.Literals.RULE__NACS) {
								Rule rule = (Rule) tPattern.eContainer();
								collect(rule.getLhs(), nodes, filter, true, false, false);
							}
						} else if(pattern.eContainmentFeature()==BiggPackage.Literals.TRIPLE_PATTERN__TARGET) {
							pattern.getNodes().stream().filter(filter).forEach(e->nodes.add(e));
							
							if(tPattern.eContainmentFeature()==BiggPackage.Literals.RULE__NACS) {
								Rule rule = (Rule) tPattern.eContainer();
								collect(rule.getLhs(), nodes, filter, false, false, true);
							}
						}
						
						return nodes;
					}
					return super.getComboBoxObjects(object);
				}
			}
			);
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(new ItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_type_feature", "_UI_Edge_type"),
				 BiggPackage.Literals.EDGE__TYPE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null) {
				
				@Override
				protected Collection<?> getComboBoxObjects(Object object) {
					if(object instanceof Edge) {
						Pattern pattern = getPattern((EObject)object);
						
						EPackage pkg = null;
						
						EObject root = EcoreUtil.getRootContainer(pattern);
						if(root instanceof BiggProgram) {
							if(pattern.eContainmentFeature()==BiggPackage.Literals.TRIPLE_PATTERN__SOURCE) {
								pkg = ((BiggProgram) root).getSource();
							} else if(pattern.eContainmentFeature()==BiggPackage.Literals.TRIPLE_PATTERN__TARGET) {
								pkg = ((BiggProgram) root).getTarget();
							} else if(pattern.eContainmentFeature()==BiggPackage.Literals.TRIPLE_PATTERN__CORRESPONDENCE) {
								pkg = ((BiggProgram) root).getCorrespondence();
							}
							
						}
						
						Set<EReference> cand = new HashSet<>();
						
						if(((Edge) object).getSource()!=null) {
							EClass srcCls = ((Edge) object).getSource().getType();
							cand.addAll(srcCls.getEAllReferences());
						} else {
							if(pkg!=null) {
								pkg.eAllContents().forEachRemaining(e->{
									if(e instanceof EReference) {
										cand.add((EReference)e);
									}
								});
							}
						}
						
						if(((Edge) object).getTarget()!=null) {
							cand.removeIf(r->!r.getEReferenceType().isSuperTypeOf(((Edge) object).getTarget().getType()));
						}
						
						return cand;
					}
					return super.getComboBoxObjects(object);
				}
				
			}
		);
	}

	/**
	 * This returns Edge.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Edge"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		Edge edge = (Edge)object;
		StringBuilder builder = new StringBuilder();
		if(edge.getSource()!=null) builder.append(edge.getSource().getName());
		builder.append('-');
		if(edge.getType()!=null) builder.append(edge.getType().getName());
		builder.append("->");
		if(edge.getTarget()!=null) builder.append(edge.getTarget().getName());
		return builder.toString();
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
